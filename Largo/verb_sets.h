/**
 * \file      sentence.h
 * \brief     The Sentence file
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S.Panin
 * \version   v.1.1
 * \date      Created on 24 September 2014 y., 22:24
 * \TODO
**/
#pragma once
#include <QString>
#include "const.h"
#include "ps/c++/qt/db/sqlite/serialization.h"

/**
 * \brief The Sentence
 * \code
 * \endcode
**/
class Sentence {
public:
    typedef Sentence       class_name;
    typedef uint           val_t;
    typedef QVector<val_t> sets_t;

private:
    static const unsigned count_elements = 3;
    static const unsigned none           = 0;

    val_t   tense_;
    val_t   type_;
    val_t   form_;
    val_t   lang_;
    QString str_;

public:
    explicit Sentence()
        : str_(QString()), tense_(none), type_(none), form_(none), lang_(none){
    }
    explicit Sentence(QString const& str, val_t tense, val_t type, val_t form, val_t lang)
        : str_(str), tense_(tense), type_(type), form_(form), lang_(lang) {
    }
    /**
     * \brief Saving the sets to String
     * \code
     *     auto const& to = sets::toString(s);
     * \endcode
    **/
    static QString toString(Sentence const& s) {
        sets_t s;
        s.push_back(tense_);
        s.push_back(type_);
        s.push_back(form_);
        return ps::Serialization48::to(s);
    }
    /**
     * \brief Getting a sets from String
     * \code
     *     auto s2 = sets::fromString(str);
     * \endcode
    **/
    static Sentence fromString(QString const& str) {
        sets_t s;
        s = ps::Serialization48::from<sets_t>(str);
        s.resize(count_elements);
        return Sentence(s[0], s[1], s[2]);
    }
    friend bool operator ==(Sentence const& lhs, Sentence const& rhs) {
        return lhs.tense_ == rhs.tense_ && lhs.type_ == rhs.type_ && lhs.form_ == rhs.form_;
    }
    friend bool operator !=(Sentence const& lhs, Sentence const& rhs) {
        return !(lhs == rhs);
    }

    /**
     * \brief Setting a language
     * \code
     *       sets_.lang(QLocale::AnyLanguage);
     *       sets_.lang(QLocale::English);
     *       sets_.lang(QLocale::Russian);
     * \endcode
     */
    void lang(item_t item) {
        sets_[lang_index] = item;
    }
    /**
     * \brief Getting a language
     * \code
     *       if(sets_.lang() == QLocale::English) result = item.lang_;
     *       if(sets_.lang() == QLocale::AnyLanguage || sets_.lang() == first.lang_) items_.push_back(first);
     * \endcode
     */
    item_t lang() const {
        return sets_[lang_index];
    }
    /**
     * \brief form
     * \code
     *       using namespace module::library::form;
     *       sets_.form(interrogative().first | declarative().first | negative().first);
     * \endcode
     */
    void form(item_t item) {
        sets_[form_index] = item;
    }
    /**
     * \brief form
     * \code
     *       if(sets_.form() & interrogative().first || item.form_ == interrogative().first) result = item.form_;
     * \endcode
     */
    item_t form() const {
        return sets_[form_index];
    }
    /**
     * \brief tense
     * \code
     *       using namespace module::library::tense;
     *       sets_.tense(present().first | past().first | future().first);
     * \endcode
    **/
    void tense(item_t item) {
        sets_[tense_index] = item;
    }
    /**
     * \brief tense
     * \code
     *       using namespace module::library::dialog;
     *       if(sets_.tense() & present().first && item.tense_ == present().first) result = item.tense_;
     * \endcode
    **/
    item_t tense() const {
        return sets_[tense_index];
    }
    /**
     * \brief The dialogs
     * \code
     *       using namespace module::library::dialog;
     *       sets_.tense(simple().first | words().first | true_false().first);
     * \endcode
    **/
    void dialog(item_t item) {
        sets_[dialog_index] = item;
    }
    /**
     * \brief The dialogs
     * \code
     *       if(sets_.dialog() & simple().first && item.dialog_ == simple().first) result = item.simple_;
     * \endcode
    **/
    item_t dialog() const {
        return sets_[dialog_index];
    }
    /**
     * \brief type
     * \code
     *       using namespace module::library::type;
     *       sets_.type() = simple().first;
     * \endcode
     */
    void type(item_t item) {
        sets_[type_index] = item;
    }
    item_t  type() const {
        return sets_[type_index];
    }
};

/** \namespace sets */
namespace sets {
/**
 * \brief The Tense to String
**/
static QString tenseToString (Sentence const& s) {
    using namespace module::library::tense;
    QString result;
    if(s.tense() & past().first)    result = past().second;
    if(s.tense() & present().first) result += (result.size()?"+":"") + present().second;
    if(s.tense() & future().first)  result += (result.size()?"+":"") + future().second;;
    return result;
}

/**
 * \brief The Form to String
**/
static QString formToString (Sentence const& s) {
    using namespace module::library::form;
    QString result;
    if(s.form() & interrogative().first) result = interrogative().second;
    if(s.form() & declarative().first)   result += (result.size()?"+":"") + declarative().second;
    if(s.form() & negative().first)      result += (result.size()?"+":"") + negative().second;
    return result;
}
/**
 * \brief The lang to String
**/
static QString langToString (Sentence const& s) {
    if(s.lang() == QLocale::English)     return "english";
    if(s.lang() == QLocale::Russian)     return "russian";
    return "all";
}
/**
 * \brief The dialog to String
**/
static QString dialogToString (Sentence const& s) {
    using namespace module::dialog;
    if(s.dialog() & simple().first)   return simple().second;
    if(s.dialog() & simple2().first)  return simple2().second;
    if(s.dialog() & words().first)    return words().second;
    if(s.dialog() & birds2().first)   return birds2().second;
    return none().second;
}

} // end namespace sets
