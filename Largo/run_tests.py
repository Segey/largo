#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file		stat.py
#   @brief		The main file runs all tests 
#   @author		S.Panin
#   @Copyright	S.Panin
#   @version	v.1.1
#   @date	    Created on 13 August 2013 y., 00:18
#   @TODO
#
import os;
import time;
import re;
from subprocess import call

cx = 0
path  = "C:/Projects/largo/Largo/"
files = [
            "classes/db/tests_/library/",
            "classes/db/tests_/settings/",
            "classes/policies/data/tests_/",
            "classes/policies/data/tests_/",
            "classes/policies/sequences/tests_/",
            "classes/policies/times/tests_/",
            "classes/simple/docks/simple_words/from_xml/tests_/"
        ]

print("********* Start Tests *********")
for file in files:
    f = path + file
    os.chdir(f)
    output = call("main.py", shell=True, stdout=None, stdin=None)
    if(output != 0):
        print("ERROR: {0:<5}".format(file))
    else :
        print("PASS:  {0:<5}".format(file))
        cx +=1
    #os.spawnvp(os.P_WAIT, 'main.py')
    #os.spawnl(["main.py", ""], "")
    #print(os.system("main.py >))
print("Totals: {0} passed, {1} failed".format(len(files), len(files) - cx))
print("********* End Tests *********")
