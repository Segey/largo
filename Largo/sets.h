/**
 * \file      sets.h
 * \brief     The Sets file
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S.Panin
 * \version   v.1.1
 * \date      Created on 23 September 2013 y., 16:27
 * \TODO
**/
#pragma once
#include <QString>
#include <QVector>
#include "ps/c++/qt/db/sqlite/serialization.h"

/**
 * \brief All options's data
 * \code
 *     Sets s();
 * \endcode
**/
class Sets {
public:
    typedef Sets            class_name;
    typedef unsigned        item_t;
    typedef QVector<item_t> sets_t;

private:
    static const unsigned lang_index    = 0;
    static const unsigned form_index    = 1;
    static const unsigned tense_index   = 2;
    static const unsigned dialog_index  = 3;
    static const unsigned type_index    = 4;

private:
    sets_t sets_;

public:
    explicit Sets() {
        std::fill_n(std::back_inserter(sets_), type_index + 1, 0);
    }
    /**
     * \brief Saving the sets to String
     * \code
     *     auto const& to = sets::toString(s);
     * \endcode
    **/
    static QString toString(Sets const& s) {
        return ps::Serialization48::to(s.sets_);
    }
    /**
     * \brief Getting a sets from String
     * \code
     *     auto s2 = sets::fromString(str);
     * \endcode
    **/
    static Sets fromString(QString const& str) {
        Sets s;
        s.sets_ = ps::Serialization48::from<sets_t>(str);
        s.sets_.resize(type_index + 1);
        return s;
    }
    friend bool operator ==(Sets const& lhs, Sets const& rhs) {
        return lhs.sets_ == rhs.sets_;
    }
    friend bool operator !=(Sets const& lhs, Sets const& rhs) {
        return !(lhs.sets_ == rhs.sets_);
    }

    /**
     * \brief Setting a language
     * \code
     *       sets_.lang(QLocale::AnyLanguage);
     *       sets_.lang(QLocale::English);
     *       sets_.lang(QLocale::Russian);
     * \endcode
     */
    void lang(item_t item) {
        sets_[lang_index] = item;
    }
    /**
     * \brief Getting a language
     * \code
     *       if(sets_.lang() == QLocale::English) result = item.lang_;
     *       if(sets_.lang() == QLocale::AnyLanguage || sets_.lang() == first.lang_) items_.push_back(first);
     * \endcode
     */
    item_t lang() const {
        return sets_[lang_index];
    }
    /**
     * \brief form
     * \code
     *       using namespace module::library::form;
     *       sets_.form(interrogative().first | declarative().first | negative().first);
     * \endcode
     */
    void form(item_t item) {
        sets_[form_index] = item;
    }
    /**
     * \brief form
     * \code
     *       if(sets_.form() & interrogative().first || item.form_ == interrogative().first) result = item.form_;
     * \endcode
     */
    item_t form() const {
        return sets_[form_index];
    }
    /**
     * \brief tense
     * \code
     *       using namespace module::library::tense;
     *       sets_.tense(present().first | past().first | future().first);
     * \endcode
    **/
    void tense(item_t item) {
        sets_[tense_index] = item;
    }
    /**
     * \brief tense
     * \code
     *       using namespace module::library::dialog;
     *       if(sets_.tense() & present().first && item.tense_ == present().first) result = item.tense_;
     * \endcode
    **/
    item_t tense() const {
        return sets_[tense_index];
    }
    /**
     * \brief The dialogs
     * \code
     *       using namespace module::library::dialog;
     *       sets_.tense(simple().first | words().first | true_false().first);
     * \endcode
    **/
    void dialog(item_t item) {
        sets_[dialog_index] = item;
    }
    /**
     * \brief The dialogs
     * \code
     *       if(sets_.dialog() & simple().first && item.dialog_ == simple().first) result = item.simple_;
     * \endcode
    **/
    item_t dialog() const {
        return sets_[dialog_index];
    }
    /**
     * \brief type
     * \code
     *       using namespace module::library::type;
     *       sets_.type() = simple().first;
     * \endcode
     */
    void type(item_t item) {
        sets_[type_index] = item;
    }
    item_t  type() const {
        return sets_[type_index];
    }
};

/** \namespace sets */
namespace sets {
/**
 * \brief The Tense to String
**/
static QString tenseToString (Sets const& s) {
    using namespace module::library::tense;
    QString result;
    if(s.tense() & past().first)    result = past().second;
    if(s.tense() & present().first) result += (result.size()?"+":"") + present().second;
    if(s.tense() & future().first)  result += (result.size()?"+":"") + future().second;;
    return result;
}

/**
 * \brief The Form to String
**/
static QString formToString (Sets const& s) {
    using namespace module::library::form;
    QString result;
    if(s.form() & interrogative().first) result = interrogative().second;
    if(s.form() & declarative().first)   result += (result.size()?"+":"") + declarative().second;
    if(s.form() & negative().first)      result += (result.size()?"+":"") + negative().second;
    return result;
}
/**
 * \brief The lang to String
**/
static QString langToString (Sets const& s) {
    if(s.lang() == QLocale::English)     return "english";
    if(s.lang() == QLocale::Russian)     return "russian";
    return "all";
}
/**
 * \brief The dialog to String
**/
static QString dialogToString (Sets const& s) {
    using namespace module::dialog;
    if(s.dialog() & simple().first)   return simple().second;
    if(s.dialog() & simple2().first)  return simple2().second;
    if(s.dialog() & words().first)    return words().second;
    if(s.dialog() & birds2().first)   return birds2().second;
    return none().second;
}

} // end namespace sets
