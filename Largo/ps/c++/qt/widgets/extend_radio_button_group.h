/**
 * \file       ps/c++/qt/widgets/extend_radio_button_group.h
 * \brief      The ExtendRadioButtonGroup
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S.Panin
 * \version    v.1.3
 * \date       Created on 02 September 2013 y., 22:27
 * \TODO
**/
#pragma once
#include <QString>
#include <QComboBox>
#include <QGroupBox>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QButtonGroup>
#include <QRadioButton>

/** \namespace ps */
namespace ps {
class ExtendRadioButtonGroup : public QGroupBox {
Q_OBJECT

public:
    typedef QGroupBox		        inherited;
    typedef ExtendRadioButtonGroup  class_name;

private:
    static const int MORE_ID = -1000;

private:
    unsigned      MAX_;
    QVBoxLayout*  base_lay_;
    QGridLayout*  top_lay_;
    QHBoxLayout*  bottom_lay_;
    QButtonGroup* group_;
    QRadioButton* more_text_;
    QComboBox*    more_combobox_;
    unsigned      cx_;
    unsigned      cy_;
    int           current_;

    /**
     * \brief This function instances all signals for the class.
    **/
    void instanceSignals() {
       connect(group_,         SIGNAL(buttonClicked(int)),        SLOT(onButtonClicked(int)));
       connect(more_combobox_, SIGNAL(currentIndexChanged(int)),  SLOT(onCurrentIndexChanged(int)));
    }
    /**
     * \brief This function instances the Bottom Layout
    **/
    void instanceBottomLayout() {
        bottom_lay_->addWidget(more_text_);
        bottom_lay_->addWidget(more_combobox_, 10, Qt::AlignAbsolute);
        group_->addButton(more_text_, MORE_ID);
        more_combobox_->setEnabled(false);
    }
    /**
     * \brief This function instances the Layout
    **/
    void instanceLayout() {
        instanceBottomLayout();
        base_lay_->addLayout(top_lay_);
        base_lay_->addLayout(bottom_lay_);
        inherited::setLayout(base_lay_);
    }
private slots:
    void onButtonClicked(int but_id) {
        if(but_id == MORE_ID) {  
            if(more_combobox_->count()) more_combobox_->setEnabled(true);
            onCurrentIndexChanged(more_combobox_->currentIndex());
            return;
        }
        more_combobox_->setEnabled(false);
        current_ = but_id;
        emit(buttonClicked(but_id));
    }
    void onCurrentIndexChanged(int but_id) {
        bool done = false;
        auto id = more_combobox_->itemData(but_id).toInt(&done);
        if(done) {
            current_ = id;
            emit(buttonClicked(id));
        }
    }
signals:
    void buttonClicked(unsigned);                //!< This signal is emitted when a button with the given id is clicked.

public:
    /**
     * \brief Ctor
     * \param {in: unsigned} MAX         - The maximum of column  
     * \param {in: QString const&} title - The title of RadioButton Group
     * \param {in: QString const&} more  - The More text for ComboBox
     * \param {in: QWidget*} parent      - The parent of MainWindow
     * \code
     *	   ps::ExtendRadioButtonGroup* group = new ps::ExtendRadioButtonGroup(1, "Cool", "More", parent_);
     *	   parent_->addRadioButtonGroupWidget(Qt::LeftRadioButtonGroupWidgetArea, dock);
     * \endcode
    **/
    explicit ExtendRadioButtonGroup(unsigned MAX, QString const& title = QString(), QString const& more = QString(), QWidget* parent = nullptr)
        : inherited(title, parent), MAX_(MAX), base_lay_(new QVBoxLayout), top_lay_(new QGridLayout), bottom_lay_(new QHBoxLayout),
          group_(new QButtonGroup), more_text_(new QRadioButton(more)), more_combobox_(new QComboBox), cx_(0), cy_(0), current_(0) {
        instanceLayout();
        instanceSignals();
    }
    /**
     * \brief The function creates and adds QRadioButton to group 
     * \param {in: QString const& } title - The title of QRadioButton
     * \param {in: int } id - The id of QRadioButton
     * \code
     *    group->addRadioButton("cool", 1);
     * \endcode
    **/
    void addRadioButton(QString const& title, int id) { 
        QRadioButton* button = new QRadioButton(title, this);
        group_->addButton(button, id);
        if(cx_ >= MAX_) {
            ++cy_; 
            cx_ = 0;
        }
        top_lay_->addWidget(button, cy_, cx_++);
    }
    /**
     * \brief The function creates and adds QRadioButton to group 
     * \param {in: QString const& } title - The title of QRadioButton
     * \param {in: int } id - The id of QRadioButton
     * \code
     *    group->addRadioButton("cool", 1);
     * \endcode
    **/
    void addExtendRadioButton(QString const& title, int id) { 
        more_combobox_->addItem(title, id);
    }
    void setMoreText(QString const& text) {
       return more_text_->setText(text);
    }
    /**
     * \code
     *      grop->group()->button(1)->setChecked(true);
     * \endcode
    **/
    QButtonGroup* group() {
       return group_; 
    }
    void setCheckeddButton(int id, bool is) {
        group_->button(id)->setChecked(is);
        current_ = id;
    }

    /**
     * \code
     *      grop->button(1)->setChecked(true);
     * \endcode
    **/
    QAbstractButton* button(int id) {
       return group_->button(id);
    }
    /**
     * \code
     *      grop->size();
     * \endcode
    **/
    unsigned size() {
        return group_->buttons().size();
    }
    /**
     * \brief Returns a current button id
    **/
    int currentId() const {
        return current_;
    }
};
} // end namespace go

