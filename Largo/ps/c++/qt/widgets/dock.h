/**
*   \file		go/dock/dock.h
*   \brief		The Dock
*   \author		S.Panin <dix75@mail.ru>
*   \Copyright  S. Panin
*   \version	v.1.03
*   \date		Created on 25 Febuary 2012 y., 01:12
*   \TODO
*/
#pragma once
#include <QDockWidget>

/** namespace go */
namespace go {
template<typename T>
class Dock : public QDockWidget {
public:
    typedef QDockWidget			inherited;
    typedef T           		value_type;
    typedef Dock<value_type>	class_name;
    typedef QDockWidget 		dock_type;
    typedef value_type* 		pointer;

private:
    QWidget*           			parent_;
    pointer            			widget_;

public:
    /**
     * \brief Ctor
     * \param {in: QWidget*} parent - The parent of MainWindow 
     * \code
     *	go::Dock* dock = new go::Dock<QCalendarWidget>(parent_);
	 *	(*dock)->setGridVisible(true);
	 *	parent_->addDockWidget(Qt::LeftDockWidgetArea, dock); 
     * \endcode
     */
    explicit Dock(QWidget* parent) : inherited(parent), parent_(parent), widget_(nullptr) {
        widget_ = new value_type(parent_);
        inherited::setWidget(widget_);
    }
    pointer widget() const { 
        return widget_; 
    }
    pointer operator->() const {
        return widget_; 
    }
    pointer operator->() {
        return widget_; 
    }
};
} // end namespace go

