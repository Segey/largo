/**
 * \file       ps/c++/qt/widgets/radio_button_group.h
 * \brief      The RadioButtonGroup
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.3.3
 * \date       Created on 18 August 2012 y., 21:46
 * \TODO
**/
#pragma once
#include <QString>
#include <QGroupBox>
#include <QGridLayout>
#include <QButtonGroup>
#include <QRadioButton>

/** \namespace ps */
namespace ps {
class RadioButtonGroup : public QGroupBox {
Q_OBJECT

public:
    typedef QGroupBox		  inherited;
    typedef RadioButtonGroup  class_name;

private:
    unsigned      MAX_;
    QGridLayout*  lay_;
    QButtonGroup* group_;
    unsigned      cx_;
    unsigned      cy_;

    /**
     * \brief This function instances all signals for the class.
    **/
    void instanceSignals() {
       connect(group_, SIGNAL(buttonClicked(QAbstractButton*)),  SIGNAL(buttonClicked(QAbstractButton*)));
       connect(group_, SIGNAL(buttonClicked(int)),               SIGNAL(buttonClicked(int)));
       connect(group_, SIGNAL(buttonPressed(QAbstractButton*)),  SIGNAL(buttonPressed(QAbstractButton*)));
       connect(group_, SIGNAL(buttonPressed(int)),               SIGNAL(buttonPressed(int)));
       connect(group_, SIGNAL(buttonReleased(QAbstractButton*)), SIGNAL(buttonReleased(QAbstractButton*)));
       connect(group_, SIGNAL(buttonReleased(int)),              SIGNAL(buttonReleased(int)));
    }

signals:
    void buttonClicked(QAbstractButton*);   //!< This signal is emitted when the given button is clicked.  
    void buttonClicked(int);                //!< This signal is emitted when a button with the given id is clicked.
    void buttonPressed(QAbstractButton*);   //!< This signal is emitted when the given button is pressed down. 
    void buttonPressed(int);                //!< This signal is emitted when a button with the given id is pressed down.
    void buttonReleased(QAbstractButton*);  //!< This signal is emitted when the given button is released. 
    void buttonReleased(int);               //!< This signal is emitted when a button with the given id is released.

public:
    /**
     * \brief Ctor
     * \param {in: unsigned} MAX         - The maximum of column  
     * \param {in: QString const&} title - The title of RadioButton Group
     * \param {in: QWidget*} parent      - The parent of MainWindow
     * \code
     *	   ps::RadioButtonGroup* group = new ps::RadioButtonGroup(1, "Cool", parent_);
     *	   parent_->addRadioButtonGroupWidget(Qt::LeftRadioButtonGroupWidgetArea, dock);
     * \endcode
    **/
    explicit RadioButtonGroup(unsigned MAX, QString const& title = QString() , QWidget* parent = nullptr )
        : inherited(title, parent), MAX_(MAX), lay_(new QGridLayout), group_(new QButtonGroup(parent)), cx_(0), cy_(0) {
        inherited::setLayout(lay_);
        instanceSignals();
    }
    /**
     * \brief The function creates and adds QRadioButton to group 
     * \param {in: QString const& } title - The title of QRadioButton
     * \param {in: int } id - The id of QRadioButton
     * \code
     *    group->addRadioButton("cool", 1);
     * \endcode
    **/
    void addRadioButton(QString const& title, int id) { 
        QRadioButton* button = new QRadioButton(title, this);
        group_->addButton(button, id);
        if(cx_ >= MAX_) {
            ++cy_; 
            cx_ = 0;
        }
        lay_->addWidget(button, cy_, cx_++);
    }
    /**
     * \code
     *      grop->group()->button(1)->setChecked(true);
     * \endcode
    **/
    QButtonGroup* group() {
       return group_; 
    }
    /**
     * \code
     *      grop->button(1)->setChecked(true);
     * \endcode
    **/
    QAbstractButton* button(int id) {
       return group_->button(id); 
    }
    /**
     * \code
     *      grop->size();
     * \endcode
    **/
    unsigned size() {
        return group_->buttons().size();
    }
    /**
     * \brief Returns a current button id
    **/
    int currentId() const {
        return group_->checkedId();
    }
};
} // end namespace go

