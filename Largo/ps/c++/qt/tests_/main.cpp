#include <QtTest/QtTest>
#include <QTGui>
#include "../rand.h"

class Test_Rand : public QObject {
Q_OBJECT

private slots:
    void initTestCase()    { //!< Called before everything else
    }      
    void cleanupTestCase() { //!< After testing
    }
    void testRandWithHole() {
        for(int j=0; j != 1000; ++j)
            for(int i=0; i != 9; ++i) {
                auto val = ps::randWithHole(0,7,i);
               // qDebug() << val << i ;
                QVERIFY(val != i);
                QVERIFY(0 <= val && val <= 7);
        }
    } 
    void testRandWithHoles() {
        for(int j=0; j != 100; ++j)
            for(int i=0; i != 7; ++i) {
                QVector<unsigned> vec;
                for(int k=0; k!= i; ++k) {
                    const auto rand = ps::randInt(0,7);
                    vec.push_back(rand);
                }
                auto val = ps::randWithHoles(0,7,vec);
                auto it = qFind(vec.begin(), vec.end(), val);
             //   qDebug() << val << vec ;
                QVERIFY(it == vec.end());
                QVERIFY(0 <= val && val <= 7);
        }
    }
};

QTEST_MAIN(Test_Rand)
#include "main.moc"

