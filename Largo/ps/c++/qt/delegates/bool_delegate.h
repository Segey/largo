/**
 * \file       ps/c++/qt/delegates/bool_delegate.h
 * \brief      The Bool delegate
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.2.1
 * \date       Created on 07 September 2012 y., 01:38
 * \TODO
**/
#pragma once
#include <QWidget>
#include <QComboBox>
#include <QModelIndex>
#include <QItemDelegate>
#include <QAbstractItemModel>
#include <QStyleOptionViewItem>

/** namespace ps::delegate */
namespace ps {
namespace delegate {
class Bool : public QItemDelegate {
public:
    typedef QItemDelegate inherited;
    typedef Bool 		  class_name;

    static const unsigned bool_role = Qt::UserRole +1;              //!< The bool role for 3 column

public:
    explicit Bool(QObject* parent = 0) : inherited(parent) {
    }
    QWidget* createEditor(QWidget* parent, QStyleOptionViewItem const& /* option */, QModelIndex const& /* index */) const {
        QComboBox* box = new QComboBox(parent);
        box->addItem(iWord::tr("false"));
        box->addItem(iWord::tr("true"));
        return box;
    }
    void setEditorData(QWidget* widget, QModelIndex const& index) const {
        const bool value = index.model()->data(index, bool_role).toBool();

        QComboBox* box = static_cast<QComboBox*>(widget);
        box->setCurrentIndex(value == false ? 0 : 1);
    }
    void setModelData(QWidget* widget, QAbstractItemModel* model, QModelIndex const& index) const {
        QComboBox* box = static_cast<QComboBox*>(widget);
        const bool value = box->currentIndex() == 1 ? true : false;
        model->setData(index, value, bool_role);
        model->setData(index, value, Qt::EditRole);
    }
    void updateEditorGeometry(QWidget* widget, QStyleOptionViewItem const& option, QModelIndex const& /* index */) const {
        widget->setGeometry(option.rect);
    }
};
}} // end namespace ps::delegate

