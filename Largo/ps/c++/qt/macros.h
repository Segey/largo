/**
 * \file      macros.h
 * \brief     The All macros for library
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.01
 * \date      Created on 19 July 2012 y., 00:21
 * \TODO
**/
#pragma once

#define OVERRIDE_CURSOR(_a) {\
    QApplication::setOverrideCursor(Qt::WaitCursor);\
    { _a; }\
    QApplication::restoreOverrideCursor();\
}

#define RESTORE_OVERRIDE_CURSOR(_a) {\
    QApplication::restoreOverrideCursor();\
    _a;\
    return false;\
}

#define ALERT(_a)  {\
    QMessageBox msgBox;\
    msgBox.setText(QString("%1").arg(_a));\
    msgBox.exec();\
}

