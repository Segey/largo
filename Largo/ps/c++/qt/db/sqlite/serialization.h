/**
 * \file      ps/c++/qt/db/serialization.h
 * \brief     The file for serialization simple data
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin
 * \version   v.1.2
 * \date      Created on 04 April 2012 y., 23:42
 * \TODO		
**/
/** \namespace ps  */
#pragma once
namespace ps {

/** 
 * \code
 *      const auto& to      = ps::Serialization48::to(QVector<unsigned>());
 * 	    const auto& to 		= ps::Serialization48::to(QString("ggg"));
 * 	    const auto& from 	= ps::Serialization48::from<QString>(s22);
 * 	    uint version 	    = ps::Serialization48::version(s22);
 * 	    const bool is 	    = ps::Serialization48::is(s22);
 * \endcode
**/
template<int VERSION = QDataStream::Qt_4_8>
struct Serialization {
    /**
     * \brief Does the version of serials right?
     * \param {in: QString const&} val Serialization
    **/
   static bool is(QString const& val) {
        QByteArray byteArray = QByteArray::fromBase64(QByteArray().append(val));
        QDataStream in(&byteArray, QIODevice::ReadOnly);
        return in.version() == VERSION;
    }
   /**
    * \brief The version serialization
    * \param {in: QString const& } val - the serial
   **/
   static uint version(QString const& val) {
        QByteArray byteArray = QByteArray::fromBase64(QByteArray().append(val));
        QDataStream in(&byteArray, QIODevice::ReadOnly);
        return in.version();
    }
    /**
     * \brief Getting data from serialization
     * \param {in: QString const& } val - the serialization
     * \param {in: template T} defaultValue - возращаемое значение если версия другая
     * \result  T   данные из сериала или дефолтовая строка
    **/
    template<typename T>
    static T from(QString const& val, T const& defaultValue = T()) {
        T result = T();
        int ver;
        QByteArray byteArray = QByteArray::fromBase64(QByteArray().append(val));
        QDataStream in(&byteArray, QIODevice::ReadOnly);
        in >> ver;
        in >> result;
        return ver == VERSION ? result : defaultValue;
    }

    /**
     * \brief Setting data to serialization
     * \param {in: QString const& } val - the data
     * \result  T   данные из сериала или дефолтовая строка
    **/
    template<typename T>
    static QString to(T const& val) {
        QByteArray byteArray;
        QDataStream out(&byteArray, QIODevice::WriteOnly);
        out  << VERSION << val;
        return byteArray.toBase64().data();
    }
};

typedef Serialization<QDataStream::Qt_4_8>   Serialization48;
} // end namespace ps

