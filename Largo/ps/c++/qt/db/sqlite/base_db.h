/**
 * \file      ps/c++/qt/db/sqlite/base_db.h
 * \brief     The file works with database SQLite
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin
 * \version   v.1.8
 * \date      Created on 01 April 2012 y., 15:20
 * \TODO		
**/
#pragma	once

#include <QSqlDatabase>
#include <QSqlQuery>

/** \namespace ps */
namespace ps {

/**
 * \class base_db
 * \brief The Base class for work with database
 * \param S - The Error strategy
 * \see ps::strategy::message::base;
 **/
template<typename S>
class base_db {
    typedef S                      strategy_type;
    typedef base_db<strategy_type> class_name;

    QSqlDatabase bd_;
    QString      database_filename_;
    QString      database_name_;

    static bool check_correct_bd(QSqlDatabase const& bd, QString const& database_name) {
        QSqlQuery query(bd);
        query.exec("SELECT program, database FROM program_info");
        query.next();
        return (query.value(0).toString() == QString(program::name()) && (query.value(1).toString() == database_name));
    }
    static bool show_error_message(QString const& target_file) {
		QString str = iMessage::tr("Error while trying to create database file '%1' !").arg(target_file);
        strategy_type::show(str, ps::strategy::message::base::critical);
		return false;
    }

public:
    virtual ~base_db() = 0;

	/**
     * \brief Checking conection to the database
     * \param {in: const QString&} filename - The Database file name
     * \param {in: const QString&} database_name - The Database name
     * \result bool The success of this operation
     * \warning The Database has to contain the ProgramInfo table with 2 fields:\n
     *   The first field - The program name which returns from the function program::name();\n
     *   The seconde field - The Database name (ie the second parameter);
	**/
    static bool check_connect(QString const& filename, QString const& database_name) {
        if(false == QFile::exists(filename)) return false;

        bool b = false;
        {
			QSqlDatabase bd = QSqlDatabase::addDatabase("QSQLITE", "Other");
			bd.setDatabaseName(filename);
			b = !bd.open() ? false : check_correct_bd(bd, database_name);
        }
        QSqlDatabase::removeDatabase("Other");
        return b;
    }
	/**
	 * \brief Conecting with SQLite database
     * \param {in: const QString&} filename - The Database file name
     * \param {in: const QString&} database_name - The Database name
     * \result bool The success of this operation
	**/
    bool connect(QString const& filename, QString const& database_name) {
        if(false == class_name::check_connect(filename, database_name)) {
            QString str = iMessage::tr("%1 could not open '%2' because it is either not a supported file type or because the file has been damaged (for example, it was sent as an email attachment and wasn't correctly decoded).").arg(program::name()).arg(filename);
            strategy_type::show(str, strategy::message::base::critical);
            return false;
        }

        disconnect();
        bd_ = QSqlDatabase::addDatabase("QSQLITE", database_name);
        bd_.setDatabaseName(filename);
        database_filename_ = filename;
        database_name_ = database_name;
        return bd_.open();
    }
	/**
	 * \brief Creating the Database from a  script list. 
	 * \param {in: const QStringList&} list - The script list is a QStringList with SQL instructions
	 * \param {in: const QString&} target_file - The Creatiing file name
	 * \param {in: const QString&} database_name - The db name
     * \result bool The success of this operation
	 **/
    static bool createDatabase(QStringList const& list, QString const& target_file, QString const& database_name) {       
        if(false == QFile::exists(target_file))
            QDir().mkpath(QFileInfo(target_file).dir().path());
        else QFile::remove(target_file);

        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE","Create");
        db.setDatabaseName(target_file);

        if(false == db.open()) return show_error_message(target_file);

        size_t done = 0;
        QSqlQuery query(db);

		foreach (QString const& line, list)
			done += !query.exec(line);
        return !done && check_connect(target_file, database_name);
    }
	/** \brief Disconnecting */
    void  disconnect() { 
        bd_.close();
    }
	/**
	 * \brief Getting the DB link
	 * \param {in: const QString&} database_name - The db name
	 * \result QSqlDatabase - The DB link
	**/
    static QSqlDatabase database(QString const& database_name) {
		return QSqlDatabase::database(database_name); 
	}
	/**
	 * \brief The Database filename
	**/
    const QString& database_filename() const { 
        return database_filename_; 
    }
	/**
	 * \brief  Does the connect exist?
	 * \result bool - существует соединение или нет.
	**/
    bool is_connect() const { 
        return bd_.isValid(); 
    }
    /**
     * \brief Getting the last row id
     */
    static unsigned getLastRowId(QSqlQuery& query) {
        query.exec(QString("SELECT last_insert_rowid();"));
        if(false == query.next()) return 0;
        return  query.value(0).toUInt();
    }
};

template<typename S> base_db<S>::~base_db() { }

}; // end namespace ps 
