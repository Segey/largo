/**
 * \file      ps/lg/c++/instance/plugin_main_window_instance.h
 * \brief     The Plugin instanse 
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.02
 * \date      Created on 16 August 2012 y., 23:25
 * \TODO
 **/
#pragma once
/** \namespace ps::instance::plugin  */
namespace ps {
namespace instance {
namespace plugin {
/** 
 * \brief 	Instantation MainWindow
 * \param T The MainWindow
 * \note    The MainWindow has to containt this functions
 * <pre>
 *  - instance_form();
 *  - instance_widgets();
 *  - translate();
 * </pre>
 *\code
 *   ps::instance::plugin::MainWindow< ps::Instance > (this, window).instance();
 *\endcode
 **/
template<typename T>
class MainWindow: private T {
    typedef T             inherited;
    typedef MainWindow<T> class_name;

public:
   /** 
    *   \brief Ctor
    *	\param parent - The template pointer on the object where there are all pointers on components
    * 	\param window - The template pointer on the object where there are all pointers on components
    **/
    template <typename U, typename W>
    explicit MainWindow(U* parent, W* window) {
        inherited::instance_form(parent, window);
		inherited::instance_widgets(parent, window);
		inherited::translate(parent, window);
    }
    /**
     * \brief instantation MainWindow.  
     */
    void instance() {
    }
};
}}} // end namespace ps::instance::plugin

