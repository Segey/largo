/**
 * \file      ps/c++/qt/instance/main_window_instance.h
 * \brief     The Main Windows instance 
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.01
 * \date      Created on 16 August 2012 y., 23:19
 * \TODO
**/
#pragma once
/** \namespace ps::instance  */
namespace ps {
namespace instance {
/** 
 * \brief   Instantation MainWindow
 * \param T The MainWindow
 * \note    The MainWindow has to containt this functions
 * <pre>
 *  - instance_form();
 *  - instance_widgets();
 *  - translate();
 * </pre>
 *   \code
 *	    ps::instance::MainWindow< main::Instance > (window).instance();
 *   \endcode
 **/
template<typename T>
class MainWindow : private T {
    typedef T             ihnerited;
    typedef MainWindow<T> class_name;

    void instance_form() { 
        ihnerited::instance_form(); 
    }
    void instance_widgets() {
        ihnerited::instance_widgets(); 
    } 
    void translate() { 
        ihnerited::translate(); 
    }

public:
   /** 
    *	\brief Ctor
    *	\param {in: typename U} form - The template pointer on the MainWindow.
    */
    template <typename U> explicit MainWindow(U* form) : ihnerited(form) {
    }
    /**
     * \brief Instantation the MainWindow.  
     */
    void instance() {
		instance_form();
		instance_widgets();
		translate();
	}
};
}} // end namespace ps::instance

