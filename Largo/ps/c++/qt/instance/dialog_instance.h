/**
 * \file      ps/c++/qt/instance/dialog_instance.cpp
 * \brief     The Dialog instance 
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S.Panin
 * \version   v.1.1
 * \date      Created on 16 August 2012 y., 23:09
 * \TODO
**/
#pragma once
/** \namespace ps::instance  */
namespace ps {
namespace instance {
/** 
 *    \brief The instantation Dialog
 *    \param T	a Dialog
 *    \note The template parametr have to contain this functions
 *    <pre>
 *    - instance_form();
 *    - instance_widgets();
 *    - translate();
 *    </pre>
 *    \code
 *        ps::instance::Dialog< main::Dialog > (dia).instance();
 *    \endcode
**/
template<typename T>
class Dialog : private T {
    typedef T         ihnerited;
    typedef Dialog<T> class_name;

    void instance_form() { 
        ihnerited::instance_form(); 
    }
    void instance_widgets() { 
        ihnerited::instance_widgets(); 
    } 
    void translate() {
        ihnerited::translate(); 
    }
public:
   /** 
    *   \brief The ctor
    *	\param {in: U} form - The template pointer on the dialog
   **/
    template <typename U>   
    explicit Dialog(U* form) : ihnerited(form) {
    }
   /**
    *  \brief The instantation the Dialog.  
   **/
    void instance() {
		instance_form();
		instance_widgets();
		translate();
	}
};
}} // end namespace ps::instance
