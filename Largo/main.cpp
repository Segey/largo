/**
 * \File    main.cpp
 * \Author  S.Panin <dix75@mail.ru>
 *
 * Created on 26 July 2013 y., 01:42
 **/
#include <QApplication>
#include <QtCore/qvector.h>
#include <qwidget.h>
#include "classes/simple/simple.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/icons/Largo.ico"));

    qsrand((uint)QTime::currentTime().msec());

    QTranslator translator;
    QString langs = QApplication::applicationDirPath() + QString("%1langs").arg(QDir::separator());
    translator.load("ru", langs);
    app.installTranslator(&translator);

    if(false == QFile::exists(module::settings::file()))
        lg::SettingsNone::createDatabase();

    if(false == QFile::exists(module::library::file()))
        lg::LibraryNone::createDatabase();

    lg::Settings n;   n.tryToUpdate();
    lg::Library l;    l.tryToUpdate();

    lg::SimpleMainWindow  simple;
    simple.show();

  //  using namespace lg::policy;
//           lg::dialog::Words<data::Words, time::Simple, sequence::Random> words;
//       //   simple.setData(data);
//           auto result = words.exec();
//           const auto time_p = words.time_policy();
//           qDebug() << time_p.time();

    return app.exec();
}
