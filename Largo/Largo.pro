#-------------------------------------------------
#
# Project created by QtCreator 2013-06-26  01:37:08
#
#-------------------------------------------------

QT += core gui sql widgets xml xmlpatterns

TARGET = Largo
TEMPLATE = app
OBJECTS_DIR = ../debug

SOURCES += \
    main.cpp \
    classes/simple/docks/simple_words/simple_words.cpp \
    ps/c++/qt/widgets/check_header_view.cpp \
    classes/simple/simple.cpp

HEADERS  += \
    tr.h \
    pch.h \
    const.h \
    classes/db/source_library.h \
    classes/db/library.h \
    classes/simple/central_widget_instance.h \
    classes/simple/central_widget.h \
    classes/simple/simple_instance.h \
    classes/simple/simple.h \
    classes/simple/docks/simple_words/simple_words_view.h \
    classes/simple/docks/simple_words/simple_words_instance.h \
    classes/simple/docks/simple_words/simple_words.h \
    classes/db/source_settings.h \
    classes/db/settings.h \
    ps/c++/qt/widgets/radio_button_group.h \
    ps/c++/qt/widgets/group_box.h \
    ps/c++/qt/widgets/dock.h \
    ps/c++/qt/widgets/checkbox_group.h \
    ps/c++/qt/widgets/check_header_view.h \
    classes/simple/docks/simple_words/simple_words_model.h \
    classes/simple/docks/simple_words/from_xml.h \
    classes/simple/menu/menu_help.h \
    classes/trainers/trainer/base.h \
    classes/save_restore_window.h \
    classes/save_restore_dialog.h \
    classes/dialogs/simple/instance.h \
    classes/trainers/simple.h \
    classes/trainers/base.h \
    classes/dialogs/simple/simple_dialog.h \
    classes/dialogs/simple/simple_dialog_base.h \
    classes/policies/times/simply.h \
    classes/policies/times/none.h \
    classes/policies/data/none.h \
    classes/policies/sequences/none.h \
    classes/policies/sequences/random.h \
    classes/policies/sequences/lineal.h \
    classes/dialogs/birds2/simple_dialog.h \
    classes/dialogs/birds2/instance.h \
    classes/dialogs/birds2/birds2_dialog_base.h \
    classes/dialogs/birds2/birds2_dialog.h \
    classes/simple/docks/simple_words/from_xml/v2.h \
    classes/simple/docks/simple_words/from_xml/v1.h \
    classes/simple/docks/simple_words/from_xml/base.h \
    classes/policies/data/simple.h \
    classes/policies/data/true_false.h \
    classes/dialogs/words/words_dialog_base.h \
    classes/dialogs/words/words_dialog.h \
    classes/dialogs/words/instance.h \
    classes/policies/data/birds2.h \
    classes/policies/data/base.h \
    classes/policies/data/words.h \
    classes/simple/docks/options/options_instance.h \
    classes/simple/docks/options/options.h \
    ps/c++/qt/widgets/extend_radio_button_group.h \
    sets.h \
    classes/simple/docks/logs/logs_instance.h \
    classes/simple/docks/logs/logs.h \
    classes/db/settings/logsv1.h \
    classes/policies/logs/v1.h \
    classes/policies/logs/none.h \
    classes/functions/dialog.h \
    classes/dialogs/done/instance.h \
    classes/dialogs/done/done_dialog.h \
    classes/db/update/library/worker.h \
    classes/db/update/library/v2.h \
    classes/db/update/base.h \
    classes/db/update/settings/worker.h \
    classes/db/update/settings/v2.h \
    ps/c++/qt/db/sqlite/serialization.h \
    ps/c++/qt/db/sqlite/base_db.h \
    classes/dialogs/simple2/simple2_dialog_base.h \
    classes/dialogs/simple2/simple2_dialog.h \
    classes/dialogs/simple2/instance.h \
    classes/db/settings/settings2.h \
    classes/db/update/settings/v3.h \
    classes/wizards/simple/first_page.h \
    classes/wizards/simple/second_page.h \
    classes/functions/irregular_verbs.h \
    classes/functions/verb.h

PRECOMPILED_HEADER = pch.h
QMAKE_CXXFLAGS    += -std=gnu++0x
CONFIG            += precompile_header

RESOURCES += \
    resources/panels.qrc \
    resources/icons.qrc

TRANSLATIONS += langs/ru.ts \
                langs/en.ts
