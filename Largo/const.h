/**
 * \file      const.h
 * \brief     The Project consts
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S.Panin
 * \version   v.1.6
 * \date      Created on 27 June 2013 y., 21:55
 * \TODO
**/
#pragma once
#include <QApplication>
#include <QDir>

#define GOLDEN_SECTION 1.6180339887

/**
 * \brief All program's data
 * \code
 *     auto list = program::usedLanguages();
 * \endcode
**/
struct program {
    static QString name()           { return "Largo"; }
    static QString version()        { return "1.4"; }
    static QString build()          { return "0248"; }
    static QString name_full()      { return QString(name()) + " v." + version(); }
    static QString dir_home()       { return QDir::homePath() + "/.largo"; }
    static QString dir_langs()      { return QApplication::applicationDirPath() + "/langs"; }
    static QString name_help_file() { return "Help/Help.chm"; }
    static QString url_homepage()   { return "http://largo.org"; }
    static QString author()         { return "Sergey Panin"; }
    static QString copyright()      { return "Copyright (c) 2013-2014, S. Panin. All rights reserved."; }
};

/**
 * \code
 *
 * \endcode
**/
namespace module {
/**
 * \code
 *     using namespace module::dialog;
 *     if(id == simple().first) ;
 *     if(id == words().first) ;
 *     else true_fale();
 * \endcode
**/
namespace dialog {
    typedef std::pair<uint, QString>  type_t;
    static type_t none()          { return type_t(0, QString()); }
    static type_t simple()        { return type_t(1, "simple"); }
    static type_t words()         { return type_t(2, "words"); }
    static type_t birds2() 	      { return type_t(4, "birds2"); }
    static type_t simple2()       { return type_t(8, "simple2"); }
};

    namespace library {
        static QString name()           { return "library"; }
        static QString version()        { return "2"; }
        static QString ext()            { return ".lli"; }
        static QString file()           { return program::dir_home() + "/library" + ext(); }

        namespace form {
            typedef std::pair<uint, QString>  type_t;
            static type_t none()          { return type_t(0, QString()); }
            static type_t interrogative() { return type_t(1, "interrogative"); }
            static type_t declarative()   { return type_t(2, "declarative"); }
            static type_t negative() 	  { return type_t(4, "negative"); }
        };
        /**
         * \code
         *     module::library::tense::past();
         * \endcode
        **/
        namespace tense {
            typedef std::pair<uint, QString>  type_t;
            static type_t none()        { return type_t(0, QString()); }
            static type_t past() 		{ return type_t(1, "past"); }
            static type_t present()     { return type_t(2, "present"); }
            static type_t future() 		{ return type_t(4, "future"); }
        };
        namespace type {
            typedef std::pair<uint, QString>  type_t;

            //static type_t none()        { return type_t(0, QString()); }
            static type_t simple() 		{ return type_t(1, "simple"); }
      //      static type_t get(QString const& name) {
       //         if(name == simple().second) return simple();
       //         return none();
      //      }
        };
        /**
         * \code
         *     module::library::pronoun::I();
         * \endcode
        **/
        namespace pronoun {
            typedef std::pair<uint, QString>  type_t;

     //       static type_t none()    { return type_t(0, QString()); }
     //       static type_t I() 		{ return type_t(1, "I"); }
       //     static type_t ye() 	    { return type_t(2, "You"); }
         //   static type_t you() 	{ return type_t(4, "You"); }
           // static type_t he() 	    { return type_t(8, "He"); }
        //    static type_t she() 	{ return type_t(16, "She"); }
           // static type_t it() 	    { return type_t(32, "It"); }
           // static type_t we() 	    { return type_t(64, "We"); }
          //  static type_t they() 	{ return type_t(128, "They"); }
        };
    };
    /**
     * \code
     *     module::settings::resource_file_ext();
     * \endcode
    **/
    namespace settings {
        static QString name()               { return "settings"; }
        static QString version()            { return "3"; }
        static QString ext()                { return ".lse"; }
        static QString resource_file_ext()  { return ".lsem"; }
        static QString file()               { return program::dir_home() + "/settings" + ext(); }

        /**
         *  \code
         *    module::settings::settings2::simple_simple2();
         *  \endcode
        **/
        namespace settings2 {
            static QString simple_simple2()     { return "SimpleSimple2SpinBox"; }
        };
    };
} // end namespace module
