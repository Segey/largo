/**
 * \file      tr.h
 * \brief     The TR
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.2
 * \date      Created on 27 June 2013 y., 00:38
 * \TODO
**/
#pragma once
#include <QObject>

struct iMessage      : public QObject { Q_OBJECT };

    struct iAction       : public QObject { Q_OBJECT };
//    struct iBook         : public QObject { Q_OBJECT };
//    struct iBody         : public QObject { Q_OBJECT };
//    struct iBD           : public QObject { Q_OBJECT };
    struct iButton       : public QObject { Q_OBJECT };
//    struct iBullet       : public QObject { Q_OBJECT };
    struct iCalc         : public QObject { Q_OBJECT };
//    struct iCopyright    : public QObject { Q_OBJECT };
//    struct iColor        : public QObject { Q_OBJECT };
//    struct iChart        : public QObject { Q_OBJECT };
//    struct iDate         : public QObject { Q_OBJECT };
    struct iDialog       : public QObject { Q_OBJECT };
//    struct iDocs         : public QObject { Q_OBJECT };
//    struct iImage        : public QObject { Q_OBJECT };
//    struct iEditor       : public QObject { Q_OBJECT };
    struct iTemplate     : public QObject { Q_OBJECT };
    struct iError        : public QObject { Q_OBJECT };
//    struct iFilter       : public QObject { Q_OBJECT };
//    struct iLanguage     : public QObject { Q_OBJECT };
//    struct iPanel        : public QObject { Q_OBJECT };
//    struct iMethod       : public QObject { Q_OBJECT };
    struct iMenu         : public QObject { Q_OBJECT };
//    struct iUser         : public QObject { Q_OBJECT };
//    struct iWorkOut      : public QObject { Q_OBJECT };
    struct iWord         : public QObject { Q_OBJECT };
    struct iText         : public QObject { Q_OBJECT };
    struct iType         : public QObject { Q_OBJECT };
    struct iTitle        : public QObject { Q_OBJECT };
