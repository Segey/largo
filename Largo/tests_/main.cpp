#include <QtTest/QtTest>
#include <QTGui>
#include "const.h"
#include "sets.h"

using namespace module::library::form;
using namespace module::library::tense;

class Test_Sets : public QObject {
Q_OBJECT

private slots:
     void initTestCase()    { //!< Called before everything else
    }      
    void cleanupTestCase() { //!< After testing
    }
	void testSaveToString() {
        sets s1,s2;
        s1.form(interrogative().first | declarative().first | negative().first);
        QString str = sets::toString(s1);
        QVERIFY("AAAADAAAAAUAAAAAAAAABwAAAAAAAAAAAAAAAA==" == str);
        s2 = sets::fromString(str);
        QVERIFY(s2 == s1);

        s1.form(0);
        s1.tense(present().first | past().first | future().first);
        str = sets::toString(s1);
        QVERIFY("AAAADAAAAAUAAAAAAAAAAAAAAAcAAAAAAAAAAA==" == str);
        s2 = sets::fromString(str);
        QVERIFY(s2 == s1);

        s1.form(interrogative().first | declarative().first | negative().first);
        s1.tense(present().first | past().first | future().first);
        str = sets::toString(s1);
        QVERIFY("AAAADAAAAAUAAAAAAAAABwAAAAcAAAAAAAAAAA==" == str);
        s2 = sets::fromString(str);
        QVERIFY(s2 == s1);

        s1.lang(QLocale::English);
        s1.form(interrogative().first | declarative().first | negative().first);
        s1.tense(present().first | past().first | future().first);
        str = sets::toString(s1);
        QVERIFY("AAAADAAAAAUAAAAfAAAABwAAAAcAAAAAAAAAAA==" == str);
        s2 = sets::fromString(str);
        QVERIFY(s2 == s1);

        s1.lang(QLocale::Russian);
        s1.form(interrogative().first | declarative().first | negative().first);
        s1.tense(present().first | past().first | future().first);
        str = sets::toString(s1);
        QVERIFY("AAAADAAAAAUAAABgAAAABwAAAAcAAAAAAAAAAA==" == str);
        s2 = sets::fromString(str);
        QVERIFY(s2 == s1);

        s1.lang(QLocale::Russian);
        s1.form(interrogative().first);
        s1.tense(present().first);
        str = sets::toString(s1);
        QVERIFY("AAAADAAAAAUAAABgAAAAAQAAAAIAAAAAAAAAAA==" == str);
        s2 = sets::fromString(str);
        QVERIFY(s2 == s1);
	}
};

QTEST_MAIN(Test_Sets)
#include "main.moc"

