#include <QtTest/QtTest>
#include <QTGui>
#include <QString>
#include <QStringList>
#include "classes/translate/base.h"
#include "classes/translate/none.h"
#include "classes/translate/simple.h"
#include "classes/translate/google.h"

using namespace lg::translate;

class Test_Translate : public QObject {
Q_OBJECT
    typedef Test_Translate class_name;

    bool waitForSignal(QObject *sender, const char *signal, int timeout = 1000) {
        QEventLoop loop;
        QTimer timer;
        timer.setInterval(timeout);
        timer.setSingleShot(true);

        loop.connect(sender, signal, SLOT(quit()));
        loop.connect(&timer, SIGNAL(timeout()), SLOT(quit()));
        timer.start();
        loop.exec();

        return timer.isActive();
    }

    lg::Word test_word_;
    QString test_function_;

private slots:
    void finishedWord(lg::Word const& word) {
      //  qDebug() << (word == test_word_)  << word;
       // QCOMPARE(word,test_word_);
        QVERIFY2(word == test_word_, QString("%1 doesn't equal %2 in the test_function=%3").arg(word).arg(test_word_).arg(test_function_).toStdString().c_str());
    }

private slots:
   void initTestCase()    { //!< Called before everything else
   }
   void cleanupTestCase() {  //!< After testing
   }
    void testNoneTranslate() {
        None none;
        QVERIFY(none.translate(lg::Word::English("book")) == lg::Word::English(""));
        QVERIFY(none.translate(lg::Sentence::English("I love book.")) == lg::Sentence::English(""));

        auto s = None::sentences_t() << lg::Sentence::English("I love book.");
        QVERIFY(none.translate(s) ==  None::sentences_t());
    } 
    void testSimpleTranslate() {
        Simple simple;
        QVERIFY(simple.translate(lg::Word::English("book")) == lg::Word::English("book"));
        QVERIFY(simple.translate(lg::Sentence::English("I love book.")) == lg::Sentence::English("I love book."));

        auto s = Simple::sentences_t() << lg::Sentence::English("I love book.");
        QVERIFY(simple.translate(s) ==  s);
    }
    void testGoogleTranslate() {
        try {
            test_function_ = "testGoogleTranslate";

            qDebug() << _LINE_;

            Google google;
            google.translate(lg::Word::English("book"));
            test_word_ = lg::Word::English("книга");
            this->connect(&google, SIGNAL(finishedWord(lg::Word const&)), this, SLOT(finishedWord(lg::Word const&)));
            QVERIFY(waitForSignal(&google, SIGNAL(finishedWord(lg::Word const&)), 5000));

        } catch(int e) {
            qDebug() << "Error" << e;
        } catch(...) {

        }

       // QTest::qSleep(5000);
       // QVERIFY(simple.translate(lg::Sentence::English("I love book.")) == lg::Sentence::English("I love book."));

        //auto s = Simple::sentences_t() << lg::Sentence::English("I love book.");
       // QVERIFY(simple.translate(s) ==  s);
    }
};

QTEST_MAIN(Test_Translate)
#include "main.moc"

