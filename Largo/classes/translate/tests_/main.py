#! /usr/bin/env python

'''
File        : main.py
Author      : S.Panin
Contact     : S.Panin <dix75@mail.ru>
Date        : 04.09.2013 23:31
Version     : 1.1
Description :
'''
import os
import shutil

FILE      = 'main'
MOC       = 'moc'
QMAKE     = 'qmake'
MAKE      = "mingw32-make"

def removes():
    for file in ["main.moc", "Makefile", "Makefile.Debug", "Makefile.Release", "object_script.Test.Release", "object_script.Test.Debug"]:
        if(True == os.path.exists(file)):
            os.unlink(file)
    for dir in ["debug", "release"]:
        if(True == os.path.exists(dir)):
            shutil.rmtree(dir)

def copy_only_for_7_Windows():
    os.system("{0}".format(MAKE))
    if(True == os.path.exists('releasemain.o')):
        shutil.move('releasemain.o', 'release\{0}.o'.format(FILE))

removes()
os.system("{0} {1}.cpp -o {1}.moc".format(MOC, FILE))
os.system("{0}".format(QMAKE, FILE))
copy_only_for_7_Windows()
os.system("{0}".format(MAKE))
os.system("release\\{0}.exe".format(FILE))
removes()
