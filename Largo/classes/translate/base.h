/**
 * \file      base.h
 * \brief     The File includes the base translate abstract class
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S.Panin
 * \version   v.1.1
 * \date      Created on 24 January 2014 y., 20:08
 * \TODO
**/

#pragma once
#include <QString>
#include <QList>
#include "classes/words/word.h"
#include "classes/words/sentence.h"

/** \namespace lg::translate */
namespace lg {
namespace translate {

class Base {
public:
    typedef Base            class_name;
    typedef QList<Sentence> sentences_t;

private:
    Base const& operator=(Base const&);
    Base(Base const&);

    virtual Word doTranslate(Word const& word) const            = 0;
    virtual Sentence doTranslate(Sentence const& s) const       = 0;
    virtual sentences_t doTranslate(sentences_t const& s) const = 0;

public:
    Base(){ }
    virtual ~Base() {}
    Word translate(Word const& word) const {
        return doTranslate(word);
    }
    Sentence translate(Sentence const& s) const {
        return doTranslate(s);
    }
    sentences_t translate(sentences_t const& s) const {
        return doTranslate(s);
    }
};

}} // end namespace lg::translate
