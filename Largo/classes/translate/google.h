/**
 * \file      /classes/translates/google.h
 * \brief     The File includes the Google translate class
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin
 * \version   v.1.0
 * \date      Created on 24 March 2014 y., 17:05
 * \TODO
**/

#pragma once
#include <QtWidgets>
#include <QtNetwork/QtNetwork>
#include <QJsonDocument>
#include <QObject>
#include "classes/translate/base.h"

/** \namespace lg::translate */
namespace lg {
namespace translate {

class Google : public QObject, public Base {
    Q_OBJECT

public:
    typedef Google                 class_name;
    typedef Base                   inherited;
    typedef inherited::sentences_t sentences_t;

private:
    mutable QNetworkAccessManager manager_word_;
    mutable QNetworkAccessManager manager_sentence_;

    static QUrl url(QString const& s, Language const lang) {
        return QUrl(QString("http://www.translate.google.ru/translate_a/t?client=x&text=%1&sl=%2&tl=ru").arg(s).arg(lang.short_lang()));
    }

    virtual Word doTranslate(Word const& word) const {
        auto u = url(word, word.language());
        manager_word_.get(QNetworkRequest(u));

        return word;
    }
    virtual Sentence doTranslate(Sentence const& s) const {
        return s;
    }
    virtual sentences_t doTranslate(sentences_t const& s) const {
        return s;
    } 
signals:
    void finishedWord(lg::Word const&);

private slots:
    void replyWordFinished(QNetworkReply* reply) {
        if (!reply->error())  {
            QJsonParseError error;
            auto json = QJsonDocument::fromJson(reply->readAll(), &error);
            if(QJsonParseError::NoError != error.error) throw(1);

            auto o = json.object();
            if(o.isEmpty()) throw 2;
            auto v = o.value("sentences");
            if(v.isUndefined()) throw 3;
            if(!v.isArray()) throw 4;
            auto _3 = v.toArray();
            if(_3.isEmpty()) throw 5;
            auto _4 = _3.at(0);
            if(_4.isUndefined()) throw 6;
            if(!_4.isObject()) throw 7;
            auto _5 = _4.toObject();
            if(_5.isEmpty()) throw 8;
            auto _6 = _5.value("trans");
            emit finishedWord(Word(_6.toString(), Language::Russian() ));
        } else  {
            throw(2);
        }
    }

    void replySentenceFinished(QNetworkReply* reply) {
        if (!reply->error())  {
            QJsonParseError error;
            auto json = QJsonDocument::fromJson(reply->readAll(), &error);
            if(QJsonParseError::NoError != error.error) throw(1);

            auto o = json.object();
            if(o.isEmpty()) throw 2;
            auto v = o.value("sentences");
            if(v.isUndefined()) throw 3;
            if(!v.isArray()) throw 4;
            auto _3 = v.toArray();
            if(_3.isEmpty()) throw 5;
            auto _4 = _3.at(0);
            if(_4.isUndefined()) throw 6;
            if(!_4.isObject()) throw 7;
            auto _5 = _4.toObject();
            if(_5.isEmpty()) throw 8;
            auto _6 = _5.value("trans");
            emit finishedWord(Word(_6.toString(), Language::Russian() ));
        } else  {
            throw(2);
        }
    }
public:
    Google(){
        connect(&manager_word_, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyWordFinished(QNetworkReply*)));
        connect(&manager_sentence_, SIGNAL(finished(QNetworkReply*)), this, SLOT(replySentenceFinished(QNetworkReply*)));
    }
    virtual ~Google() {}
};

}} // end namespace lg::translate
