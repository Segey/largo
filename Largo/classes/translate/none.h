/**
 * \file      none.h
 * \brief     The File includes the None translate class
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S.Panin
 * \version   v.1.2
 * \date      Created on 24 January 2014 y., 20:21
 * \TODO
**/

#pragma once
#include <QString>
#include <QList>
#include "classes/translate/base.h"

/** \namespace lg::translate */
namespace lg {
namespace translate {

class None : public Base {
public:
    typedef None                   class_name;
    typedef Base                   inherited;
    typedef inherited::sentences_t sentences_t;

private:
    virtual Word doTranslate(Word const&) const {
        return Word();
    }
    virtual Sentence doTranslate(Sentence const&) const {
        return Sentence();
    }
    virtual sentences_t doTranslate(sentences_t const&) const {
        return sentences_t();
    } 

public:
    None(){ }
    virtual ~None() {}
};

}} // end namespace lg::translate
