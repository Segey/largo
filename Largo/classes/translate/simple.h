/**
 * \file      /classes/translates/simple.h
 * \brief     The File includes the Simple translate class
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S.Panin
 * \version   v.1.2
 * \date      Created on 24 January 2014 y., 20:21
 * \TODO
**/

#pragma once
#include <QString>
#include <QList>
#include "classes/translate/base.h"

/** \namespace lg::translate */
namespace lg {
namespace translate {

class Simple : public Base {
public:
    typedef Simple                 class_name;
    typedef Base                   inherited;
    typedef inherited::sentences_t sentences_t;

private:
    virtual Word doTranslate(Word const& word) const {
        return word;
    }
    virtual Sentence doTranslate(Sentence const& s) const {
        return s;
    }
    virtual sentences_t doTranslate(sentences_t const& s) const {
        return s;
    } 

public:
    Simple(){ }
    virtual ~Simple() {}
};

}} // end namespace lg::translate
