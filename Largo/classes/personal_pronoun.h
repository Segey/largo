/**
 * \file      classes/personal_pronoun.h 
 * \brief     The Personal pronoun file provides a class which give personal pronoun support
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   March (the) 23(th), 2014, 18:40 MSK
 * \updated   March (the) 23(th), 2014, 18:40 MSK
 * \TODO      
**/
#pragma once
#include "classes/language.h"

/** \namespace lg */
namespace lg {

class PersonalPronoun {
public:
    typedef PersonalPronoun class_name;

private:
    QString  name_;
    Language lang_;
    unsigned id_;

    static const unsigned none = 0;
    static const unsigned i    = 1;
    static const unsigned ye   = 2;
    static const unsigned you  = 4;
    static const unsigned he   = 8;
    static const unsigned she  = 16;
    static const unsigned it   = 32;
    static const unsigned we   = 64;
    static const unsigned they = 128;

    virtual QString do_I()    const = 0;
    virtual QString do_ye()   const = 0;
    virtual QString do_you()  const = 0;
    virtual QString do_he()   const = 0;
    virtual QString do_she()  const = 0;
    virtual QString do_it()   const = 0;
    virtual QString do_we()   const = 0;
    virtual QString do_they() const = 0;

    explicit PersonalPronoun(QString const& name, unsigned id, Language const& lang) 
        : name_(name), id_(id), lang_(lang) {
    }
public:
    PersonalPronoun(PersonalPronoun const& rhs)
        : name_(rhs.name_), lang_(rhs.lang_), id_(rhs.id_) {
    }
    PersonalPronoun& operator=(PersonalPronoun const& rhs){
        name_ = rhs.name_;
        lang_ = rhs.lang_;
        id_   = rhs.id_;
        return *this;
    }
    virtual ~PersonalPronoun(){
    }
    static QString I() {
        return do_I();
    }
    QString ye() const {
        return do_ye();
    }
    QString you() const {
        return do_you();
    }
    QString he() const {
        return do_he();
    }
    QString she() const {
        return do_she();
    }
    QString it() const {
        return do_it();
    }
    QString we() const {
        return do_we();
    }
};

}; // end namespace lg
