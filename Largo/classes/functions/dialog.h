/**
 * \file      dialog.h
 * \brief     The File collects functions that work with the QDialog
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.1
 * \date      Created on 02 October 2013 y., 01:37
 * \TODO
**/
#pragma once
#include <QDialog>

/** \namespace lg */
namespace lg {

struct Dialog {
    typedef Dialog class_name;

    /**
     * \code
     *  lg::Dialog::setWindowTitle(this, "1");
     *    or
     *  lg::Dialog::setWindowTitle(parent_, "1", "2");
     * \endcode
    **/
    template<typename D>
    static void setWindowTitle(D* d, QString const& name, QString const& add = QString()) {
        if(add == QString()) d->setWindowTitle(QString("%1").arg(name));
        else d->setWindowTitle(QString("%1-%2").arg(name).arg(add));
    }
};

} // end namespace lg
