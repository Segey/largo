/**
 * \file      classes/words/word.h
 * \brief     The file provides work with words
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   March (the) 16(th), 2014, 16:42 MSK
 * \updated   March (the) 16(th), 2014, 16:42 MSK
 * \TODO      
**/
#pragma once
#include "classes/language.h"

/** \namespace lg */
namespace lg { 

class Word {
public:
    typedef Word class_name;

private:
    QString  word_;
    Language lang_;

public:
    Word(QString const& word = QString(), Language const& lang = lg::Language::None())
        : word_(word), lang_(lang){
    }
    Word(Word const& rhs)
        : word_(rhs.word_), lang_(rhs.lang_) {
    }
    static Word Russian(QString const& word) {
        return Word(word, lg::Language::Russian());
    }
    static Word English(QString const& word) {
        return Word(word, lg::Language::English());
    }
    Word& operator=(Word const& rhs){
        word_ = rhs.word_;
        lang_ = rhs.lang_;
        return *this;
    }
    Word& operator=(QString const& str){
        word_ = str;
        return *this;
    }
    operator QString() const {
        return word_;
    }
    void setWord(QString const& word) {
        word_ = word;
    }
    void setLanguage(Language const& lang) {
        lang_ = lang;
    }
    Language const& language() const {
        return lang_;
    }
    virtual ~Word(){
    }
};
} // end namespace lg
