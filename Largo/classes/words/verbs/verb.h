/**
 * \file      classes/words/verbs/verb.h
 * \brief     The File provides work with verb
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.3
 * \created   March (the) 16(th), 2014, 16:42 MSK
 * \updated   March (the) 16(th), 2014, 16:42 MSK
 * \TODO
**/

#pragma once
#include "classes/words/word.h"

/** \namespace lg */
namespace lg {

class Verb {
    typedef Verb class_name;

    QString infinitive_;
    QString past_simple_;
    QString past_participle_;

public:
    EnglishVerb(QString const& infinitive = QString(), QString const& past_simple = QString(), QString const& past_participle = QString())
        : infinitive_(infinitive), past_simple_(past_simple), past_participle_(past_participle){
    }
    bool isExists() {
        return infinitive_ != QString() && past_simple_ != QString() && past_participle_ != QString();
    }
    QString const& infinitive() const {
        return infinitive_;
    }
    QString const& pastSimple() const {
        return past_simple_;
    }
    QString const& pastParticiple() const {
        return past_participle_;
    }
};

} // end namespace lg
