#include <QtTest/QtTest>
#include <QTGui>
#include "classes/words/verbs/english_verb.h"
#include "classes/words/verbs/irregular_english_verbs.h"
#include "classes/words/verbs/english_verbs.h"

class Test_EnglishWords : public QObject {
Q_OBJECT
    typedef Test_EnglishWords class_name;

private slots:
    void initTestCase()    { //!< Called before everything else
   }
   void cleanupTestCase() { //!< After testing
   }
    void testIrregularEnglishVerbs() {
        lg::IrregularEnglishVerbs verbs;
        auto word1 = verbs.find("go");
        QVERIFY(word1.isExists() == true);
        QVERIFY(word1.infinitive() == "go");
        QVERIFY(word1.pastSimple() == "went");
        QVERIFY(word1.pastParticiple() == "gone");

        auto word2 = verbs.find("book");
        QVERIFY(word2.isExists() == false);
        QVERIFY(word2.infinitive() == "");
        QVERIFY(word2.pastSimple() == "");
        QVERIFY(word2.pastParticiple() == "");

        auto word3 = verbs.find("bear");
        QVERIFY(word3.isExists() == true);
        QVERIFY(word3.infinitive() == "bear");
        QVERIFY(word3.pastSimple() == "bore");
        QVERIFY(word3.pastParticiple() == "born");
    } 
    void testEnglishVerbs() {
        lg::EnglishVerbs verbs;
        auto word1 = verbs.get("go");
        QVERIFY(word1.isExists() == true);
        QVERIFY(word1.infinitive() == "go");
        QVERIFY(word1.pastSimple() == "went");
        QVERIFY(word1.pastParticiple() == "gone");

        auto word2 = verbs.get("book");
        QVERIFY(word2.isExists() == true);
        QVERIFY(word2.infinitive() == "book");
        QVERIFY(word2.pastSimple() == "booked");
        QVERIFY(word2.pastParticiple() == "booked");
    }
};

QTEST_MAIN(Test_EnglishWords)
#include "main.moc"

