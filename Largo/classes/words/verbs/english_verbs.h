/**
 * \file      english_verbs.h
 * \brief     The File includes the verbs maintenance
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S.Panin
 * \version   v.1.1
 * \date      Created on 23 January 2014 y., 16:53
 * \TODO
**/

#pragma once
#include <QString>
#include "english_verb.h"
#include "irregular_english_verbs.h"

/** \namespace lg */
namespace lg {

class EnglishVerbs {
    typedef EnglishVerbs class_name;

    IrregularEnglishVerbs irregular_;

public:
    EnglishVerbs(){
    }
    EnglishVerb get(QString const& word) {
        auto verb = irregular_.find(word);
        if(verb.isExists()) return verb;
        return EnglishVerb(word, word+"ed", word+"ed");
    }
};

} // end namespace lg
