/**
 * \file      /classses/words/verbs/english_verbs.h
 * \brief     The File includes the verb maintenance
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S.Panin
 * \version   v.1.3
 * \date      Created on 20 January 2014 y., 19:59
 * \TODO
**/

#pragma once
#include <QString>

/** \namespace lg */
namespace lg {

class EnglishVerb {
    typedef EnglishVerb class_name;

    QString infinitive_;
    QString past_simple_;
    QString past_participle_;

public:
    EnglishVerb(QString const& infinitive = QString(), QString const& past_simple = QString(), QString const& past_participle = QString())
        : infinitive_(infinitive), past_simple_(past_simple), past_participle_(past_participle){
    }
    bool isExists() {
        return infinitive_ != QString() && past_simple_ != QString() && past_participle_ != QString();
    }
    QString const& infinitive() const {
        return infinitive_;
    }
    QString const& pastSimple() const {
        return past_simple_;
    }
    QString const& pastParticiple() const {
        return past_participle_;
    }
};

} // end namespace lg
