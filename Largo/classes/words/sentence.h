/**
 * \file      classes/functions/ex.h 
 * \brief     The file provides work with ex
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.1
 * \created   March (the) 16(th), 2014, 22:12 MSK
 * \updated   March (the) 16(th), 2014, 22:12 MSK
 * \TODO      
**/
#pragma once
#include "classes/language.h"

/** \namespace lg */
namespace lg { 

class Sentence {
public:
    typedef Sentence class_name;

private:
    QString  sentence_;
    Language lang_;

public:
    Sentence(QString const& sentence = QString(), Language const& lang = lg::Language::None())
        : sentence_(sentence), lang_(lang){
    }
    Sentence(Sentence const& rhs)
        : sentence_(rhs.sentence_), lang_(rhs.lang_) {
    }
    static Sentence Russian(QString const& sentence) {
        return Sentence(sentence, lg::Language::Russian());
    }
    static Sentence English(QString const& sentence) {
        return Sentence(sentence, lg::Language::English());
    }
    Sentence& operator=(Sentence const& rhs){
        sentence_ = rhs.sentence_;
        lang_     = rhs.lang_;
        return *this;
    }
    Sentence& operator=(QString const& str){
        sentence_ = str;
        return *this;
    }
    operator QString() const {
        return sentence_;
    }
    void setLanguage(Language const& lang) {
        lang_ = lang;
    }
    Language const& language() const {
        return lang_;
    }
    virtual ~Sentence(){
    }
};
} // end namespace lg
