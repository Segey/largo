#include <QtTest/QtTest>
#include <QTGui>
#include "classes/language.h"
#include "classes/words/word.h"

class Test_Functions : public QObject {
Q_OBJECT

private slots:
    void initTestCase()    { //!< Called before everything else
    }      
    void cleanupTestCase() { //!< After testing
    }
    void testLanguages() {
        lg::Language lang1;
        QVERIFY(lang1 == 0u);
        
        lg::Language lang2(QLocale::Russian);
        QVERIFY(lang2 == 96u);

        lg::Language lang3(QLocale::English);
        QVERIFY(lang3 == 31u);

        lg::Language lang4 = lang3;
        QVERIFY(lang4 == QLocale::English);

        auto lang5 = lg::Language::Russian();
        QVERIFY(lang5 == 96u);

        auto lang6 = lg::Language::English();
        QVERIFY(lang6 == 31u);
	}
    void testWords() {
        auto word1 = lg::Word();
        QVERIFY(word1 == QString());
        QVERIFY(word1.language() == lg::Language::None());

        auto word2 = lg::Word("Cool", lg::Language::English());
        QVERIFY(word2 == QString("Cool"));
        QVERIFY(word2.language() == lg::Language::English());

        auto word3 = lg::Word::Russian("Cool3");
        QVERIFY(word3 == QString("Cool3"));
        QVERIFY(word3.language() == lg::Language::Russian());

        lg::Word word4 = QString("Cool5");
        QVERIFY(word4 == QString("Cool5"));
        QVERIFY(word4.language() == lg::Language::None());
    }
};

QTEST_MAIN(Test_Functions)
#include "main.moc"
