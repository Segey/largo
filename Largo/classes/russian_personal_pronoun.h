/**
 * \file      classes/russian_personal_pronoun.h 
 * \brief     The Russian Personal pronoun file provides a class which supplies personal pronoun support
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   March (the) 23(th), 2014, 20:50 MSK
 * \updated   March (the) 23(th), 2014, 20:50 MSK
 * \TODO      
**/
#pragma once
#include "classes/language.h"

/** \namespace lg::russian */
namespace lg {
namespace russian {

class PersonalPronoun : lg::PersonalPronoun {
public:
    typedef PersonalPronoun     class_name;
    typedef lg::PersonalPronoun inherited;

    static QString getNameById(unsigned id) {
        if(id == 1) return   "Я";
        if(id == 2) return   "Ты";
        if(id == 4) return   "Вы";
        if(id == 8) return   "Он";
        if(id == 16) return  "Она";
        if(id == 32) return  "Оно";
        if(id == 64) return  "Мы";
        if(id == 128) return "Они";
        return "Неопреленно";
    }
    static unsigned getIdByName(QString const& name) {
        if(name == "Я")   return 1;
        if(name == "Ты")  return 4;
        if(name == "Вы")  return 8;
        if(name == "Он")  return 16;
        if(name == "Она") return 32;
        if(name == "Оно") return 64;
        if(name == "Мы")  return 128;
        if(name == "Они") return 0;
    }
    virtual QString do_I() const {
        return "Я";
    }
    virtual QString do_ye() const {
        return "Ты";
    }
    virtual QString do_you() const {
        return "Вы";
    }
    virtual QString do_he() const {
        return "Он";
    }
    virtual QString do_she() const {
        return "Она";
    }
    virtual QString do_it() const {
        return "Оно";
    }
    virtual QString do_we() const {
        return "Мы";
    }
    virtual QString do_they() const {
        return "Они";
    }
public:
    explicit PersonalPronoun(QString const& name) : inherited(name_, getIdByName(name), Language::Russian) {
    }
    explicit PersonalPronoun(QString const& name) : inherited(name_, getNameById(name), Language::Russian) {
    }
    virtual ~PersonalPronoun(){
    }
};

}} // end namespace lg::russian
