/**
 * \file       classes/dialogs/simple/simple_dialog.h
 * \brief      The Simple class provides a simple dialog
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.1
 * \date       Created on 16 July 2013 y., 16:51
 * \TODO
**/
#pragma once
#include "simple_dialog_base.h"

/** \namespace lg::dialog */
namespace lg {
namespace dialog {

/**
 *  \brief The Simple class provides easy work with QTime
 *   \code
 *       using namespace lg::policy;
 *       lg::dialog::Simple<data::Simple, time::Simple, sequence::Random> simple;
 *       simple.setData(data);
 *       auto result = simple.exec();
 *       const auto time_p = simple.time_policy();
 *       qDebug() << time_p.time();
 *   \endcode
**/
template< typename T  //!< The data     policy
         ,typename Y  //!< The time     policy
         ,typename Z  //!< The sequense policy
         ,typename X  //!< The logs policy
         >
class Simple : public SimpleBase {
public:
    typedef T                                          data_policy_t;
    typedef Y                                          time_policy_t;
    typedef Z                                          sequense_policy_t;
    typedef X                                          logs_policy_t;
    typedef Simple<data_policy_t, time_policy_t,
                   sequense_policy_t, logs_policy_t>   class_name;
    typedef SimpleBase                                 inherited;

private:
    data_policy_t       data_policy_;
    time_policy_t       time_policy_;
    sequense_policy_t   sequense_policy_;
    logs_policy_t       logs_policy_;

    virtual QString doOnClickedNext() {
        const auto size = data_policy_.size();
        if(size < 1) return QString();
        auto num = sequense_policy_.next(size - 1);
        return data_policy_.showQuestion(num);
    }
    virtual QString doOnClickedAnswer() {
        return data_policy_.showAnswer();
    }
    virtual unsigned doShowRest() {
        return data_policy_.size();
    }
    bool doHasNext() const {
        return data_policy_.hasQuestion();
    }
public:
    explicit Simple(QWidget* parent = nullptr)
        : inherited(parent) {
    }
    virtual ~Simple() {
    }
    time_policy_t const& time_policy() const {
        return time_policy_;
    }
    data_policy_t const& data_policy() const {
        return data_policy_;
    }
    sequense_policy_t const& sequense_policy() const {
        return sequense_policy_;
    }
    logs_policy_t const& logs_policy() const {
        return logs_policy_;
    }
    template<typename A, typename B>
    void setData(A const& data, B const& w, Sets const& s) {
        data_policy_.setData(data, w, s);
    }
    virtual int exec() {
        firstClick();
        time_policy_.start();
        auto result = inherited::exec();
        time_policy_.stop();
        if(QDialog::Accepted == result) logs_policy_.save(time_policy_.time(), data_policy_.getWords(), data_policy_.getSets());
        return result;
    }
};

}} // end namespace lg::dialog

