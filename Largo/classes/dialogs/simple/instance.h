/**
 * \file      classes/dialogs/simple/instance.h
 * \brief     The Instance class provides an instance to the Simple dialog.
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.1
 * \date      Created on 06 July 2013 y., 16:56
 * \TODO
**/
#pragma once
#include <QDialog>
#include <QMenu>
#include <QLabel>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QPushButton>

/** namespace lg::dialog::simple */
namespace lg {
namespace dialog {
namespace simple {

template<typename T>
class Instance {
    typedef Instance  class_name;
    typedef T		  parent;

    parent*           parent_;

protected:
    void instance_form() {
        parent_->setMinimumSize(GOLDEN_SECTION * 200,200);
    }
    void instanseQuestionText() {
        auto text = parent_->question_text_   = new QLabel;
        auto font = text->font();
        font.setBold(true);
        font.setPixelSize(20);
        text->setFont(font);
    }
    void instanseAnswerText() {
        auto text = parent_->answer_text_   = new QLabel;
        auto font = text->font();
        font.setPixelSize(16);
        text->setFont(font);
    }
    void instance_widgets() {
        parent_->title_     = new QLabel;
        parent_->rest_text_ = new QLabel;

        instanseQuestionText();
        instanseAnswerText();

        auto buts = parent_->buttons_ = new QDialogButtonBox;
        buts->addButton(new QPushButton(iButton::tr("Answer")), QDialogButtonBox::YesRole);
        buts->addButton(new QPushButton(iButton::tr("Next >>")),  QDialogButtonBox::ActionRole);
        instance_layout();
    }
    void instance_layout() {
        auto lay = new QVBoxLayout(parent_);
        lay->addWidget(parent_->title_);
        lay->addStretch(8);
        lay->addWidget(parent_->question_text_, 10, Qt::AlignCenter);
        lay->addStretch();
        lay->addWidget(parent_->answer_text_, 5, Qt::AlignCenter);
        lay->addStretch(10);

        auto butt_lay = new QHBoxLayout;
        butt_lay->addWidget(parent_->rest_text_);
        butt_lay->addWidget(parent_->buttons_);
        lay->addLayout(butt_lay);
    }

public:
    explicit Instance(parent* p) : parent_(p) {
    }
    void translate() {
        parent_->title_->setText(iWord::tr("The current word:"));
    }
};
}}} // end namespace lg::dialog::simple

