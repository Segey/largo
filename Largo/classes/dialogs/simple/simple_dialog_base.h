/**
 * \file       classes/dialogs/simple/simple_dialog_base.h
 * \brief      The SimpleBase class provides a simple dialog
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.0
 * \date       Created on 16 July 2013 y., 16:51
 * \TODO
**/
#pragma once
#include <QTime>
#include <QLabel>
#include <QDialogButtonBox>
#include <QSharedPointer>
#include "instance.h"

/** namespace lg::dialog */
namespace lg {
namespace dialog {

class SimpleBase : public lg::SaveRestoreDialog {
Q_OBJECT
friend class simple::Instance<SimpleBase>;
    typedef lg::SaveRestoreDialog       inherited;
    typedef SimpleBase                  class_name;

private:
    QLabel* 		    title_;
    QLabel* 		    question_text_;
    QLabel*             answer_text_;
    QLabel*             rest_text_;
    QDialogButtonBox* 	buttons_;

    const static QLatin1String name() {
        return QLatin1String("SimpleDialog");
    }
    void instanceSignals() {
        connect(buttons_, SIGNAL(clicked(QAbstractButton*)), SLOT(onClicked(QAbstractButton*)));
    }

private slots:
    void onClicked(QAbstractButton* button) {
        const auto role = buttons_->buttonRole(button);
        if(role == QDialogButtonBox::YesRole) answer_text_->setText(doOnClickedAnswer());
        else {
            rest_text_->clear();
            answer_text_->clear();
            if(doHasNext()) question_text_->setText(doOnClickedNext());
            else done(QDialog::Accepted);
            auto num = doShowRest();   
            rest_text_->setText(iWord::tr("Rest %1 word(s)").arg(num));
        }
    }

protected:
    virtual QString doOnClickedNext()   = 0;
    virtual QString doOnClickedAnswer() = 0;
    virtual unsigned doShowRest()       = 0;
    virtual bool doHasNext() const      = 0;

public:
    explicit SimpleBase(QWidget* parent = nullptr)
        : inherited(name(), parent), title_(nullptr), question_text_(nullptr), answer_text_(nullptr), buttons_(nullptr) {
       ps::instance::Dialog< lg::dialog::simple::Instance<SimpleBase> >(this).instance();
       instanceSignals();
       inherited::settingsLocationRead();
    }
    void firstClick() {
        onClicked(nullptr);
    }
    virtual  ~SimpleBase() {
    }
};

}} // end namespace lg::dialog

