/**
 * \file       classes/dialogs/done/done_dialog.h
 * \brief      The Done class provides a Done dialog
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S.Panin
 * \version    v.1.1
 * \date       Created on 01 October 2013 y., 01:30
 * \TODO
**/
#pragma once
#include <QLabel>
#include <QDialogButtonBox>
#include "instance.h"

/** \namespace lg::dialog */
namespace lg {
namespace dialog {

/**
 *  \brief The Done class provides easy work with Done dialog
 *   \code
 *       using namespace lg::dialog;
 *       Done done(3,5);
 *       done.show();
 *   \endcode
**/
class Done: public QDialog {
    friend class done::Instance<Done>;

public:
    typedef Done    class_name;
    typedef QDialog inherited;

private:
    QLabel*           text_;
    QLabel*           result_;
    QDialogButtonBox* buttons_;
    Sets              sets_;
    QStringList       words_;

    /**
     * \brief The Words to String
    **/
    QString getWords () {
        if(words_.size() == 0) return QString();

        QString result = words_[0];
        for(int i = 1; i != words_.size(); ++i) {
            result += "," + words_[i];
        }
        return result;
    }
    void execute(unsigned success, unsigned count) {
        QString text, result;
        text += iText::tr("The type of dialog") + ":\n";              result += sets::dialogToString(sets_) + "\n";
        text += iText::tr("The word(s)") + ":\n";                     result += getWords()  + "\n";
        text += iText::tr("The language") + ":\n";                    result += sets::langToString(sets_) + "\n";
        text += iText::tr("The forms") + ":\n";                       result += sets::formToString(sets_) + "\n";
        text += iText::tr("The tenses") + ":\n";                      result += sets::tenseToString(sets_) + "\n";
        text += iText::tr("The number of correct answers") + ":\n";   result += QString("%1\n").arg(success);
        text += iText::tr("The number of questions") + ":\n";         result += QString("%1\n").arg(count);
        text += iText::tr("The percentage") + ":\n";                  result += QString("%1%\n").arg(double(success) /count * 100);
        text_->setText(text);                                         result_->setText(result);
    }

public:
    explicit Done(Sets const& s, QStringList const& words, unsigned success, unsigned count)
        : text_(nullptr), result_(nullptr), buttons_(nullptr), sets_(s), words_(words) {
        ps::instance::Dialog< lg::dialog::done::Instance<Done> >(this).instance();
        execute(success, count);
        connect(buttons_, SIGNAL(accepted()), this, SLOT(accept()));        
    }
    virtual ~Done() {
    }
};

}} // end namespace lg::dialog

