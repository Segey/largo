/**
 * \file      classes/dialogs/done/instance.h
 * \brief     The Instance class provides an instance to the All Done dialog.
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S.Panin
 * \version   v.1.2
 * \date      Created on 01 October 2013 y., 01:23
 * \TODO
**/
#pragma once
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QDialogButtonBox>

/** \namespace lg::dialog::done */
namespace lg {
namespace dialog {
namespace done {

template<typename T>
class Instance {
    typedef Instance  class_name;
    typedef T		  parent;

    parent*           parent_;

protected:
    void instance_form() {
        parent_->setMinimumSize(150, GOLDEN_SECTION * 150);
    }
    void instance_widgets() {
        parent_->text_    = new QLabel;
        parent_->result_  = new QLabel;

        parent_->buttons_ = new QDialogButtonBox(QDialogButtonBox::Ok);
        instance_layout();
    }
    void instance_layout() {
        auto vlay = new QVBoxLayout(parent_);
        auto hlay = new QHBoxLayout;
        hlay->addWidget(parent_->text_);
        hlay->addWidget(parent_->result_);

        vlay->addLayout(hlay);
        vlay->addStretch();
        vlay->addWidget(parent_->buttons_);
    }

public:
    explicit Instance(parent* p) : parent_(p) {
    }
    void translate() {
        lg::Dialog::setWindowTitle(parent_, "Done", iDialog::tr("Statistics"));
    }
};
}}} // end namespace lg::dialog::done

