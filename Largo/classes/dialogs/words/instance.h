/**
 * \file      classes/dialogs/words/instance.h
 * \brief     The Instance class provides an instance to the Words dialog.
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.1
 * \date      Created on 31 August 2013 y., 22:48
 * \TODO
**/
#pragma once
#include <QDialog>
#include <QMenu>
#include <QLabel>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QPushButton>

/** namespace lg::dialog::words */
namespace lg {
namespace dialog {
namespace words {

template<typename T>
class Instance {
    typedef Instance  class_name;
    typedef T		  parent;

    parent*           parent_;

protected:
    void instance_form() {
        parent_->setMinimumSize(GOLDEN_SECTION * 200,200);
    }
    void instanseQuestionText() {
        auto text = parent_->question_text_   = new QLabel;
        auto font = text->font();
        font.setBold(true);
        font.setPixelSize(20);
        text->setFont(font);
    }
    void instance_widgets() {
        parent_->title_question_      = new QLabel;
        parent_->rest_text_           = new QLabel;
        parent_->placeholders_        = new ps::RadioButtonGroup(2, iWord::tr("The variations of words:"), parent_);
        instanseQuestionText();

        auto buts = parent_->buttons_ =new QDialogButtonBox;
        parent_->answer_button_ = buts->addButton(iButton::tr("Answer"),   QDialogButtonBox::YesRole);
        parent_->next_button_   = buts->addButton(iButton::tr("Next >>"),  QDialogButtonBox::ActionRole);
        parent_->next_button_->setEnabled(false);
        parent_->answer_button_->setEnabled(false);
        instance_layout();
    }
    void instance_layout() {
        auto lay = new QVBoxLayout(parent_);
        lay->addWidget(parent_->title_question_);
        lay->addWidget(parent_->question_text_, 1, Qt::AlignCenter);
        lay->addWidget(parent_->placeholders_, 2);
        lay->addStretch();
        lay->addSpacing(5);

        auto butt_lay = new QHBoxLayout;
        butt_lay->addWidget(parent_->rest_text_);
        butt_lay->addWidget(parent_->buttons_);
        lay->addLayout(butt_lay);
    }

public:
    explicit Instance(parent* p) : parent_(p) {
    }
    void translate() {
        parent_->title_question_->setText(iWord::tr("The current word:"));
    }
};
}}} // end namespace lg::dialog::words

