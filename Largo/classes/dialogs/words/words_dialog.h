/**
 * \file       classes/dialogs/words/words.h
 * \brief      The Words class provides a words dialog
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.0
 * \date       Created on 31 August 2013 y., 22:54
 * \TODO
**/
#pragma once
#include "words_dialog_base.h"

/** namespace lg::dialog */
namespace lg {
namespace dialog {

/**
 *  \brief The Words class provides easy work with QTime
 *   \code
 *       using namespace lg::policy;
 *       lg::dialog::Words<data::Words, time::Simple, sequence::Random> words;
 *       words.setData(data);
 *       auto result = words.exec();
 *       const auto time_p = words.time_policy();
 *       qDebug() << time_p.time();
 *   \endcode
**/
template< typename T  //!< The data     policy
         ,typename Y  //!< The time     policy
         ,typename Z  //!< The sequense policy
         ,typename X  //!< The logs policy
         >
class Words : public WordsBase {
public:
    typedef T                                          data_policy_t;
    typedef Y                                          time_policy_t;
    typedef Z                                          sequense_policy_t;
    typedef X                                          logs_policy_t;
    typedef Words<data_policy_t, time_policy_t,
                   sequense_policy_t, logs_policy_t>   class_name;
    typedef WordsBase                                  inherited;
    typedef inherited::question_t                      question_t;

private:
    data_policy_t       data_policy_;
    time_policy_t       time_policy_;
    sequense_policy_t   sequense_policy_;
    logs_policy_t       logs_policy_;

    virtual question_t doOnClickedNext() {
        const auto size = data_policy_.size();
        if(size < 1) return question_t();
        auto num = sequense_policy_.next(size - 1);
        return data_policy_.showQuestion(num);
    }
    virtual QString doOnClickedAnswer() {
        return data_policy_.showAnswer();
    }
    virtual unsigned doShowRest() {
        return data_policy_.size();
    }
    bool doHasNext() const {
        return data_policy_.hasQuestion();
    }
    virtual unsigned doMaxVariants() const {
        return data_policy_.max_variants();
    }
    virtual QString doGetAnswer() const {
        return data_policy_.showAnswer();
    }
    virtual Sets doGetSets() const {
        return data_policy_.getSets();
    }
    virtual QStringList doGetWords() const {
        return data_policy_.getWords();
    }
public:
    explicit Words(QWidget* parent = nullptr)
        : inherited(parent) {
    }
    virtual ~Words() {
    }
    time_policy_t const& time_policy() const {
        return time_policy_;
    }
    data_policy_t const& data_policy() const {
        return data_policy_;
    }
    sequense_policy_t const& sequense_policy() const {
        return sequense_policy_;
    }
    logs_policy_t const& logs_policy() const {
        return logs_policy_;
    }
    template<typename A, typename B>
    void setData(A const& data, B const& w, Sets const& s) {
        data_policy_.setData(data, w, s);
    }
    virtual int exec() {
        firstClick(data_policy_.size());
        time_policy_.start();
        auto result = inherited::exec();
        time_policy_.stop();
        if(QDialog::Accepted == result) logs_policy_.save(time_policy_.time(), data_policy_.getWords(), data_policy_.getSets(), inherited::success(), inherited::count());
        return result;
    }
};

}} // end namespace lg::dialog

