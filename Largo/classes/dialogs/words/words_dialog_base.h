/**
 * \file       classes/dialogs/words/words_dialog_base.h
 * \brief      The WordsBase class provides a words dialog
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.0
 * \date       Created on 31 August 2013 y., 22:50
 * \TODO
**/
#pragma once
#include <QTime>
#include <QLabel>
#include <QDialogButtonBox>
#include <QSharedPointer>
#include "instance.h"

/** \namespace lg::dialog */
namespace lg {
namespace dialog {

class WordsBase : public lg::SaveRestoreDialog {
Q_OBJECT
friend class words::Instance<WordsBase>;
public:
    typedef lg::SaveRestoreDialog         inherited;
    typedef WordsBase                     class_name;
    typedef QPair<QString, QStringList>   question_t;

private:
    QLabel* 		      title_question_;
    QLabel* 		      question_text_;
    ps::RadioButtonGroup* placeholders_;
    QLabel*               rest_text_;
    QDialogButtonBox* 	  buttons_;
    QPushButton*          next_button_;
    QPushButton*          answer_button_;
    unsigned              success_;
    bool                  is_answer_;
    unsigned              count_;

    const static QLatin1String name() {
        return QLatin1String("WordsDialog");
    }
    void instanceSignals() {
        connect(buttons_,      SIGNAL(clicked(QAbstractButton*)), SLOT(onClicked(QAbstractButton*)));
        connect(placeholders_, SIGNAL(buttonClicked(int)),        SLOT(onButtonClicked(int)));
    }
    void workOut() {
        if(is_answer_) return;
        is_answer_ = true;
        auto index = placeholders_->currentId();
        if(index <0) return;
        if(doOnClickedAnswer() == placeholders_->button(index)->text())
            ++success_;
    }
    void initRadioGroup() {
        for(unsigned index = 0; index != doMaxVariants() +1; ++index)
            placeholders_->addRadioButton("", index);
    }
    void hasNext() {
        workOut();
        question_t result = doOnClickedNext();
        question_text_->setText(result.first);
        if(!placeholders_->size()) initRadioGroup();

        result.second << doGetAnswer();
        std::random_shuffle(result.second.begin(),result.second.end());

        placeholders_->group()->setExclusive(false);
        for(unsigned index = 0; index != doMaxVariants() +1; ++index) {
            placeholders_->button(index)->setText(result.second.at(index));
            placeholders_->button(index)->setChecked(false);
        }

        placeholders_->group()->setExclusive(true);
        is_answer_ = false;
    }
    void showAnswer() {
        workOut();
        auto text = doOnClickedAnswer();
        for(unsigned index = 0; index != doMaxVariants() + 1; ++index) {
            if(placeholders_->button(index)->text() == text) {
                placeholders_->button(index)->setChecked(true);
                return;
            }
        }
    }

private slots:
    void onClicked(QAbstractButton* button) {
        const auto role = buttons_->buttonRole(button);
        if(role == QDialogButtonBox::YesRole) showAnswer();
        else {
            rest_text_->clear();
            next_button_->setEnabled(false);
            answer_button_->setEnabled(false);
            if(doHasNext()) hasNext();
            else {
                inherited::hide();
                lg::dialog::Done d(doGetSets(), doGetWords(), success_,count_);
                d.exec();
                inherited::done(QDialog::Accepted);
            }
            auto num = doShowRest();   
            rest_text_->setText(iWord::tr("Rest %1 word(s)").arg(num));
        }
    }
    void onButtonClicked(int) {
        next_button_->setEnabled(true);
        answer_button_->setEnabled(true);
    }

protected:
    virtual question_t doOnClickedNext()   = 0;
    virtual QString doOnClickedAnswer()    = 0;
    virtual unsigned doShowRest()          = 0;
    virtual bool doHasNext() const         = 0;
    virtual unsigned doMaxVariants() const = 0;
    virtual QString doGetAnswer() const    = 0;
    virtual Sets doGetSets() const         = 0;
    virtual QStringList doGetWords() const = 0;

    unsigned success() const {
        return success_;
    }
    unsigned count() const {
        return count_;
    }

public:
    explicit WordsBase(QWidget* parent = nullptr)
        : inherited(name(), parent), title_question_(nullptr), question_text_(nullptr), placeholders_(nullptr),
          buttons_(nullptr), next_button_(nullptr), success_(0), is_answer_(false), count_(0) {
       ps::instance::Dialog< lg::dialog::words::Instance<WordsBase> >(this).instance();
       instanceSignals();
       inherited::settingsLocationRead();
    }
    void firstClick(unsigned count) {
        count_ = count;
        onClicked(nullptr);
    }
    virtual ~WordsBase() {
    }
};

}} // end namespace lg::dialog

