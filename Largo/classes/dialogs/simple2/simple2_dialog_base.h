/**
 * \file       classes/dialogs/simple2/simple2_dialog_base.h
 * \brief      The Simple2Base class provides a simple2 dialog
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.1
 * \date       Created on 16 July 2013 y., 16:51
 * \TODO
**/
#pragma once
#include <QTime>
#include <QTimer>
#include <QLabel>
#include <QSpinBox>
#include <QDialogButtonBox>
#include <QSharedPointer>
#include "instance.h"

/** namespace lg::dialog */
namespace lg {
namespace dialog {

class Simple2Base : public lg::SaveRestoreDialog {
Q_OBJECT
friend class simple2::Instance<Simple2Base>;
    typedef lg::SaveRestoreDialog       inherited;
    typedef Simple2Base                 class_name;

private:
    QLabel* 		    title_;
    QLabel* 		    question_text_;
    QLabel*             answer_text_;
    QLabel*             rest_text_;
    QTimer*             answer_timer_;
    QTimer*             sleep_timer_;
    QLabel*             delay_text_;
    QSpinBox*           spin_box_;
    QDialogButtonBox* 	buttons_;

    const static QLatin1String name() {
        return QLatin1String("Simple2Dialog");
    }
    void setAnswer() {
        answer_text_->setText(doOnClickedAnswer());
    }
    void instanceSignals() {
        connect(buttons_,      SIGNAL(clicked(QAbstractButton*)), SLOT(onClicked(QAbstractButton*)));
        connect(answer_timer_, SIGNAL(timeout()), SLOT(showAnswer()));
        connect(sleep_timer_,  SIGNAL(timeout()), SLOT(showNextQuestion()));
    }
    void readSpinBoxDataFromDB() {
        using namespace lg::settings;
        auto s2 = Settings2::fromDB(module::settings::settings2::simple_simple2(), "3");
        auto value = s2.value().toUInt();
        spin_box_->setValue(value);
    }
    void writeSpinBoxDataToDB() const {
        using namespace lg::settings;
        auto str = QString("%1").arg(spin_box_->value());
        Settings2::toDB(Settings2(module::settings::settings2::simple_simple2(), str));
    }

private slots:
    void onClicked(QAbstractButton* /* button */) {
        if(doHasNext()) {
            if(answer_text_->text().isEmpty()) {
                showAnswer();
                sleep_timer_->start(600);
            } else showNextQuestion();
            return;
        }
        writeSpinBoxDataToDB();
        done(QDialog::Accepted);
    }
    void showAnswer() {
        setAnswer();
        answer_timer_->stop();
    }
    void showNextQuestion() {
        sleep_timer_->stop();
        rest_text_->clear();
        answer_text_->clear();
        question_text_->setText(doOnClickedNext());

        auto num = doShowRest();
        rest_text_->setText(iWord::tr("Rest %1 word(s)").arg(num));
        answer_timer_->start(spin_box_->value() * 1000);
    }

protected:
    virtual QString doOnClickedNext()   = 0;
    virtual QString doOnClickedAnswer() = 0;
    virtual unsigned doShowRest()       = 0;
    virtual bool doHasNext() const      = 0;

public:
    explicit Simple2Base(QWidget* parent = nullptr)
        : inherited(name(), parent), title_(nullptr), question_text_(nullptr), answer_text_(nullptr),
          answer_timer_(nullptr), sleep_timer_(nullptr), delay_text_(nullptr),  spin_box_(nullptr), buttons_(nullptr) {
       ps::instance::Dialog< lg::dialog::simple2::Instance<Simple2Base> >(this).instance();
       instanceSignals();
       inherited::settingsLocationRead();
       readSpinBoxDataFromDB();
    }
    void firstClick() {
        showNextQuestion();
    }
    virtual  ~Simple2Base() {
        writeSpinBoxDataToDB();
        answer_timer_->stop();
        sleep_timer_->stop();
    }
};

}} // end namespace lg::dialog

