/**
 * \file      classes/dialogs/simple2/instance.h
 * \brief     The Instance class provides an instance to the Simple dialog.
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.1
 * \date      Created on 06 July 2013 y., 16:56
 * \TODO
**/
#pragma once
#include <QDialog>
#include <QMenu>
#include <QLabel>
#include <QVBoxLayout>
#include <QSpinBox>
#include <QDialogButtonBox>
#include <QPushButton>

/** namespace lg::dialog::simple2 */
namespace lg {
namespace dialog {
namespace simple2 {

template<typename T>
class Instance {
    typedef Instance  class_name;
    typedef T		  parent;

    parent*           parent_;

protected:
    void instance_form() {
        parent_->setMinimumSize(GOLDEN_SECTION * 200,200);
    }
    void instanseQuestionText() {
        auto text = parent_->question_text_ = new QLabel;
        auto font = text->font();
        font.setBold(true);
        font.setPixelSize(20);
        text->setFont(font);
    }
    void instanseAnswerText() {
        auto text = parent_->answer_text_ = new QLabel;
        auto font = text->font();
        font.setPixelSize(16);
        text->setFont(font);
    }
    void instanceTimers() {
        parent_->sleep_timer_ = new QTimer(parent_);
        parent_->answer_timer_ = new QTimer(parent_);
    }
    void instanceSpinBox() {
        parent_->delay_text_ = new QLabel();

        auto box = parent_->spin_box_ = new QSpinBox();
        box->setMaximum(20);
        box->setMinimum(1);
    }
    void instance_widgets() {
        parent_->title_     = new QLabel;
        parent_->rest_text_ = new QLabel;

        instanseQuestionText();
        instanseAnswerText();
        instanceTimers();
        instanceSpinBox();

        auto buts = parent_->buttons_ = new QDialogButtonBox;
        buts->addButton(new QPushButton(iButton::tr("Next >>")),  QDialogButtonBox::ActionRole);
        instance_layout();
    }
    QVBoxLayout* instance_delay_layout() {
        auto lay = new QHBoxLayout;
        lay->addWidget(parent_->delay_text_);
        lay->addWidget(parent_->spin_box_);

        auto del_lay = new QVBoxLayout;
        del_lay->addWidget(parent_->rest_text_);
        del_lay->addLayout(lay);
        return del_lay;

    }

    void instance_layout() {
        auto lay = new QVBoxLayout(parent_);
        lay->addWidget(parent_->title_);
        lay->addStretch(10);
        lay->addWidget(parent_->question_text_, 10, Qt::AlignCenter);
        lay->addStretch();
        lay->addWidget(parent_->answer_text_, 5, Qt::AlignCenter);
        lay->addStretch(10);

        auto butt_lay = new QHBoxLayout;
        butt_lay->addLayout(instance_delay_layout());
        butt_lay->addWidget(parent_->buttons_);
        lay->addLayout(butt_lay);
    }

public:
    explicit Instance(parent* p) : parent_(p) {
    }
    void translate() {
        parent_->title_->setText(iWord::tr("The current word:"));
        parent_->delay_text_->setText(iButton::tr("Delay time(sec):"));
    }
};
}}} // end namespace lg::dialog::simple2

