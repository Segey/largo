/**
 * \file      classes/dialogs/birds2/instance.h
 * \brief     The Instance class provides an instance to the Birds2 dialog.
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S.Panin
 * \version   v.1.1
 * \date      Created on 05 August 2013 y., 01:07
 * \TODO
**/
#pragma once
#include <QDialog>
#include <QMenu>
#include <QLabel>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QPushButton>

/** namespace lg::dialog::birds2 */
namespace lg {
namespace dialog {
namespace birds2 {

template<typename T>
class Instance {
    typedef Instance  class_name;
    typedef T		  parent;

    parent*           parent_;

protected:
    void instance_form() {
        parent_->setMinimumSize(GOLDEN_SECTION * 200,200);
    }
    void instanseQuestionText() {
        auto text = parent_->question_text_ = new QLabel;
        auto font = text->font();
        font.setBold(true);
        font.setPixelSize(20);
        text->setFont(font);
    }
    void instanseAnswerText() {
        parent_->answer_text_ = new QLabel;
    }
    void instanseButtons() {
       parent_->right_button_ = new QPushButton(iButton::tr("True"));
       parent_->left_button_ = new QPushButton(iButton::tr("False"));
    }

    void instance_widgets() {
        parent_->title_     = new QLabel;
        parent_->rest_text_ = new QLabel;

        instanseQuestionText();
        instanseAnswerText();
        instanseButtons();
        instance_layout();
    }
    void instance_layout() {
        auto center = new QHBoxLayout;
        center->addStretch();
        center->addWidget(parent_->left_button_);
        center->addStretch();
        center->addWidget(parent_->right_button_);
        center->addStretch();

        auto lay = new QVBoxLayout(parent_);
        lay->addWidget(parent_->title_);
        lay->addStretch(2);
        lay->addWidget(parent_->question_text_, 0, Qt::AlignCenter);
        lay->addWidget(parent_->answer_text_, 0, Qt::AlignCenter);
        lay->addStretch(3);
        lay->addLayout(center);
        lay->addStretch(2);
        lay->addWidget(parent_->rest_text_, 0, Qt::AlignLeft);
    }

public:
    explicit Instance(parent* p) : parent_(p) {
    }
    void translate() {
        parent_->title_->setText(iWord::tr("The current word:"));
    }
};
}}} // end namespace lg::dialog::birds2

