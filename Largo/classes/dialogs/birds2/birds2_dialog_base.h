/**
 * \file       classes/dialogs/birds2/birds2_dialog_base.h
 * \brief      The Birds2Base class provides a birds dialog
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.1
 * \date       Created on 05 August 2013 y., 01:12
 * \TODO
**/
#pragma once
#include <QTime>
#include <QLabel>
#include <QDialogButtonBox>
#include <QSharedPointer>
#include "instance.h"

/** namespace lg::dialog */
namespace lg {
namespace dialog {

class Birds2Base : public lg::SaveRestoreDialog {
Q_OBJECT
friend class birds2::Instance<Birds2Base>;
    typedef lg::SaveRestoreDialog       inherited;
    typedef Birds2Base                  class_name;
    typedef QPair<QString,QString>      question_t;

private:
    QLabel* 		title_;
    QLabel* 		question_text_;
    QLabel* 		answer_text_;
    QLabel*         rest_text_;
    QPushButton* 	left_button_;
    QPushButton* 	right_button_;
    question_t      current_;
    unsigned        success_;
    unsigned        count_;

    const static QLatin1String name() {
        return QLatin1String("Bird2Dialog");
    }
    void instanceSignals() {
        connect(left_button_,  SIGNAL(clicked(bool)), SLOT(onClickedFalse(bool)));
        connect(right_button_, SIGNAL(clicked(bool)), SLOT(onClickedTrue(bool)));
    }
    void next() {
        rest_text_->clear();
        answer_text_->clear();
        if(doHasNext()) {
            current_ = doOnClickedNext();
            question_text_->setText(current_.first);
            answer_text_->setText(current_.second);
        }
        else {
            inherited::hide();
            lg::dialog::Done d(doGetSets(), doGetWords(), success_,count_);
            d.exec();
            done(QDialog::Accepted);
        }
        auto num = doShowRest();
        rest_text_->setText(iWord::tr("Rest %1 word(s)").arg(num));
    }
    bool isAnswerCorrect() const {
        return current_.second == doOnClickedAnswer();
    }

private slots:
    void onClickedFalse(bool) {
        auto done = isAnswerCorrect();
        if(!done) ++success_;
        next();
    }
    void onClickedTrue(bool) {
        auto done = isAnswerCorrect();
        if(done) ++success_;
        next();
    }

protected:
    virtual question_t doOnClickedNext()       = 0;
    virtual QString doOnClickedAnswer() const  = 0;
    virtual unsigned doShowRest()              = 0;
    virtual bool doHasNext() const             = 0;
    virtual Sets doGetSets() const             = 0;
    virtual QStringList doGetWords() const     = 0;

public:
    explicit Birds2Base(QWidget* parent = nullptr)
        : inherited(name(), parent), title_(nullptr), question_text_(nullptr), answer_text_(nullptr), rest_text_(nullptr),
          left_button_(nullptr), right_button_(nullptr), success_(0), count_(0) {
       ps::instance::Dialog< lg::dialog::birds2::Instance<Birds2Base> >(this).instance();
       instanceSignals();
       inherited::settingsLocationRead();
    }
    unsigned success() const {
        return success_;
    }
    unsigned count() const {
        return count_;
    }
    void firstClick(unsigned count) {
        count_ = count;
        next();
    }
    virtual  ~Birds2Base() {
    }
};

}} // end namespace lg::dialog

