/**
 * \file      classes/errors/pillar.h 
 * \brief     The Class pillar error represents problems with pillar sequence db 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   March (the) 11(th), 2014, 02:07 MSK
 * \updated   March (the) 11(th), 2014, 02:07 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class pillar : public base {
    typedef pillar class_name;
    typedef base   inherited;

   virtual unsigned type() const override {
       return inherited::critical; 
   }
   virtual std::string name() const override {
       return "pillar";
   }
   explicit pillar(str_t const& info, str_t const& location, place const& place)
        : inherited(info, place) {
    }
public:
   static pillar none_next(str_t const& location, place const& place) {
        return pillar("Can't obtain the next pillar id.", location, place);
   }
};
}}// end namespace zz::exception

