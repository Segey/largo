/**
 * \file      classes/errors/address.h 
 * \brief     The Class address error represents problems with addresses
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   March (the) 12(th), 2014, 23:59 MSK
 * \updated   March (the) 12(th), 2014, 23:59 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class address : public base {
    typedef address class_name;
    typedef base    inherited;

   virtual unsigned type() const override {
       return inherited::error; 
   }
   virtual std::string name() const override {
       return "address";
   }
   explicit address(str_t const& info, str_t const& location, place const& place)
        : inherited(info, location, place) {
   }
public:
   static address empty_street(str_t const& location, place const& place) {
        return address("The street param is empty.", location, place);
   }
   static address bad_table_id(int table_id, str_t const& location, place const& place) {
        address cls("The table_id less than 1.", location, place);
        cls.addAdditionMessage("table_id", table_id);
        return cls;
   }
};
}}// end namespace zz::exception

