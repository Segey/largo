/**
 * \file      classes/errors/conversion.h 
 * \brief     The Bad Conversion class provides incorrect convert(coding, encoding)
 * \author    S.Panin <serg.p@nanoflash.biz>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.2
 * \created   February (the) 16(th), 2014, 23:04 MSK
 * \updated   February (the) 16(th), 2014, 23:04 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class conversion : public base {
    typedef conversion class_name;
    typedef base       inherited;

   virtual unsigned type() const {
       return inherited::error; 
   }
   virtual str_t name() const {
       return "conversion";
   }
   explicit conversion(str_t const& m, place const& place)
       :  inherited(m, place) {
   }
   explicit conversion(str_t const& m, str_t const& location, place const& place)
       :  inherited(m, location, place) {
   }
public:
    static conversion bad(str_t const& original_string, str_t const& description, place const& place) {
        conversion cls("The bad string conversion.", place);
        cls.addAdditionMessage("orginal_string", original_string);
        cls.addAdditionMessage("context", description);
        return cls;
    }
};

}} // end namespace zz::exception

