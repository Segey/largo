/**
 * \file      classes/errors/body_empty.h 
 * \brief     The BodyEmpty class provides the exception when a Body Request is empty
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   February (the) 16(th), 2014, 19:29 MSK
 * \updated   February (the) 16(th), 2014, 19:29 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class body_empty : public base {
    typedef body_empty  class_name;
    typedef base        inherited;

   virtual unsigned type() const override {
       return inherited::error; 
   }
   virtual std::string name() const override {
       return "body_empty";
   }
public:
    explicit body_empty(place const& place)
        : inherited("The Request body is empty.", place) {
    }
};

}} // end namespace zz::exception

