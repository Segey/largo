/**
 * \file      classes/errors/json.h 
 * \brief     The JSON error class represents a problem in incmoming json  
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   February (the) 20(th), 2014, 23:23 MSK
 * \updated   February (the) 20(th), 2014, 23:23 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class json : public base {
    typedef json class_name;
    typedef base inherited;

    virtual unsigned type() const override {
       return inherited::error; 
    }
    virtual str_t name() const override {
       return "json";
    }
    explicit json(str_t const& value, place const& place)
       : inherited(value, place) {
    }
public:
    static json version(unsigned version, place const& place) {
        json json ("The version of json data doesn't equal to 1", place);
        json.addAdditionMessage("version", version);
        return json;
    }
    static json empty_version(place const& place) {
        return json("The version of json data musn't be empty.", place);
    }
};

}} // end namespace zz::exception

