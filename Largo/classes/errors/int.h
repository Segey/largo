/**
 * \file      classes/errors/int_string.h 
 * \brief     The class Int provides the complex mix errors in wrork with string
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.1
 * \created   February (the) 08(th), 2014, 13:49 MSK
 * \updated   February (the) 22(th), 2014, 12:57 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class Int : public base {
    typedef Int  class_name;
    typedef base inherited;

    virtual unsigned type() const {
       return inherited::error; 
    }
    virtual str_t name() const {
       return "Int";
    }
    explicit Int(str_t const& value, std::string const& location, place const& place)
       : inherited(value, location, place) {
    }
public:
    static Int min(int min, int value, str_t const& location, place const& place) {
       Int cls("The int value less than the min value.", location, place);
       cls.addAdditionMessage("value", value);
       cls.addAdditionMessage("min", min);
       return cls;
    }
    static Int max(int max, int value, str_t const& location, place const& place) {
       Int cls("The int value more than the max value.", location, place);
       cls.addAdditionMessage("value", value);
       cls.addAdditionMessage("max", max);
       return cls;
    }   
};

}} // end namespace zz::exception

