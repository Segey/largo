/**
 * \file      classes/errors/base.h 
 * \brief     The Base Error class provides the total functionality 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.1
 * \created   February (the) 16(th), 2014, 13:48 MSK
 * \updated   February (the) 22(th), 2014, 13:28 MSK
 * \TODO      
**/
#pragma once
#include <cassert>
#include <string>
#include <sstream>
#include "place.h"

/** \namespace zz::exception */
namespace zz  {
namespace exception  {

class base {
    typedef base                    class_name;
    typedef std::pair<str_t, str_t> pair_t;
    typedef std::map<str_t,  str_t> map_t;

    map_t map_;
    str_t message_;
    str_t location_;
    place place_;

protected:
   virtual unsigned    type() const = 0;
   virtual std::string name() const = 0;
        
    enum type_t { none = 0, info = 1, warining = 3, error = 2, critical = 4 }; 

    std::string typeToStr(unsigned type) const {
        assert(none < type && type < critical && "unknown type of an exception"); 

        if(type == info)     return "INFO";
        if(type == warining) return "WARNING";
        if(type == error)    return "ERROR";
        if(type == critical) return "CRITICAL";
        return "NONE";
    }
    void setMessage(std::string const& m) {
        message_ = m;
    }
    std::string message() const {
        return message_;
    }
    void addAdditionMessage(std::string const& key, std::string const& value) {
        map_.insert(pair_t(key, value));    
    }
    template <typename T>
    void addAdditionMessage(std::string const& key, T value) {
        map_.insert(pair_t(key, boost::lexical_cast<str_t>(value)));    
    }
    void addAdditionMessage(map_t const& map) {
        map_.insert(map.begin(), map.end());    
    }
    std::string createAdditionMessagesToJSON() const {
        std::string str;
        std::for_each(map_.begin(),map_.end(),[&](pair_t const& p){
            str += std::string(", \"") + p.first + "\" : \"" + p.second + "\"";
        });
        return str;
    }
    std::string createAdditionMessagesWhat() const {
        std::string str;
        std::for_each(map_.begin(),map_.end(),[&](pair_t const& p){
            str += p.second + std::string(" : ");
        });
        return str;
    }
public:
    base(str_t const& m, place const& place)
        :  message_(m), location_("none"), place_(place) {
    }
    base(std::string const& m, str_t const& location, place const& place)
        :  message_(m), location_(location), place_(place) {
    }
    virtual ~base() {
    }
    std::string what() const {
        std::string const& m = message() == std::string() ? std::string() : message() + " : ";
        std::ostringstream out;
        out << "[time] : " 
            << typeToStr(type()) << " : "
            << name() << " : "
            << m
            << createAdditionMessagesWhat()
            << place_.what();
        return out.str();
    }
    std::string whatToJSON() const {
        std::ostringstream out;
        out << "{" 
                << " time: \""         << typeToStr(type()) << "\""
                << ", type: \""        << typeToStr(type()) << "\""
                << ", name: \""        << name()  << "\""
                << ", message: \""     << message_  << "\""
                << ", location: \""    << location_  << "\""
                << createAdditionMessagesToJSON()
                << ", place: {" 
                    << place_.toJSON()
                << "}" 
            << "}";
        return out.str();
    }
};
    
}} // end namespace zz::exception
