/**
 * \file      classes/errors/table.h 
 * \brief     The class int validate error represent problems in the getting values; 
 * \author    S.Panin <serg.p@nanoflash.biz>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.4
 * \created   February (the) 16(th), 2014, 14:29 MSK
 * \updated   February (the) 23(th), 2014, 00:12 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class table : public base {
    typedef table class_name;
    typedef base  inherited;

   virtual unsigned type() const override {
       return inherited::critical; 
   }
   virtual str_t name() const override {
       return "table";
   }
   explicit table(str_t const& info, place const& place)
        : inherited(info, place) {
    }
   explicit table(str_t const& info, str_t const& location, place const& place)
        : inherited(info, location, place) {
    }
public:
   static table doesnot_exist(str_t const& name, place const& place) {
        table t("The table's name doesn't exist.", place);
        t.addAdditionMessage("table_name", name);
        return t;
   }
   static table doesnot_exist(int table_id, place const& place) {
        table t("The table's id doesn't exist.", place);
        t.addAdditionMessage("table_id", table_id);
        return t;
   }
};

}} // end namespace zz::exception

