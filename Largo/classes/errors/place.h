/**
 * \file      classes/errors/place.h 
 * \brief     The Error Info class shows us the place info
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.1
 * \created   February (the) 13(th), 2014, 23:10 MSK
 * \updated   February (the) 18(th), 2014, 00:38 MSK
 * \TODO      
**/
#pragma once

/** \namespace zz */
namespace zz  {

class place {
    typedef place class_name;

    std::string file_;
    std::string fun_;
    int         line_;

public:
    place(std::string const& file, std::string const& fun, int line)
        : file_(file), fun_(fun), line_(line) {
    }
    std::string file() const {
        return file_;
    }
    std::string fun() const {
        return fun_;
    }
    int line() const {
        return line_;
    }
    str_t what() const {
        std::ostringstream out;
        out << file_ << " : "  << fun_ <<  " : " << line_;
        return out.str();
    }
    str_t toJSON() const {
        std::ostringstream out;
        out << "{" 
                << "file_: \""    << file_ <<"\""
                << "function: \"" << fun_  <<"\""
                << "line: \""     << line_ <<"\""
            << "}";
        return out.str();
    }
};
    
#define PLACE_INFO (zz::place(__FILE__, __FUNCTION__, __LINE__))
//#define PLACE_INFO (place(__FILE__, __FUNCTION__, __LINE__))

} // end namespace zz
