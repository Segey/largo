/**
 * \file      classes/errors/password.h 
 * \brief     The class password provides the complex mix errors in work with a password
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.1
 * \created   February (the) 27(th), 2014, 00:04 MSK
 * \updated   February (the) 27(th), 2014, 00:04 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class password : public base {
    typedef password class_name;
    typedef base         inherited;

    virtual unsigned type() const override {
       return inherited::error; 
    }
    virtual str_t name() const override {
       return "password";
    }
    explicit password(str_t const& value, str_t const& location, place const& place)
       : inherited(value, location, place) {
    }
public:
    static password min(int min, int current_size, str_t const& value, str_t const& location, place const& place) {
       password cls("The password value size less than the min value.", location, place);
       cls.addAdditionMessage("value", value);
       cls.addAdditionMessage("min", min);
       cls.addAdditionMessage("current_size", current_size);
       return cls;
    }
    static password max(int max, int current_size, str_t const& value, str_t const& location, place const& place) {
       password cls("The password value size more than the max value.", location, place);
       cls.addAdditionMessage("value", value);
       cls.addAdditionMessage("max", max);
       cls.addAdditionMessage("current_size", current_size);
       return cls;
    }    
    static password incorrect(str_t const& value, str_t const& location, place const& place) {
       password cls("The password value isn't correct.", location, place);
       cls.addAdditionMessage("value", value);
       return cls;
    }    
};

}} // end namespace zz::exception

