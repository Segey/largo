/**
 * \file      classes/errors/login.h 
 * \brief     The Class login error represent problems in the login functions
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.3
 * \created   February (the) 25(th), 2014, 01:26 MSK
 * \updated   February (the) 25(th), 2014, 01:26 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class login : public base {
    typedef login class_name;
    typedef base  inherited;

   virtual unsigned type() const override {
       return inherited::error; 
   }
   virtual str_t name() const override {
       return "login";
   }
   explicit login(str_t const& info, place const& place)
        : inherited(info, place) {
   }

   explicit login(str_t const& info, str_t const& location, place const& place)
        : inherited(info, location, place) {
   }
public:
   static login isnot_login(place const& place) {
        return login("The user isn't login.", place);
   }
   static login isnot_login(str_t const& location, place const& place) {
        return login("The user isn't login.", location, place);
   }
   static login is_login(unsigned user_id, place const& place) {
        login cls("The user is login.", place);
        cls.addAdditionMessage("user_id", user_id);
        return cls;
   }
   static login is_login(unsigned user_id, str_t const& location, place const& place) {
        login cls("The user is login.", location, place);
        cls.addAdditionMessage("user_id",user_id);
        return cls;
   }
};

}} // end namespace zz::exception

