/**
 * \file      classes/errors/cookie.h 
 * \brief     The class cookie error represents a cookie check problems;
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.1
 * \created   February (the) 18(th), 2014, 02:25 MSK
 * \updated   February (the) 18(th), 2014, 02:25 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class cookie : public base {
   typedef cookie class_name;
   typedef base       inherited;

   virtual unsigned type() const {
       return inherited::error; 
   }
   virtual std::string name() const {
       return "cookie";
   }
    explicit cookie(str_t const& info, place const& place)
        : inherited(info, place) {
    }
public:
    static cookie empty(place const& place) {
        return cookie("The cookie with name 'a' mustn't be empty.", place);
    }
    static cookie incorrect(str_t const& cookie_key_name, place const& place) {
        cookie cookie("The cookie key name doesn't equal to 'a'.", place); 
        cookie.addAdditionMessage("cookie_key_name", cookie_key_name);
        return cookie;
    }
};

}} // end namespace zz::exception

