/**
 * \file      classes/errors/not_post.h 
 * \brief     The NotPost class provides the Not Post request
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   February (the) 16(th), 2014, 18:35 MSK
 * \updated   February (the) 16(th), 2014, 18:35 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class not_post : public base {
    typedef not_post  class_name;
    typedef base      inherited;

   virtual unsigned type() const override {
       return inherited::error; 
   }
   virtual std::string name() const override {
       return "not_post";
   }
public:
    explicit not_post(str_t const& request_type, place const& place)
        : inherited("It isn't a post request.", place) {
            inherited::addAdditionMessage("request_type", request_type);
    }
};

}} // end namespace zz::exception

