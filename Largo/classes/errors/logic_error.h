/**
 * \file      C:\projects\www\server\classes\errors\logic_error.h 
 * \brief     The logic class provides the complex mix of programmer errors
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.1
 * \created   February (the) 23(th), 2014, 13:23 MSK
 * \updated   February (the) 23(th), 2014, 13:23 MSK
 * \TODO      
**/
#pragma once
#include "base.h"
#include "classes/shims/to_string.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class logic_error : public base {
    typedef logic_error class_name;
    typedef base        inherited;

   virtual unsigned type() const override {
       return inherited::critical; 
   }
   virtual std::string name() const override {
       return "logic_error";
   }
   explicit logic_error(str_t const& info, str_t const& location, place const& place)
        : inherited(info, place) {
    }
public:
   template<typename T>
   static logic_error minmax(T const& t, int min, int max, str_t const& location, place const& place) {
       logic_error cls("The min value more than the max value.", location, place);
       cls.addAdditionMessage("type", toString(t));
       cls.addAdditionMessage("min", min);
       cls.addAdditionMessage("max", max);
       return cls;
   }
};

}} // end namespace zz::exception

