/**
 * \file      classes/errors/user.h 
 * \brief     The Class user error represents problems in user 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.1
 * \created   February (the) 27(th), 2014, 01:16 MSK
 * \updated   March (the) 04(th), 2014, 01:10 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class user : public base {
    typedef user class_name;
    typedef base inherited;

   virtual unsigned type() const override {
       return inherited::error; 
   }
   virtual std::string name() const override {
       return "user";
   }
   explicit user(str_t const& info,  str_t const& location, place const& place)
        : inherited(info, location, place) {
    }
public:
   static user exists(str_t const& name, str_t const& location, place const& place) {
        user cls("The user exists.", location, place);
        cls.addAdditionMessage("user_name", name);
        return cls;
   }
   static user doesnt_exist(unsigned user_id, str_t const& location, place const& place) {
        user cls("The user doesn't exists.", location, place);
        cls.addAdditionMessage("user_id", user_id);
        return cls;
   }
   static user doesnt_exist(str_t const& name, str_t const& location, place const& place) {
        user cls("The user doesn't exists.", location, place);
        cls.addAdditionMessage("user_name", name);
        return cls;
   }
};
}}// end namespace zz::exception

