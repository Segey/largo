/**
 * \file      /home/dix/Projects/kuz/www/server/classes/errors/request.h 
 * \brief     The File request represents problems with Requests
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   March (the) 30(th), 2014, 01:46 MSK
 * \updated   March (the) 30(th), 2014, 01:46 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class request : public base {
    typedef request class_name;
    typedef base    inherited;

   virtual unsigned type() const override {
       return inherited::error; 
   }
   virtual std::string name() const override {
       return "request";
   }
   explicit request(str_t const& info, str_t const& location, place const& place)
        : inherited(info, location, place) {
   }
public:
   static request empty(str_t const& location, place const& place) {
        return request("The request is empty.", location, place);
   }
};
}}// end namespace zz::exception

