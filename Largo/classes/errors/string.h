/**
 * \file      classes/errors/string.h 
 * \brief     The class String provides the complex mix errors in wrork with string
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   February (the) 08(th), 2014, 13:49 MSK
 * \updated   February (the) 22(th), 2014, 12:57 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class String : public base {
    typedef String class_name;
    typedef base       inherited;

    virtual unsigned type() const override {
       return inherited::error; 
    }
    virtual str_t name() const override {
       return "String";
    }
    explicit String(str_t const& value, str_t const& location, place const& place)
       : inherited(value, location, place) {
    }
public:
    static String min(int min, int current_size, str_t const& value, str_t const& location, place const& place) {
       String cls("The string value size less than the min value.", location, place);
       cls.addAdditionMessage("value", value);
       cls.addAdditionMessage("min", min);
       cls.addAdditionMessage("current_size",current_size);
       return cls;
    }
    static String max(int max, int current_size, str_t const& value, std::string const& location, place const& place) {
       String cls("The string value size more than the max value.", location, place);
       cls.addAdditionMessage("value", value);
       cls.addAdditionMessage("max", max);
       cls.addAdditionMessage("current_size", current_size);
       return cls;
    }    
};

}} // end namespace zz::exception

