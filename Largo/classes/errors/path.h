/**
 * \file      classes/errors/path.h 
 * \brief     The class int validate error represent problems in the getting values; 
 * \author    S.Panin <serg.p@nanoflash.biz>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.4
 * \created   February (the) 16(th), 2014, 14:29 MSK
 * \updated   February (the) 23(th), 2014, 00:12 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class path : public base {
    typedef path class_name;
    typedef base inherited;

   virtual unsigned type() const override {
       return inherited::error; 
   }
   virtual str_t name() const override {
       return "path";
   }
   explicit path(str_t const& info, place const& place)
        : inherited(info, place) {
    }
   explicit path(str_t const& info, str_t const& location, place const& place)
        : inherited(info, location, place) {
    }
public:
   static path empty(place const& place) {
        return path("The path musn't be empty.", place);
   }
   static path not_support(str_t const& location, place const& place) {
        return path("The path's never supported yet.", location, place);
   }
   static path incorrect(str_t const& location, place const& place) {
        return path("The path mustn't contain the '..' string.", location, place);
   }
};

}} // end namespace zz::exception

