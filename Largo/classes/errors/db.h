/**
 * \file      classes/errors/db.h 
 * \brief     The Class db error represents work problem in db
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   February (the) 27(th), 2014, 01:44 MSK
 * \updated   February (the) 27(th), 2014, 01:44 MSK
 * \TODO      
**/
#pragma once
#include "base.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class db : public base {
    typedef db   class_name;
    typedef base inherited;

   virtual unsigned type() const override {
       return inherited::error; 
   }
   virtual std::string name() const override {
       return "db";
   }
   explicit db(str_t const& info, str_t const& location, place const& place)
        : inherited(info, location, place) {
    }
public:
   static db bad_select(str_t const& field, str_t const& location, place const& place) {
        db cls("The bad select.", location, place);
        cls.addAdditionMessage("field", field);
        return cls;
   }
};

}} // end namespace zz::exception

