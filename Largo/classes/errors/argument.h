/**
 * \file      classes/errors/argument.h 
 * \brief     The Class argument error represents problems with arguments sequence db 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   March (the) 12(th), 2014, 01:23 MSK
 * \updated   March (the) 12(th), 2014, 01:23 MSK
 * \TODO      
**/
#pragma once
#include "base.h"
#include "classes/shims/to_string.h"

/** \namespace zz::exception */
namespace zz {
namespace exception  {

class argument : public base {
    typedef argument class_name;
    typedef base     inherited;

   virtual unsigned type() const override {
       return inherited::error; 
   }
   virtual std::string name() const override {
       return "argument";
   }
   explicit argument(str_t const& info, str_t const& location, place const& place)
        : inherited(info, location, place) {
    }
public:
   template<typename T>
   static argument empty(T const& t, str_t const& location, place const& place) {
        argument cls("The argument is empty.", location, place);
        cls.addAdditionMessage("type", toString(t));
        return cls;
   }
};
}}// end namespace zz::exception

