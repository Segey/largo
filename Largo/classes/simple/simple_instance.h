/**
 * \file      classes/rote_learningn/rote_learning_instance.h
 * \brief     The Instance class provides an instance to the SimpleMainWindow.
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.6
 * \date      Created on 06 July 2013 y., 00:35
 * \TODO
**/
#pragma once
#include <QMenu>
#include <QMenuBar>

/** \namespace lg::simple */
namespace lg {
namespace simple {

template<typename T>
class Instance {
    typedef Instance  class_name;
    typedef T		  parent;

    parent*           parent_;

    void instance_words_list() const {
        parent_->addDockWidget(Qt::RightDockWidgetArea, parent_->dock_simple_words_ = new simple::SimpleWord(parent_));
    }
    void instance_options() const {
        parent_->addDockWidget(Qt::LeftDockWidgetArea, parent_->dock_options_ = new simple::Options(parent_));
    }
    void instance_logs() const {
        parent_->addDockWidget(Qt::BottomDockWidgetArea, parent_->dock_logs_ = new simple::Logs(parent_));
    }
    void instance_central_widget() const {
        parent_->central_widget_= new simple::CentralWidget(parent_);
        parent_->setCentralWidget( parent_->central_widget_ );
    }
    void instance_menu_help() const {
        parent_->menuBar()->addMenu( parent_->menu_help_ = new lg::menu::Help(parent_));
    }
    void instance_menu_view() const {
        parent_->menuBar()->addMenu( parent_->menu_view_ = new QMenu(parent_));
        parent_->menu_view_->addAction(parent_->dock_simple_words_->toggleViewAction());
        parent_->menu_view_->addAction(parent_->dock_options_->toggleViewAction());
        parent_->menu_view_->addAction(parent_->dock_logs_->toggleViewAction());
    }
protected:
    void instance_form() const{
        parent_->setMinimumSize(GOLDEN_SECTION * 400,400);
        instance_central_widget();
    }
    void instance_widgets() const{
        instance_words_list();
        instance_options();
        instance_logs();
        instance_menu_view();
        instance_menu_help();
    }	

public:
    explicit Instance(parent* p) : parent_(p) {
    }
    void     translate() {
       parent_->menu_view_->setTitle(iMenu::tr("&View"));
    }
};
}} // end namespace lg::simple
