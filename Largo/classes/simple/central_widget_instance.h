/**
 * \file      classes/simple/central_widget_instance.h
 * \brief     The Instance class provides an instance to the CentralWidget
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.02
 * \date      Created on 18 August 2012 y., 00:02
 * \TODO
**/
#pragma once
#include <QLabel>
#include <QLayout>
#include <QPushButton>
#include <QPlainTextEdit>

/** namespace lg::simple::central_widget */
namespace lg {
namespace simple {
namespace central_widget {

template<typename T>
class Instance {
    typedef Instance class_name;
    typedef T        parent;

    parent*          parent_;

    void instance_panel() const {
        parent_->panel_ = new QLabel;
        parent_->panel_->setScaledContents(true);
        QImage image(":/panels/simple.png");
        parent_->panel_->setPixmap(QPixmap::fromImage(image));

        parent_->label_ = new QLabel;
    }
    QVBoxLayout* instance_panel_layout() const {
        instance_panel();

        auto lay = new QVBoxLayout;
        lay->addWidget(parent_->label_);
        lay->addWidget(parent_->panel_,20);
        return lay;
    }

    void instance_button() const {
        parent_->button_ = new QPushButton;
    }
    QHBoxLayout* instance_button_layout() const {
        instance_button();

        auto lay = new QHBoxLayout;
        lay->addStretch(10);
        lay->addWidget(parent_->button_);
        return lay;
    }
protected:
    void instance_form() const{
        parent_->setObjectName(QString::fromUtf8("SimpleCentralWidget"));
    }
    void instance_widgets() const {
        auto lay = new QVBoxLayout(parent_);
        lay->addLayout(instance_panel_layout());
        lay->addLayout(instance_button_layout());
    }

public:
    explicit Instance(parent* p) : parent_(p) {}
    void translate() {
        parent_->setWindowTitle(iButton::tr("Information"));
        parent_->button_->setText(iButton::tr("Start"));
        parent_->label_->setText(iButton::tr("Information"));

    }
};
}}} // end namespace lg::simple::central_widget

