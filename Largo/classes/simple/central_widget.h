/**
 * \file       classes/simple/central_widget.h
 * \brief      The CentralWidget class provides a new widget
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.7
 * \date       Created on 01 September 2012 y., 23:32
 * \TODO
**/
#pragma once
#include <QPushButton>
#include <QDialog>

/** namespace lg::simple */
namespace lg {
namespace simple {

class CentralWidget : public QWidget {
Q_OBJECT
friend class central_widget::Instance<CentralWidget>;

public:
    typedef QWidget   inherited;
    typedef QLabel    label_t;
    typedef QLabel    image_t;

private:
     label_t*     label_;
     image_t*     panel_;
     QPushButton* button_;

    void instanceSignals(){
        connect(button_, SIGNAL(clicked(bool)), SIGNAL(clicked(bool)));
    }

signals:
    void clicked(bool);

public:
    /**
     * \brief Constructs a CentralWidget with the given parent
     * \param {in: QWidget*} parent - The given parent of MainWindow 
     */
    explicit CentralWidget(QWidget* parent)
        : inherited(parent), label_(nullptr), panel_(nullptr),  button_(nullptr) {
        ps::instance::MainWindow< lg::simple::central_widget::Instance<CentralWidget> >(this).instance();
        instanceSignals();
    }
    ~CentralWidget() {
    }
    void buttonDisabled(bool disabled) {
        button_->setDisabled(disabled);
    }
};
}} // end namespace lg::simple

