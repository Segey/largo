/**
 * \file      classes/simple/menu/menu_help.cpp
 * \brief     The Menu Help
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.2
 * \date      Created on 15 July 2013 y., 18:24
 * \TODO
**/
#include <QWidget>
#include <QMenu>
#include <QAction>

#pragma once
/** \namespace lg::menu  */
namespace lg {
namespace menu {

class Help : public QMenu {
Q_OBJECT
public:
    typedef QMenu inherited;
    typedef Help class_name;

    QAction* about_act_;

    void create_actions() {
        about_act_ = new QAction(iAction::tr("About"), this);
        //about_act_          = Action::createAbout(this);
    }
    void instance_signals() {
        connect(about_act_, SIGNAL(triggered()), SLOT(openAbout()));
    }
    void instance_actions() {
        inherited::addAction(about_act_);
    }

private slots:
    void openAbout() const {
        const QString title = QString("The Largo v.%1").arg(program::version());
        auto desc  = iMessage::tr("The program trainee for studying Languages.");
        auto value = QString("<b>%1.</b><br /><br />%2<br />%3<br />%4<br /><br />%5<br /><br />%6")
                .arg(title)
                .arg(QString("<b>") + iCalc::tr("author:") + "</b>\t" + program::author())
                .arg(QString("<b><i>") + iCalc::tr("version:") + "</i></b>\t" + program::version())
                .arg(QString("<b><i>") + iCalc::tr("build:") + "</i></b>\t" + program::build())
                .arg(desc)
                .arg(program::copyright());

        QMessageBox::about(nullptr, title, value);
    }
public:
    explicit Help(QWidget* parent =nullptr) 
        : inherited(parent), about_act_(nullptr) {
        instance();
    }
    void instance() {
        create_actions();
        instance_actions();
        instance_signals();
		translate();
    }
	void translate()  {
        inherited::setTitle(iMenu::tr("&Help")); 
    }
};

}} // end namespace lg::menu
