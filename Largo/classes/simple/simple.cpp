#include "simple.h"

using namespace lg;
SimpleMainWindow::SimpleMainWindow()
    : inherited(name()), central_widget_(nullptr), dock_simple_words_(nullptr), dock_options_(nullptr), dock_logs_(nullptr)
    ,menu_view_(nullptr), menu_help_(nullptr)
{
   ps::instance::MainWindow< simple::Instance<SimpleMainWindow> >(this).instance();
   instanceSignals();
   inherited::settingsLocationRead();
   onViewDataChanged();
   dock_logs_->show();
}
void SimpleMainWindow::instanceSignals()
{
    connect(dock_simple_words_,   SIGNAL(viewDataChanged()),  SLOT(onViewDataChanged()));
    connect(central_widget_,      SIGNAL(clicked(bool)),      SLOT(onRunTraining(bool)));
}
void SimpleMainWindow::onViewDataChanged()
{
    auto isOmited = dock_simple_words_->isAllItemsUnChecked();
    central_widget_->buttonDisabled(isOmited);
}
template<typename T>
void SimpleMainWindow::run(list_items_t const& items, words_t const& words, Sets const& s)
{
     T dialog;
     dialog.setData(items, words, s);
     if(QDialog::Rejected != dialog.exec())
         dock_logs_->add(dialog.logs_policy().get());
}

void SimpleMainWindow::onRunTraining(bool)
{
    using namespace lg::policy;
    auto const& items = dock_simple_words_->allCheckedItems();
    auto const& words = dock_simple_words_->allCheckedWords();
    if(items.empty() || words.empty()) return central_widget_->buttonDisabled(true);

    Sets s;
    s.form(dock_options_->form());
    s.tense(dock_options_->tense());
    s.lang(dock_options_->language());
    s.dialog(dock_options_->dialog());

  //  using namespace module::dialog;
    auto id = dock_options_->dialog();
    if(id == module::dialog::words().first) run<lg::dialog::Words<data::Words, time::Simple, sequence::Random, logs::V1> >(items, words, s);
    else if(id == module::dialog::simple2().first) run<lg::dialog::Simple2<data::Simple, time::Simple, sequence::Random, logs::V1> >(items, words, s);
    else if(id == module::dialog::birds2().first) run<lg::dialog::Birds2<data::Birds2, time::Simple, sequence::Random, logs::V1> >(items, words, s);
    else run<lg::dialog::Simple<data::Simple, time::Simple, sequence::Random, logs::V1> >(items, words, s);
}
