/**
 * \file      classes/simple/docks/options/options_instance.h
 * \brief     The Options Dock instance
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version	  v.1.1
 * \date      Created on 02 August 2013 y., 01:37
 * \TODO
**/
#pragma once
/** namespace lg::simple::options */
namespace lg {
namespace simple {
namespace options {

template<typename T>
class Instance {
    typedef Instance class_name;
    typedef T        parent;

    parent*          parent_;

    void instance() const {
        QWidget* widget  = new QWidget;
        QVBoxLayout* lay = new QVBoxLayout(widget);
        lay->addWidget(parent_->dialog_box_);
        lay->addSpacing(8);
        lay->addWidget(parent_->language_box_);
        lay->addSpacing(8);
        lay->addWidget(parent_->form_box_);
        lay->addSpacing(8);
        lay->addWidget(parent_->tense_box_);
        lay->addStretch();
        parent_->setWidget(widget);
    }
    void instance_dialog_box() const {
        parent_->dialog_box_  = new ps::RadioButtonGroup(2);
    }
    void instance_language_box() const {
        parent_->language_box_  = new ps::RadioButtonGroup(3);
    }
    void instance_form_box() const {
        parent_->form_box_  = new ps::ExtendRadioButtonGroup(2);
    }
    void instance_tense_box() const {
        parent_->tense_box_  = new ps::ExtendRadioButtonGroup(2);
    }

protected:
    void instance_form() const{
        parent_->setObjectName(QString::fromUtf8("Options"));
    }
    void instance_widgets() const {
        instance_dialog_box();
        instance_language_box();
        instance_form_box();
        instance_tense_box();
        instance();
    }

public:
    explicit Instance(parent* p) : parent_(p) {}
    void     translate() {
       parent_->setWindowTitle(iButton::tr("Options"));

       using namespace module::dialog;
       parent_->dialog_box_->setTitle(iButton::tr("The Dialogs"));
       parent_->dialog_box_->addRadioButton(iButton::tr("The simple"),     module::dialog::simple().first);
       parent_->dialog_box_->addRadioButton(iButton::tr("The simple2"),    simple2().first);
       parent_->dialog_box_->addRadioButton(iButton::tr("The words"),      words().first);
       parent_->dialog_box_->addRadioButton(iButton::tr("The true-false"), birds2().first);
       parent_->dialog_box_->button(module::dialog::simple().first)->setChecked(true);

       using namespace module::library::form;
       parent_->form_box_->setTitle(iButton::tr("The forms"));
       parent_->form_box_->setMoreText(iButton::tr("more..."));
       parent_->form_box_->addRadioButton(iButton::tr("All"),    interrogative().first | declarative().first | negative().first);
       parent_->form_box_->addRadioButton(iButton::tr("The interrogative"),  interrogative().first);
       parent_->form_box_->addRadioButton(iButton::tr("The declarative"),    declarative().first);
       parent_->form_box_->addRadioButton(iButton::tr("The negative"),       negative().first);
       parent_->form_box_->addExtendRadioButton(iButton::tr("The interrogative + the declarative"),  interrogative().first    | declarative().first);
       parent_->form_box_->addExtendRadioButton(iButton::tr("The interrogative + the negative"),     interrogative().first    | negative().first);
       parent_->form_box_->addExtendRadioButton(iButton::tr("The declarative + the negative"),       declarative().first      | negative().first);
       parent_->form_box_->setCheckeddButton(interrogative().first | declarative().first | negative().first, true);
        
       using namespace module::library::tense;
       parent_->tense_box_->setTitle(iButton::tr("The tenses"));
       parent_->tense_box_->setMoreText(iButton::tr("more..."));
       parent_->tense_box_->addRadioButton(iButton::tr("All"),    past().first | present().first | future().first);
       parent_->tense_box_->addRadioButton(iButton::tr("The past"),    past().first);
       parent_->tense_box_->addRadioButton(iButton::tr("The present"), present().first);
       parent_->tense_box_->addRadioButton(iButton::tr("The future"),  future().first);
       parent_->tense_box_->addExtendRadioButton(iButton::tr("The past + the present"),    past().first    | present().first);
       parent_->tense_box_->addExtendRadioButton(iButton::tr("The past + the future"),     past().first    | future().first);
       parent_->tense_box_->addExtendRadioButton(iButton::tr("The present + the future"),  present().first | future().first);
       parent_->tense_box_->setCheckeddButton(past().first | present().first | future().first, true);

       parent_->language_box_->setTitle(iButton::tr("The languages"));
       parent_->language_box_->addRadioButton(iButton::tr("All"), QLocale::AnyLanguage);
       parent_->language_box_->addRadioButton(iButton::tr("English"), QLocale::English);
       parent_->language_box_->addRadioButton(iButton::tr("Russian"), QLocale::Russian);
       parent_->language_box_->button(QLocale::AnyLanguage)->setChecked(true);
    }
};

}}} // end namespace lg::simple::options
