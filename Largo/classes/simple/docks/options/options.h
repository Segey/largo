/**
 * \file	  classes/simple/docks/options/options.h
 * \brief	  The Options provides the Options' Dock
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.1
 * \date      Created on 02 August 2013 y., 01:37
 * \TODO
**/
#pragma once
#include <QDockWidget>
#include <QPushButton>
//#include "simple_words_view.h"
#include "options_instance.h"

/** \namespace lg::simple */
namespace lg {
namespace simple {

class Options : public QDockWidget {
Q_OBJECT
friend class options::Instance<Options>;

public:
    typedef QDockWidget                          inherited;
    typedef lg::LibraryNone::LangItem::ListItems list_items_t;

private:
    ps::RadioButtonGroup*        dialog_box_;
    ps::RadioButtonGroup*        language_box_;
    ps::ExtendRadioButtonGroup*  form_box_;
    ps::ExtendRadioButtonGroup*  tense_box_;

public:
    /**
     * \brief Ctor
     * \param {in: QWidget*} parent - The parent of MainWindow 
     * \code
     *      parent_->dock_options_ = new simple::Options(parent_);
	 *      parent_->addDockWidget(Qt::RightDockWidgetArea, parent_->dock_options_);
     * \endcode
    **/
    explicit Options(QWidget* parent)
       : inherited(parent), dialog_box_(nullptr), language_box_(nullptr), form_box_(nullptr), tense_box_(nullptr) {
       ps::instance::MainWindow< options::Instance<Options> >(this).instance();
   }
    ~Options() {}

   unsigned tense() const {
       return tense_box_->currentId();
   }
   unsigned form() const {
       return form_box_->currentId();
   }
   unsigned language() const {
       return language_box_->currentId();
   }
   unsigned dialog() const {
       return dialog_box_->currentId();
   }
};

}} // end namespace lg::simple
