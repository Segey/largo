#include "simple_words.h"

using namespace lg::simple;
SimpleWord::SimpleWord(QWidget* parent)
    : inherited(parent), view_(nullptr), bottom_box_(nullptr), execute_button_(nullptr)
{
    ps::instance::MainWindow< simple_words::Instance<SimpleWord> >(this).instance();
    instanceSignals();
    onButtonClicked(bottom_box_->group()->checkedId());
}
void SimpleWord::instanceSignals()
{
    connect(view_,           SIGNAL(isChecked(QStandardItem*)),   SLOT(onIsViewItemChecked(QStandardItem*)));
    connect(view_,           SIGNAL(isUnChecked(QStandardItem*)), SLOT(onIsViewItemUnChecked(QStandardItem*)));
    connect(view_,           SIGNAL(dataChanged()),               SLOT(onViewDataChanged()));
    connect(bottom_box_,     SIGNAL(buttonClicked(int)),          SLOT(onButtonClicked(int)));
    connect(execute_button_, SIGNAL(clicked(bool)),               SLOT(onExecuteButtonClicked(bool)));
}
void SimpleWord::onButtonClicked(int id)
{
    execute_button_->setEnabled(( (but_add_file == id || but_add_word == id) ? true : !view_->isAllItemsUnChecked()) );
}
void SimpleWord::onIsViewItemChecked(QStandardItem* item)
{
    onButtonClicked(bottom_box_->group()->checkedId());
    emit isViewItemChecked(item);
}
void SimpleWord::onIsViewItemUnChecked(QStandardItem* item)
{
    onButtonClicked(bottom_box_->group()->checkedId());
    emit isViewItemUnChecked(item);
}
void SimpleWord::onViewDataChanged()
{
    onIsViewItemChecked(nullptr);
    emit viewDataChanged();
}
void SimpleWord::onExecuteButtonClicked(bool /* checked */)
{
    const auto id = bottom_box_->group()->checkedId();
    if(but_delete == id) {
        view_->removeValues();
        view_->updateData();
    }
    else if(but_add_file == id) view_->addNewFromFile();
    else if(but_add_word == id) view_->addNewFromForm();
}
