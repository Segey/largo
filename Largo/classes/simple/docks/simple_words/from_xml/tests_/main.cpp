#include <QtTest/QtTest>
#include <QTGui>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <iostream>
#include <vector>
#include <algorithm>
#include "tr.h"
#include "const.h"
#include "ps/c++/qt/macros.h"
#include "ps/c++/qt/strategy/strategy_message.h"
#include "ps/c++/qt/db/sqlite/serialization.h"
#include "ps/c++/qt/db/sqlite/base_db.h"
#include "classes/db/source_library.h"
#include "classes/db/library.h"
#include "classes/simple/docks/simple_words/from_xml.h"
#include "classes/simple/docks/simple_words/from_xml/base.h"
#include "classes/simple/docks/simple_words/from_xml/v1.h"
#include "classes/simple/docks/simple_words/from_xml/v2.h"

namespace lg {
class Test_SimpleWordFromXML : public QObject {
Q_OBJECT
    typedef Test_SimpleWordFromXML class_name;

private slots:
    void initTestCase()    { //!< Called before everything else
    }      
    void cleanupTestCase() {  //!< After testing
    }
    void check1() {
        using namespace lg::simple::from_xml;
        Simple b("c:/Projects/largo/Largo/classes/simple/docks/simple_words/from_xml/tests_/simple_words_v1.xml");
        auto error = b.init();

        QVERIFY(QString() == error);
    }
    void check2() {
        using namespace lg::simple::from_xml;
        Simple b("c:/Projects/largo/Largo/classes/simple/docks/simple_words/from_xml/tests_/simple_words_v2.xml");
        auto error = b.init();
        QVERIFY(QString() == error);
    }
};
} // end lg namespace

QTEST_MAIN(lg::Test_SimpleWordFromXML)
#include "main.moc"
