/**
 * \file       Base.h
 * \brief      The Base File contains the abstract class witch provides many functions to create SimpleWord from XML
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.1
 * \date       Created on 12 August 2013 y., 17:20
 * \TODO
**/
#pragma once
#include <QString>

/** \namespace lg */
namespace lg {
namespace simple {
namespace from_xml {

class Base  {
public:
    typedef Base                         class_name;
    typedef LibraryNone::LangItem::Item  item_t;
    typedef LibraryNone::LangItem::Items items_t;

private:
    items_t   items_;

protected:
    virtual QString doWork(QDomDocument const& doc) = 0;
    void addNewItem(item_t const& item) {
        items_.push_back(item);
    }
    unsigned getLanguage(QDomElement const& e) {
        auto lang = e.attribute("language");
        if(lang.toLower()      == "english")    return QLocale::English;
        else if(lang.toLower() == "russian")    return QLocale::Russian;
        else if(lang.toLower() == "belarusian") return QLocale::Belarusian;
        else if(lang.toLower() == "ukrainian")  return QLocale::Ukrainian;
        return QLocale::AnyLanguage;
    }
    unsigned getTense(QDomElement const& e) {
        auto tense   = e.attribute("tense");
        auto past    = module::library::tense::past();
        auto future  = module::library::tense::future();
        auto present = module::library::tense::present();
        if(tense.toLower()      == past.second)    return past.first;
        else if(tense.toLower() == present.second) return present.first;
        else if(tense.toLower() == future.second)  return future.first;
        return module::library::tense::none().first;
    }
    unsigned getForm(QDomElement const& e) {
        auto form          = e.attribute("form");
        auto interrogative = module::library::form::interrogative();
        auto declarative   = module::library::form::declarative();
        auto negative      = module::library::form::negative();
        if(form.toLower()      == interrogative.second) return interrogative.first;
        else if(form.toLower() == negative.second)      return negative.first;
        else if(form.toLower() == declarative.second)   return declarative.first;
        return module::library::form::none().first;
    }
    void getElements(unsigned lang, unsigned tense, unsigned form, QDomNodeList const& elements) {
        item_t result;
        result.lang_  = lang;
        result.tense_ = tense;
        result.form_  = form;
        result.type_  = module::library::type::simple().first;
        for (int index = 0; index != elements.count(); ++index) {
            auto e  = elements.item(index).toElement();
            auto tag = e.tagName();
            auto text = e.toElement().text();
            if(tag.toLower() == "i")    result.I_    = text;
            if(tag.toLower() == "he")   result.he_   = text;
            if(tag.toLower() == "she")  result.she_  = text;
            if(tag.toLower() == "it")   result.it_   = text;
            if(tag.toLower() == "you")  result.you_  = text;
            if(tag.toLower() == "ye")   result.ye_   = text;
            if(tag.toLower() == "we")   result.we_   = text;
            if(tag.toLower() == "they") result.they_ = text;
        }
        addNewItem(result);
    }

public:
    /**
     * \code
     *      using namespace lg::simple::from_xml;
     *      V1 v1;
     *      auto error = v1.work(doc);
     *      if(QString() != error) return;
     *      auto items = v1.items();
     * \endcode
    **/
    Base() {
    }
    virtual ~Base() {}
    QString work(QDomDocument const& doc) {
        return doWork(doc);
    }
    items_t items() const {
        return items_; 
    }
};

}}} // end namespace lg::simple::from_xml
