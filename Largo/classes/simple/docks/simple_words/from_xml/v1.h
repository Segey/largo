/**
 * \file       v1.h
 * \brief      The File _1 to create SimpleWord from XML
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.1
 * \date       Created on 12 August 2013 y., 01:12
 * \TODO
**/
#pragma once
#include <QString>
#include <QtXml>
#include <QtXmlPatterns/QXmlQuery>
#include <QXmlResultItems>
#include <iostream>
#include <algorithm>
#include "base.h"

/** \namespace lg */
namespace lg {
namespace simple {
namespace from_xml {

class V1 : public Base {
    typedef V1                  class_name;
    typedef Base                inherited;
    typedef inherited::item_t   item_t;
    typedef inherited::items_t  items_t;

    QString doWork(QDomDocument const& doc) {
        auto items = doc.elementsByTagName("item");
        if(0 == items.count()) return iError::tr("The file doesn't contain the Item tags!");
        for (int index = 0; index != items.count(); ++index) {
            auto item = items.item(index);
            auto e = item.toElement();
            const auto lang = inherited::getLanguage(e);
            if(lang == QLocale::AnyLanguage) continue;
            const auto tense = inherited::getTense(e);
            if(tense == module::library::tense::none().first) continue;
            const auto form = getForm(e);
            if(form == module::library::form::none().first) continue;
            inherited::getElements(lang, tense, form, item.childNodes());
        }
        return inherited::items().size() > 0 ? QString() : iError::tr("The file doesn't contain the correct Item tags!");
    } 
};

}}} // end namespace lg::simple::from_xml
