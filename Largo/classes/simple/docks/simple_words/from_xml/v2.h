/**
 * \file       v2.h
 * \brief      The File _2 to create SimpleWord from XML
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.1
 * \date       Created on 12 August 2013 y., 01:34
 * \TODO
**/
#pragma once
#include <QString>
#include <QtXml>
#include <QtXmlPatterns/QXmlQuery>
#include <QXmlResultItems>
#include <iostream>
#include <algorithm>

/** \namespace lg */
namespace lg {
namespace simple {
namespace from_xml {

using namespace module::library;

class V2 : public Base {
    typedef V2                  class_name;
    typedef Base                inherited;
    typedef inherited::item_t   item_t;
    typedef inherited::items_t  items_t;
    typedef QVector<QString>    words_t;

    void insertInterrogative(unsigned tense, QString const& word) {
        item_t result;
        result.lang_  = QLocale::English;
        result.tense_ = tense;
        result.form_  = form::interrogative().first;
        result.type_  = type::simple().first;

        QString first1 = tense::future().first  == tense ? "Will" :
                         tense::past().first    == tense ? "Did"  : "Do";
        QString first2 = tense::present().first == tense ? "Does" : first1;

        result.I_     = QString("%1 I %2?").arg(first1).arg(word);
        result.you_   = QString("%1 you %2?").arg(first1).arg(word);
        result.he_    = QString("%1 he %2?").arg(first2).arg(word);
        result.she_   = QString("%1 she %2?").arg(first2).arg(word);
        result.it_    = QString("%1 it %2?").arg(first2).arg(word);
        result.ye_    = QString("%1 you %2?").arg(first1).arg(word);
        result.we_    = QString("%1 we %2?").arg(first1).arg(word);
        result.they_  = QString("%1 they %2?").arg(first1).arg(word);
        inherited::addNewItem(result);
    }
    void insertDeclarative(unsigned tense, QString const& base, QString const& present, QString const& past) {
        item_t result;
        result.lang_  = QLocale::English;
        result.tense_ = tense;
        result.form_  = form::declarative().first;
        result.type_  = type::simple().first;

        QString first1 = tense::future().first  == tense ? "will " + base :
                         tense::past().first    == tense ? past  : base;
        QString first2 = tense::present().first == tense ? present : first1;

        result.I_     = QString("I %1.").arg(first1);
        result.you_   = QString("You %1.").arg(first1);
        result.he_    = QString("He %1.").arg(first2);
        result.she_   = QString("She %1.").arg(first2);
        result.it_    = QString("It %1.").arg(first2);
        result.ye_    = QString("You %1.").arg(first1);
        result.we_    = QString("We %1.").arg(first1);
        result.they_  = QString("They %1.").arg(first1);
        inherited::addNewItem(result);
    }
    void insertNegative(unsigned tense, QString const& base) {
        item_t result;
        result.lang_  = QLocale::English;
        result.tense_ = tense;
        result.form_  = form::negative().first;
        result.type_  = type::simple().first;

        QString first1 = tense::future().first  == tense ? "will not " + base :
                         tense::past().first    == tense ? "did not " + base  : "do not " + base;
        QString first2 = tense::present().first == tense ? "does not " + base : first1;

        result.I_     = QString("I %1.").arg(first1);
        result.you_   = QString("You %1.").arg(first1);
        result.he_    = QString("He %1.").arg(first2);
        result.she_   = QString("She %1.").arg(first2);
        result.it_    = QString("It %1.").arg(first2);
        result.ye_    = QString("You %1.").arg(first1);
        result.we_    = QString("We %1.").arg(first1);
        result.they_  = QString("They %1.").arg(first1);
        inherited::addNewItem(result);
    }
    bool insertEnglishElements(QDomElement const& e) {
        auto base    = e.attribute("base");
        auto present = e.attribute("present");
        auto past    = e.attribute("past");
        if(base.isEmpty() || present.isEmpty() || past.isEmpty()) return false;

        insertNegative(tense::past().first,    base);
        insertNegative(tense::present().first, base);
        insertNegative(tense::future().first,  base);
        insertInterrogative(tense::past().first,    base);
        insertInterrogative(tense::present().first, base);
        insertInterrogative(tense::future().first,  base);
        insertDeclarative(tense::past().first, base, present, past);
        insertDeclarative(tense::present().first, base, present, past);
        insertDeclarative(tense::future().first, base, present, past);
        return true;
    }
    QString doWork(QDomDocument const& doc) {
        auto items = doc.elementsByTagName("item");
        if(0 == items.count()) return iError::tr("The file doesn't contain the Item tags!");
        for (int index = 0; index != items.count(); ++index) {
            auto item = items.item(index);
            auto e = item.toElement();
            const auto lang = inherited::getLanguage(e);
            if(lang == QLocale::AnyLanguage) continue;
            if(lang == QLocale::English) {
                insertEnglishElements(e);
                continue;
            }
            const auto tense = inherited::getTense(e);
            if(tense == module::library::tense::none().first) continue;
            const auto form = getForm(e);
            if(form == module::library::form::none().first) continue;
            inherited::getElements(lang, tense, form, item.childNodes());
        }
        return inherited::items().size() > 0 ? QString() : iError::tr("The file doesn't contain the correct Item tags!");
    } 
};

}}} // end namespace lg::simple::from_xml
