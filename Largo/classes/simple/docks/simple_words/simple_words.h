/**
 * \file	  classes/simple/docks/simple_words.h
 * \brief	  The SimpleWord provides the Words' Dock
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.4
 * \date      Created on 08 July 2013 y., 14:09
 * \TODO
**/
#pragma once
#include <QDockWidget>
#include <QPushButton>
#include "simple_words_view.h"
#include "simple_words_instance.h"

/** namespace lg::simple */
namespace lg {
namespace simple {

class SimpleWord : public QDockWidget {
Q_OBJECT
friend class simple_words::Instance<SimpleWord>;

public:
    typedef QDockWidget                          inherited;
    typedef lg::LibraryNone::LangItem::ListItems list_items_t;
    typedef lg::simple_words::View::words_t      words_t;

private:
    lg::simple_words::View*  view_;
    ps::RadioButtonGroup*    bottom_box_;
    QPushButton*             execute_button_;

    enum buttons {but_edit = 2, but_delete = 3, but_add_word = 4, but_add_file = 5};

    void instanceSignals();

signals:
   void viewDataChanged();
   void isViewItemChecked(QStandardItem*);
   void isViewItemUnChecked(QStandardItem*);

private slots:
    void onIsViewItemChecked(QStandardItem*);
    void onIsViewItemUnChecked(QStandardItem*);
    void onButtonClicked(int);
    void onViewDataChanged();
    /**
     * \brief This signal is emitted when the button is activated
     * \param {in: bool} checked = false
    **/
    void onExecuteButtonClicked(bool);
public:
    /**
     * \brief Ctor
     * \param {in: QWidget*} parent - The parent of MainWindow 
     * \code
     *      parent_->dock_words_list_ = new rote_learning::WordsList(parent_);
	 *      parent_->addDockWidget(Qt::RightDockWidgetArea, parent_->dock_words_list_);
     * \endcode
    **/
    explicit SimpleWord(QWidget* parent);
    ~SimpleWord() {}
    unsigned rowCount() const {
        return view_->rowCount();
    }
    list_items_t allCheckedItems() const {
        return view_->allCheckedItems();
    }
    words_t allCheckedWords() const {
        return view_->allCheckedWords();
    }
    bool isAllItemsUnChecked() const {
        return view_->isAllItemsUnChecked();
    }
    void updateData() {
        view_->updateData();
    }
};

}} // end namespace lg::simple
