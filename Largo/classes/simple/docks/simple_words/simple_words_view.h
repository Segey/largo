/**
 * \file       classes/simple/docs/simple_words/simple_words_view.h
 * \brief      The View class provides a view for wordlist
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.3
 * \date       Created on 06 July 2013 y., 01:49
 * \TODO
**/
#pragma once
#include <QWizard>
#include <QLineEdit>
#include <QTreeView>
#include <QFileDialog>
#include <QListWidgetItem>
#include <QStandardItemModel>

/** namespace lg::simple_words */
namespace lg {
namespace simple_words {

class StandardItemModel : public QStandardItemModel {
    typedef QStandardItemModel inherited;

public :
    void beginResetModel() {
        inherited::beginResetModel();
    }
    void endResetModel() {
        inherited::endResetModel();
    }
    StandardItemModel(QWidget* parent = nullptr) : inherited(parent) {
    }
};

class View: public QObject {
Q_OBJECT
public:
    typedef QObject                              inherited;
    typedef View                                 class_name;
    typedef lg::LibraryNone::Tense::Item         tense_t;
    typedef lg::LibraryNone::Tense::Items        tenses_t;
    typedef lg::LibraryNone::LangItem::ListItems list_items_t;
    typedef QStringList                          words_t;

private:
    const static int COLUMN_CHECK_ID     = 0;
    const static int COLUMN_WORD_ID      = 1;

    QTreeView*           view_;
    StandardItemModel*   model_;
    ps::CheckHeaderView* header_;
    tenses_t             items_;

    static const uint bool_role = Qt::UserRole + 1;    // The bool role for 3 column

    /**
     * \brief Getting tenses from the db
    **/
    void getItems() {
        items_.clear();
        items_ = lg::LibraryNone::Tense::items();
    }
    /**
     * \brief Returns the database string name of widget
    **/
    const static QLatin1String name() {
        return QLatin1String("SimpleWordsView");
    }
    const static QLatin1String fileOpenPathName() {
        return QLatin1String("SimpleWordsViewPath");
    }
    /**
     * \brief Creates a checkable item.
     * \return {out: QStandardItem*} The pointer to the new checkable QStandartItem
    **/
    static QStandardItem* createCheckableItem() {
        QStandardItem* item = new QStandardItem;
        item->setCheckable(true);
        item->setTristate(true);
        item->setEditable(false);
        item->setTextAlignment(Qt::AlignCenter);
        return item;
    }
    /**
     * \brief Creates a new QStandartItem which can't be selected with the given name
     * \return {out: QStandardItem*} The pointer to the new QStandartItem
    **/
    static QStandardItem* createNotSelectedItem(QString const& name) {
        QStandardItem* item = new QStandardItem(name);
        item->setCheckable(false);
        item->setTristate(false);
        item->setEditable(false);
        item->setTextAlignment(Qt::AlignCenter);
        return item;
    }
    /**
     * \brief Creates a new QStandartItem which can be selected and showed the tooltip with the given name
     * \return {out: QStandardItem*} The pointer to the new QStandartItem
    **/
    static QStandardItem* createTooltipSelectedItem(QString const& name, QString const& tooltip) {
        QStandardItem* item = new QStandardItem(name);
        item->setToolTip(tooltip);
        return item;
    }
    /**
     * \brief This function instances all signals for the class.
    **/
    void instanceSignals() {
       connect(model_, SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&, const QVector<int>&)),       SLOT(dataChanged(const QModelIndex&, const QModelIndex&, const QVector<int>&)));
       connect(model_, SIGNAL(itemChanged(QStandardItem*)),       SLOT(itemChanged(QStandardItem*)));
    }
    /**
     * \brief This function updates DB's table when user updates view's elements.
     * \param {in: QModelIndex const&} model - The Model has been got from fired an event
     * \return {out: void }
    **/
    void updateDataFields(QModelIndex const& model) {
        auto& val = items_[model.row()];
        if(1 == model.column()) {
            val.name_  = model.data(Qt::DisplayRole).toString();
            lg::LibraryNone::Tense::update(val);
        }
        emit dataChanged();
    }
    /**
     * \brief Gets a value indicating whether the pointer to QStandardItem widget is cheked.
     * \return {out: bool } Returns true if pointer to QStandardItem object is checked; otherwise false.
    **/
    bool isCheckedItem(QStandardItem* item) const {
        if (item == nullptr || item->column() != COLUMN_CHECK_ID) return false;
        return item->index().data(Qt::CheckStateRole).toInt() == Qt::Checked;
    }
    /**
     * \brief Returns a QList object with the collection of ids.
     * \result {out: QList<int> } Sequence of elements contained by the object.
    **/
    QList<unsigned> checkedIds() const {
        QList<unsigned> result;
        for (auto index = 0; index != model_->rowCount(); ++index) {
            if(true == isCheckedItem(model_->item(index,0))) result << index;
        }
        return result;
    }
    /**
     * \brief Occurs when the table's data has changed.
    **/
    void updateHeaderData() {
        if(0 == header_->count()) return; 
        model_->setHeaderData(COLUMN_CHECK_ID,    Qt::Horizontal, QString());
        model_->setHeaderData(COLUMN_WORD_ID,     Qt::Horizontal, iWord::tr("Word"));

       if(false == lg::SettingsNone::isWindowSaved(name())) header_->resizeSection(COLUMN_CHECK_ID, 20);
       else lg::SettingsNone::restoreWindow(name(), view_->header());
    }
    /**
     * \brief Show MessageBox with more information
    **/
    void showErrorMessage(QString const& detailed_text) {
        QMessageBox msgBox;
        msgBox.setText(iError::tr("Adding operation is failed with error!"));
        msgBox.setInformativeText(iError::tr("You have to check your data in file."));
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDetailedText(detailed_text);
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }
private slots:
    /**
     * \brief This slot is called in response to signal which emitted whenever the data of item has changed.
    **/
    void itemChanged(QStandardItem* item) {
        if (0 != item->column()) return;

        const auto is_checked = isCheckedItem(item);
        if(true == is_checked) emit isChecked(item);
        else emit isUnChecked(item);

        auto font = item->font();
        font.setBold(is_checked);
        for(auto i = 1; i != model_->columnCount(); ++i)
            model_->item(item->row(), i)->setFont(font);
    }
    /**
     * \brief This slot is called in response to signal which emitted whenever the data in an existing item changes.
    **/
    void dataChanged(QModelIndex const& topLeft, QModelIndex const& /* bottomRight */, QVector<int> const& /* roles */ = QVector<int> ()) {
        if (1 == topLeft.column()) {
            updateDataFields(topLeft);
            return;
        }
     }

signals:
   void dataChanged();
   void isChecked(QStandardItem*);
   void isUnChecked(QStandardItem*);

public:
    /**
     * \brief Default ctor of this class
    **/
    View(QWidget* parent)
       : inherited(parent), view_(new QTreeView(parent)), model_( new StandardItemModel(parent)), header_(new ps::CheckHeaderView(Qt::Horizontal, view_)) {
        view_->setMinimumWidth(100);
        view_->setHeader(header_);
        header_->setStretchLastSection(true);
        view_->setRootIsDecorated(false);
        view_->setAlternatingRowColors(true);
        view_->setModel(model_);

        instanceSignals();
        view_->header()->setObjectName(QString::fromUtf8("simpleWords"));
        updateData();
    }
    ~View() {
        if(header_->count() != 0) lg::SettingsNone::saveWindow(name(), view_->header());
    }
    /**
     * \brief This function returns pointer to current view.
    **/
    QTreeView* view() const {
        return view_;
    }
    /**
     * \brief This function fired when need to update class's data(view, model, data).
     * \param {in: uint} section_id - The id of group
    **/
    void updateData() {
        model_->beginResetModel();
        getItems();
        model_->clear();
        header_->removeCheckable(0);

        foreach(auto item, items_) {
            QList<QStandardItem*> list;
            list << createCheckableItem()
                 << createTooltipSelectedItem(item.name_, item.description_);
            model_->invisibleRootItem()->appendRow(list);
        }
        updateHeaderData();
        emit (dataChanged());
        model_->endResetModel();
    }
    /**
     * \brief This function returns true if all checkedaled items is checked (In the first column) otherwise it returned false.
     * \return {out: bool }
    **/
    bool isAllItemsUnChecked() const {
        for (auto index = 0; index != model_->rowCount(); ++index) {
            if( isCheckedItem(model_->item(index,COLUMN_CHECK_ID)) == true) return false;
        }
        return true;
    }
    int rowCount() const {
        return model_->rowCount();
    }
    void removeValues() {
       auto list = checkedIds();
        foreach(uint i, list) {
            auto& value = items_[i];
            lg::LibraryNone::Tense::remove(value.tense_id_);
        }
        updateData();
    }
    /**
     * \breif Provides adding a new item from a file
    **/
    void addNewFromFile() {
        auto path = lg::SettingsNone::restoreFileDialogPath(fileOpenPathName(), QApplication::applicationDirPath() + QString("%1examples").arg(QDir::separator()));
        auto fileName = QFileDialog::getOpenFileName(static_cast<QWidget*>(inherited::parent()), iMenu::tr("Select one Simple word file to open"), path, QString("Simple word Files (*%1)").arg(module::settings::resource_file_ext()));
        if(fileName != QString())   {
            lg::SettingsNone::saveFileDialogPath(fileOpenPathName(), fileName);
            lg::simple::from_xml::Simple simple(fileName);
            auto error = simple.init();
            if(QString() != error)  return showErrorMessage(error);
            error = simple.execute();
            if(QString() != error) showErrorMessage(error);
            updateData();
        }
    }
    /**
     * \breif Provides adding a new item from the form
    **/
    void addNewFromForm() {
        using namespace lg::wizards::simple;;

        QWizard wizard;
        wizard.addPage(new FirstPage());
        wizard.addPage(new SecondPage());
        wizard.exec();
        updateData();

    }
    /**
     * \brief Getting all checked Items which contains in the table.
     * \code
     *   auto items = dock_simple_words_->allCheckedItems();
     * \endcode
    **/
    list_items_t allCheckedItems() const {
        list_items_t result;
        for (auto index = 0; index != model_->rowCount(); ++index) {
            if(true == isCheckedItem(model_->item(index,COLUMN_CHECK_ID))) {
                auto vec = lg::LibraryNone::LangItem::items(items_[index].tense_id_);
                if(!vec.empty()) result.push_back(vec);
            }
        }
        return result;
    }
    words_t allCheckedWords() const {
        words_t result;
        for (auto index = 0; index != model_->rowCount(); ++index) {
            if(true == isCheckedItem(model_->item(index,COLUMN_CHECK_ID))) {
                result << items_[index].name_;
            }
        }
        return result;
    }
};
}} // end namespace lg::simple_words

