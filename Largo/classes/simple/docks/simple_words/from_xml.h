/**
 * \file       from_xml.h
 * \brief      The Base file to create SimpleWord from XML
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.4
 * \date       Created on 13 July 2013 y., 00:31
 * \TODO
**/
#pragma once
#include <QFile>
#include <QString>
#include <QtXml>
#include <QtXmlPatterns/QXmlQuery>
#include <QXmlResultItems>
#include <iostream>
#include <algorithm>
#include "from_xml/v1.h"
#include "from_xml/v2.h"

/** \namespace lg::simple::from_xml */
namespace lg {
namespace simple {
namespace from_xml {

class Simple  {
    typedef Simple                       class_name;
    typedef LibraryNone::LangItem::Item  item_t;
    typedef LibraryNone::LangItem::Items items_t;
    typedef QSharedPointer<Base>         base_t;

    QString   file_name_;
    QString   name_;
    QString   description_;
    unsigned  version_;
    QDate     date_;
    QString   type_;
    items_t   items_;

    bool checkValidFile(QString const& f){
        QFile file(f);
        QXmlSimpleReader xmlReader;
        QXmlInputSource source(&file);
        return xmlReader.parse(source);
    }
    bool version(QXmlQuery& query) {
        QString ver;
        query.setQuery("doc($doc)/largo/@version/string()");
        if(false == query.evaluateTo(&ver)) return false;

        bool done = false;
        version_  = ver.toUInt(&done,10);
        return done && version_ == 1;
    }
    bool type(QXmlQuery& query) {
        query.setQuery("doc($doc)/largo/@type/string()");
        if(false == query.evaluateTo(&type_)) return false;

        if(type_ != "smple") type_ = "simple";
        return true;
    }
    bool date(QXmlQuery& query) {
        QString date;
        query.setQuery("doc($doc)/largo/@date/string()");
        if(false == query.evaluateTo(&date)) return true;

        date_ = QDate::fromString(date, Qt::ISODate);
        return date_.isValid();
    }
    bool name(QXmlQuery& query) {
        query.setQuery("doc($doc)/largo/@name/string()");
        auto done = query.evaluateTo(&name_);
        name_ = name_.trimmed();
        return done;
    }
    bool description(QXmlQuery& query) {
        query.setQuery("doc($doc)/largo/@description/string()");
        auto done = query.evaluateTo(&description_);
        description_ = description_.trimmed();
        return done;
    }
    unsigned getLanguage(QDomElement const e) {
        auto lang = e.attribute("language");
        if(lang.toLower() == "english") return QLocale::English;
        else if(lang.toLower() == "russian") return QLocale::Russian;
        return QLocale::AnyLanguage;
    }
    unsigned insertTense() const {
        lg::LibraryNone::Tense::Item tense(0, name_, description_);
        return lg::LibraryNone::Tense::add(tense);
    }
    bool insertTenseItems(unsigned tense_id) {
        bool result = true;
        std::for_each(items_.begin(), items_.end(), [&](item_t& item) {
            item.tense_id_ = tense_id;
            const auto id = lg::LibraryNone::LangItem::add(item);
            if(id == 0) result = false;
        });
        return result;
    }
    QString get() {
        QDomDocument doc("doc");
        QFile file(file_name_);
        if (!file.open(QIODevice::ReadOnly)) return QString(iError::tr("File %1 doesn't exist or busy !")).arg(file_name_);
        bool result = doc.setContent(&file);
        file.close();
        if(false == result) return iError::tr("The file doesn't contain the Simple Word data!");

        base_t base;
        if(version_ == 1)      base = base_t(new V1);
        else if(version_ == 2) base = base_t(new V2);
        else return iError::tr("The file contains the incorrect version of data type! This version hasn't supported yet!");

        auto error = base->work(doc);
        if(error != QString()) return error;
        items_ = base->items();
        return QString();
    }
public:
    /**
     * \code
     *      using namespace lg::simple::from_xml;
     *      Simple b(fileName);
     *      auto error = b.init();
     *      if(QString() != error) return;
     *      error = b.execute();
     *      if(QString() != error) return;
     * \endcode
     */
    Simple(QString const& file_name) : file_name_(file_name) {}

    QString init() {
        QFile file(file_name_);
        if(false == file.open(QIODevice::ReadOnly)) return QString(iError::tr("File %1 doesn't exist or busy !")).arg(file_name_);
        if(false == checkValidFile(file_name_))     return iError::tr("The file doesn't contain the Simple Word data!");

        QXmlQuery query;
        query.bindVariable("doc", &file);
        version(query);
        date(query);
        type(query);

        if(false == name(query))        return iError::tr("The Largo Tag doesn't contain the Name attribute!");
        if(false == description(query)) return iError::tr("The Largo Tag doesn't contain the Description attribute!");
        return get();
    }
    QString execute() {
        QSqlDatabase library(LibraryNone::database());
        library.transaction();
        auto tense_id = insertTense();  
        if(0 == tense_id) {
            library.rollback();
            return iError::tr("Cannot add the library tense item to the Database!");
        }
        if(false == insertTenseItems(tense_id)) {
            library.rollback();
            return iError::tr("Cannot add the library tense items to the Database!");
        }
        library.commit();
        return QString();
    }
};

}}} // namespace lg::simple::from_xml
