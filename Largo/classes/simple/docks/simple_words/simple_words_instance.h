/**
 * \file      classes/simple/docks/simple_words/words_list_instance.h
 * \brief     The Words List instance
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version	  v.1.1
 * \date      Created on 18 August 2012 y., 00:02
 * \TODO
**/
#pragma once
/** namespace lg::simple::simple_words */
namespace lg {
namespace simple {
namespace simple_words {

template<typename T>
class Instance {
    typedef Instance class_name;
    typedef T        parent;

    parent*          parent_;

    void instance_table() const {
        parent_->view_ = new lg::simple_words::View(parent_);
    }
    void instance_bottom_panel() const {
        parent_->bottom_box_      = new ps::RadioButtonGroup(1);
        parent_->execute_button_  = new QPushButton;
    }
    QHBoxLayout* instance_bottom_layout() const {
        QVBoxLayout* lay_right = new QVBoxLayout;
        lay_right->addStretch();
        lay_right->addWidget(parent_->execute_button_);

        QHBoxLayout* lay = new QHBoxLayout;
        lay->addWidget(parent_->bottom_box_,10);
        lay->addLayout(lay_right);
        return lay;
    }
    void instance_central_widget() const {
        QWidget* widget  = new QWidget;
        QVBoxLayout* lay = new QVBoxLayout(widget);
        lay->addWidget(parent_->view_->view(),10);
        lay->addLayout(instance_bottom_layout());
        parent_->setWidget(widget);
    }

protected:
    void instance_form() const{
        parent_->setObjectName(QString::fromUtf8("SimpleWord"));
    }
    void instance_widgets() const{
        instance_table();
        instance_bottom_panel();
        instance_central_widget();
    }

public:
    explicit Instance(parent* p) : parent_(p) {}
    void     translate() {
        parent_->setWindowTitle(iButton::tr("The list of words"));
        
        parent_->bottom_box_->setTitle(iButton::tr("The list of actions"));
        parent_->bottom_box_->addRadioButton(iButton::tr("Remove word(s)"), parent_->but_delete);
        parent_->bottom_box_->addRadioButton(iButton::tr("Add a word ..."), parent_->but_add_word);
        parent_->bottom_box_->addRadioButton(iButton::tr("Add a word from a file"), parent_->but_add_file);
        parent_->bottom_box_->group()->button(parent_->but_delete)->setChecked(true);

        parent_->execute_button_->setText(iButton::tr("Execute"));
    }
};

}}} // end namespace lg::simple::simple_words
