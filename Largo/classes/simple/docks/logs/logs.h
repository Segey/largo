/**
 * \file	  classes/simple/docks/options/options.h
 * \brief	  The Options provides the Options' Dock
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.2
 * \date      Created on 02 August 2013 y., 01:37
 * \TODO
**/
#pragma once

#include <QLabel>
#include <QDockWidget>
#include <QPushButton>
#include "logs_instance.h"

/** \namespace lg::simple */
namespace lg {
namespace simple {

class Logs : public QDockWidget {
Q_OBJECT
friend class logs::Instance<Logs>;

public:
    typedef QDockWidget  inherited;

private:
    QLabel*  list_;
    QMenu*   menu_;
    QAction* actClearLogs_;

    void instanceSignals() {
        connect(actClearLogs_, SIGNAL(triggered()), SLOT(onClearLogs()));
    }
    void contextMenuEvent(QContextMenuEvent* e) {
         menu_->exec(e->globalPos());
    }

private slots:
    void onClearLogs() {
        lg::policy::logs::V1::clearAllLogs();
        update();
    }

public:
    /**
     * \brief Ctor
     * \param {in: QWidget*} parent - The parent of MainWindow 
     * \code
     *      parent_->dock_logs_ = new simple::Logs(parent_);
     *      parent_->addDockWidget(Qt::BottomDockWidgetArea, parent_->dock_logs_);
     * \endcode
    **/
    explicit Logs(QWidget* parent)
        : inherited(parent), list_(nullptr), menu_(nullptr), actClearLogs_(nullptr) {
       ps::instance::MainWindow< logs::Instance<Logs> >(this).instance();
       instanceSignals();
   }
   ~Logs() {}

   void show() {
       auto logs = lg::policy::logs::V1::getAllLogs();
       if(logs.size() == 0) return;

       QString out = logs[0];
       for(int i=1; i!=logs.size(); ++i)
           out += "\n" + logs[i];

       list_->setText(out);
       list_->adjustSize();
   }
   void add(QString const& log) {
       list_->setText(log + "\n" + list_->text());
       list_->adjustSize();
   }
   void update()  {
       clear();
       show();
   }
   void clear() {
        list_->clear();
   }
};

}} // end namespace lg::simple
