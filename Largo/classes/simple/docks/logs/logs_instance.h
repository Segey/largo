/**
 * \file      classes/simple/docks/logs/options_logs.h
 * \brief     The Logs Dock instance
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version	  v.1.1
 * \date      Created on 24 September 2013 y., 16:17
 * \TODO
**/
#pragma once
#include <QVBoxLayout>
#include <QScrollArea>

/** \namespace lg::simple::logs */
namespace lg {
namespace simple {
namespace logs {

template<typename T>
class Instance {
    typedef Instance class_name;
    typedef T        parent;

    parent*          parent_;

    void instance() const {
        QScrollArea* a = new QScrollArea;
        a->setWidget(parent_->list_);
        parent_->setWidget(a);
    }
    void instance_list() const {
        parent_->list_  = new QLabel;
    }
    void instance_menu() const {
        auto menu = parent_->menu_  = new QMenu();
        auto actLogs = parent_->actClearLogs_ = new QAction(iAction::tr("Clear Logs"), parent_);
        menu->addAction(actLogs);
    }
protected:
    void instance_form() const{
        parent_->setObjectName(QString::fromUtf8("Logs"));
    }
    void instance_widgets() const {
        instance_menu();
        instance_list();
        instance();
    }

public:
    explicit Instance(parent* p) : parent_(p) {}
    void     translate() {
       parent_->setWindowTitle(iButton::tr("Logs"));
    }
};

}}} // end namespace lg::simple::logs
