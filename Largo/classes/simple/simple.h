/**
 * \file       classes/simple/simple.h
 * \brief      The Simple class provides a simple window
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.7
 * \date       Created on 01 September 2013 y., 23:32
 * \TODO
**/
#pragma once
#include "simple_instance.h"

/** \namespace lg */
namespace lg {
class SimpleMainWindow : public SaveRestoreMainWindow {
Q_OBJECT
friend class simple::Instance<SimpleMainWindow>;

    typedef SimpleMainWindow                  class_name;
    typedef SaveRestoreMainWindow             inherited;
    typedef simple::SimpleWord::list_items_t  list_items_t;
    typedef simple::SimpleWord::words_t       words_t;

    simple::CentralWidget*   central_widget_;
    simple::SimpleWord*      dock_simple_words_;
    simple::Options*         dock_options_;
    simple::Logs*            dock_logs_;
    QMenu*                   menu_view_;
    menu::Help*              menu_help_;

    const static QLatin1String name() {
        return QLatin1String("SimpleMainWindow");
    }
    void instanceSignals();
    template<typename T>
    void run(list_items_t const& items, words_t const& words, Sets const& s);

public slots:
    void onViewDataChanged();
    void onRunTraining(bool);

public:
    SimpleMainWindow();
    ~SimpleMainWindow() {}
};

} // end namespace lg
