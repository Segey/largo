#include <QtTest/QtTest>
#include <QTGui>
#include "ps/c++/qt/rand.h"
#include "classes/policies/sequences/none.h"
#include "classes/policies/sequences/random.h"
#include "classes/policies/sequences/lineal.h"

using namespace lg::policy::sequence;

class Test_Sequence : public QObject {
Q_OBJECT

    template <typename T>
    void testInstance() {
        T t;
        t.next(10u);
    }
private slots:
    void initTestCase()    { //!< Called before everything else
    }      
    void cleanupTestCase() { //!< After testing
    }
	void testNone() {
        None none;
        auto seq = none.next(10u);
        QVERIFY(seq == 0u);
        testInstance<None>();
    } 
	void testRandom() {
        Random random;
        auto seq = random.next(10u);
        QVERIFY(seq != 11u);
        testInstance<Random>();
	}
	void testLineal() {
        Lineal lineal;
        auto seq = lineal.next(10u);
        QVERIFY(seq == 10u);
        testInstance<Lineal>();
	}
};

QTEST_MAIN(Test_Sequence)
#include "main.moc"

