/**
 * \file       classes/policies/sequences/random.h
 * \brief      The Random Sequences policy class provides random functionality
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.1
 * \date       Created on 03 August 2013 y., 02:20
 * \TODO
**/
#pragma once

/** namespace lg::policy::sequence */
namespace lg {
namespace policy {
namespace sequence {

/**
 *  \brief The Empty class is nothing doing 
 *   \code
 *        using namespace lg::policy::sequence;
 *        Random random;
 *        auto index = random.next(10u);
 *        QVERIFY(index != 11u);
 *   \endcode
**/
class Random {
public:
    typedef Random  class_name;

public:
    explicit Random() {}
    ~Random() {}
    unsigned next(unsigned max) {
        return ps::randInt(max);
    }
};

}}} // end namespace lg::policy::sequence

