/**
 * \file       classes/policies/sequences/lineal.h
 * \brief      The Lineal Sequences policy class provides lineal functionality
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.1
 * \date       Created on 03 August 2013 y., 02:43
 * \TODO
**/
#pragma once

/** namespace lg::policy::sequence */
namespace lg {
namespace policy {
namespace sequence {

/**
 *  \brief The Empty class is nothing doing 
 *   \code
 *        using namespace lg::policy::sequence;
 *        Lineal lineal;
 *        auto index = lineal.next();
 *   \endcode
**/
class Lineal {
public:
    typedef Lineal  class_name;

public:
    explicit Lineal() {}
    ~Lineal() {}
    unsigned next(unsigned max) {
        return max;
    }
};

}}} // end namespace lg::policy::sequence

