/**
 * \file       classes/policies/sequences/none.h
 * \brief      The Abstract Sequences policy class provides virtual functions
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.1
 * \date       Created on 02 August 2013 y., 01:22
 * \TODO
**/
#pragma once

/** namespace lg::policy::sequence */
namespace lg {
namespace policy {
namespace sequence {

/**
 *  \brief The Empty class is nothing doing 
 *   \code
 *        using namespace lg::policy::sequence;
 *        None none;
 *        auto seq = none.next(10u);
 *        QVERIFY(seq == 0u);
 *   \endcode
**/
class None {
public:
    typedef None  class_name;

public:
    explicit None() {}
    ~None() {}
    unsigned next(unsigned /* max */) {
        return 0u;
    }
};

}}} // end namespace lg::policy::sequence

