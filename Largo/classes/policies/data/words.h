/**
 * \file       classes/policies/data/simply.h
 * \brief      The Simple Data policy class provides work with the Simple dialog
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.2
 * \date       Created on 01 August 2013 y., 01:40
 * \TODO
**/
#pragma once
#include <QTime>
#include "ps/c++/qt/rand.h"
#include "base.h"

/** \namespace lg::policy::data */
namespace lg {
namespace policy {
namespace data {

/** \namespace words */
namespace words {
struct Item {
    typedef Item         class_name;
    typedef QStringList  variants_t;

    unsigned    id_;
    unsigned    lang_;
    QString     question_;
    QString     answer_;
    variants_t  variants_;

    Item() {}
    Item(unsigned id, unsigned lang, QString const& question)
        : id_(id), lang_(lang), question_(question){
    }
};

struct Lang {
    typedef Lang class_name;

    unsigned  id_;
    unsigned  lang_;
    QString   text_;

    Lang(unsigned id, unsigned lang, QString const& text)
        : id_(id), lang_(lang), text_(text) {
    }
    Lang(){}
    friend bool operator ==(Lang const& lhs, Lang const& rhs) {
        return lhs.id_ == rhs.id_ && lhs.lang_ == rhs.lang_ && lhs.text_ == lhs.text_;
    }
    friend bool operator !=(Lang const& lhs, Lang const& rhs) {
        return !(lhs == rhs);
    }
};
} // end namespace words

/**
 *  \brief The WordsList class provides easy work with the WordsList dialog
 *   \code
 *        using namespace lg::policy::data;
 *        WordsList words;
 *        words.start();
 *        words.stop();
 *        auto data = words.getData();
 *   \endcode
**/
class Words: public Base {
public:
    typedef Words                                class_name;
    typedef Base                                 inherited;
    typedef words::Lang                          lang_t;
    typedef QVector<lang_t>                      langs_t;
    typedef unsigned                             id_t;
    typedef words::Item                          item_t;
    typedef QMultiMap<id_t,item_t>               imaps_t;
    typedef item_t::variants_t                   variants_t;
    typedef lg::LibraryNone::LangItem::Item      Item;
    typedef lg::LibraryNone::LangItem::Items     Items;
    typedef lg::LibraryNone::LangItem::ListItems ListItems;
    typedef QPair<QString, QStringList>          question_t;
    typedef QVector<item_t>                      items_t;

private:
    static const unsigned  MAX_VARIANTS = 3;

private:
    langs_t ru_;
    langs_t en_;
    Sets    sets_;
    item_t  current_;
    items_t items_;

    void createMap(imaps_t& imap, Item const& item)  {
        auto t  = inherited::tense(item);
        auto f  = inherited::form(item);
        if(!t || !f) return;
        unsigned result = t | f;

        langs_t langs;
        langs.push_back(lang_t((result | 64),  item.lang_, item.I_));
        langs.push_back(lang_t((result | 128), item.lang_, item.ye_));
        langs.push_back(lang_t((result | 256), item.lang_, item.you_));
        langs.push_back(lang_t((result | 512), item.lang_, item.he_));
        langs.push_back(lang_t((result | 1024),item.lang_, item.she_));
        langs.push_back(lang_t((result | 2048),item.lang_, item.it_));
        langs.push_back(lang_t((result | 4096),item.lang_, item.we_));
        langs.push_back(lang_t((result | 8192),item.lang_, item.they_));

        auto& la = item.lang_ == QLocale::English ? en_ : ru_;

        foreach (auto lang, langs) {
            imap.insert(lang.id_, item_t(lang.id_, lang.lang_, lang.text_));
            la.push_back(lang);
        }
    }
    void setVariants(item_t& item, unsigned lang, QString const& answer) {
        item.answer_ = answer;
        auto const& array = item.lang_ == QLocale::English ? ru_ : en_;
        auto it = qFind(array.begin(), array.end(), lang_t(item.id_, lang, item.answer_));
        if(it == array.end()) return;

        const auto hole = std::distance(array.begin(), it);

        for (unsigned i = 0; i!= MAX_VARIANTS; ++i) {
            auto index = ps::randIntWithHole(0, array.size() -1, hole);
            item.variants_ << array[index].text_;
        }

       // qDebug() << item.question_ << item.answer_ << item.variants_;
    }

public:
    explicit Words() {
    }
    ~Words() {
    }
    void setData(ListItems const& list, words_t const& w, Sets const& s) {
        inherited::setSets(s);
        inherited::setWords(w);

        items_.clear();
        items_.reserve(144 * list.size());
        std::for_each(list.begin(), list.end(), [&] (Items const& items) {
            imaps_t imap;
            std::for_each(items.begin(), items.end(), [&] (Item const& item) {
                createMap(imap, item);
            });
            foreach (auto index, imap.uniqueKeys()) {
                auto const& vals = imap.values(index);
                auto first  = vals.front();
                auto second = vals.back();
                setVariants(first,  second.lang_, second.question_);
                setVariants(second, first.lang_,  first.question_);

                if(s.lang() == QLocale::AnyLanguage || s.lang() == first.lang_)  items_.push_back(first);
                if(s.lang() == QLocale::AnyLanguage || s.lang() == second.lang_) items_.push_back(second);
            }
        });
    }
    question_t showQuestion(unsigned next) {
        if(static_cast<int>(next) >= items_.size()) return question_t();
        auto it   = items_.begin() + next;
        current_  = (*it);
        items_.erase(it);
        return question_t(qMakePair(current_.question_, current_.variants_));
    }
    QString showAnswer() const {
        return current_.answer_;
    }
    bool hasQuestion() const {
        return false == items_.empty();
    }
    unsigned size() const {
        return items_.size();
    }
    unsigned max_variants() const {
        return MAX_VARIANTS;
    }
};

}}} // end namespace lg::policy::data
