######################################################################
# Automatically generated by qmake (2.01a) ?? 10. ??? 00:16:44 2012
######################################################################

TEMPLATE        = app
TARGET          = main
DEPENDPATH     += .
INCLUDEPATH    += c:\Projects\largo\Largo
QT             += testlib widgets sql
QMAKE_CXXFLAGS += -std=gnu++0x

HEADERS += main.moc \
        ../none.h \
        ../simply.h \
        ../simple.h \
        ../../../../const.h \
        ../words_list.h \
    ../base.h \
    ../birds2.h \
    ../../../../sets.h
SOURCES += main.cpp
