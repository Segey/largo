#include <QtTest/QtTest>
#include <QTGui>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <iostream>
#include <vector>
#include <algorithm>
#include "tr.h"
#include "const.h"
#include "sets.h"
#include "ps/c++/qt/macros.h"
#include "ps/c++/qt/strategy/strategy_message.h"
#include "ps/c++/qt/db/sqlite/serialization.h"
#include "ps/c++/qt/db/sqlite/base_db.h"
#include "classes/db/source_library.h"
#include "classes/db/library.h"
#include "classes/policies/data/none.h"
#include "classes/policies/data/simple.h"
#include "classes/policies/data/words.h"
#include "classes/policies/data/birds2.h"

using namespace lg::policy::data;
using namespace module::library::form;
using namespace module::library::tense;

class Test_Data : public QObject {
Q_OBJECT
    typedef Test_Data class_name;
    typedef lg::LibraryNone::LangItem::ListItems ListItems;

    QSqlDatabase db_;

    QString dbName() const {
        return "library.db";
    }

    void removeDB() {
        if(true == QFile::exists(dbName())) {
            db_.close();
            QFile::remove(dbName());
        }
    }

    void createAndOpenDB() {
        removeDB();

        db_ = QSqlDatabase::addDatabase("QSQLITE","library");
        db_.setDatabaseName(dbName());
        if(false == db_.open()) return;

        size_t done = 0;
        QSqlQuery query(db_);

        QStringList list = lg::db::source::Library::source();
        foreach (QString const& line, list) {
            done += !query.exec(line);
        }
    }

    template <typename T>
    void testInstance() {
        ListItems lists;
        sets s;
        T t;
        t.setData(lists, s);
        t.showQuestion(5u);
        t.showAnswer();
        t.hasQuestion();
        t.size();
        t.words();
        t.getSets();
    }

    void insertIntoDb() {
        lg::LibraryNone::Tense::Item tense(5u,"look", "look tense");
        auto id = lg::LibraryNone::Tense::add(tense);
        QCOMPARE(id, 1u);

        for(unsigned i = 1; i < 144;) {
            for(unsigned tense = 1; tense != 4; ++tense) {
                const auto ctense = tense == 3 ? 4 : tense;
                for(unsigned form = 1; form != 4; ++form) {
                    const auto cform = form == 3 ? 4 : form;
                    lg::LibraryNone::LangItem::Item item1(i, id, ctense, 1, cform, QLocale::Russian, QString::fromUtf8("I смотрю"),QString::fromUtf8("Ye смотришь"), QString::fromUtf8("You смотришь"), QString::fromUtf8("He смотрит"), QString::fromUtf8("She смотрит"), QString::fromUtf8("It смотрит"), QString::fromUtf8("We смотрят"), QString::fromUtf8("They смотрят"));
                    lg::LibraryNone::LangItem::Item item2(i+1, id, ctense, 1, cform, QLocale::English,"I look","ye look", "you look", "he looks", "she looks", "it looks", "we look", "they look");
                    auto id1 = lg::LibraryNone::LangItem::add(item1);
                    auto id2 = lg::LibraryNone::LangItem::add(item2);
                    QVERIFY(id1 == i);
                    QVERIFY(id2 == i+1);
                    i+=2;
                }
            }
        }

        auto vec = lg::LibraryNone::LangItem::items(id);
        QCOMPARE(vec.size(), 144u);
    }
    ListItems items() {
        auto i = lg::LibraryNone::LangItem::items(1);

        ListItems itms;
        itms.push_back(i);
        return itms;
    }

private slots:
    void initTestCase()    { //!< Called before everything else
        createAndOpenDB();
        insertIntoDb();
   }
   void cleanupTestCase() { //!< After testing
       removeDB();
   }
	void testNone() {
        None none;
        sets s;
        none.setData(5,s);
        const auto question = none.showQuestion(5u);
        QCOMPARE(question, QString());
        const auto answer = none.showAnswer();
        QCOMPARE(answer, QString());
        const auto has = none.hasQuestion();
        QCOMPARE(has, false);
        const auto size = none.size();
        QCOMPARE(size, 0u);
        testInstance<None>(); 
    } 
    void testSimple() {
        Simple simple;
        testInstance<Simple>();

        sets s;
        simple.setData(items(),s);
        QCOMPARE(simple.size(),0u);

        s.form(interrogative().first | declarative().first | negative().first);
        simple.setData(items(),s);
        QCOMPARE(simple.size(),0u);

        s.form(0);
        s.tense(present().first | past().first | future().first);
        simple.setData(items(),s);
        QCOMPARE(simple.size(),0u);

        s.form(interrogative().first | declarative().first | negative().first);
        s.tense(present().first | past().first | future().first);
        simple.setData(items(),s);
        QCOMPARE(simple.size(),144u);

        s.lang(QLocale::English);
        s.form(interrogative().first | declarative().first | negative().first);
        s.tense(present().first | past().first | future().first);
        simple.setData(items(),s);
        QCOMPARE(simple.size(),72u);

        s.lang(QLocale::Russian);
        s.form(interrogative().first | declarative().first | negative().first);
        s.tense(present().first | past().first | future().first);
        simple.setData(items(),s);
        QCOMPARE(simple.size(),72u);

        s.lang(QLocale::Russian);
        s.form(interrogative().first);
        s.tense(present().first);
        simple.setData(items(),s);
        QCOMPARE(simple.size(),8u);

	}
    void testWords() {
        Words list;
        testInstance<Words>();

        sets s;
        list.setData(items(),s);
        QCOMPARE(list.size(),0u);

        s.form(interrogative().first | declarative().first | negative().first);
        list.setData(items(),s);
        QCOMPARE(list.size(),0u);

        s.form(0);
        s.tense(present().first | past().first | future().first);
        list.setData(items(),s);
        QCOMPARE(list.size(),0u);

        s.form(interrogative().first | declarative().first | negative().first);
        s.tense(present().first | past().first | future().first);
        list.setData(items(),s);
        QCOMPARE(list.getSets(),s);
        QCOMPARE(list.size(),144u);

        s.lang(QLocale::English);
        s.form(interrogative().first | declarative().first | negative().first);
        s.tense(present().first | past().first | future().first);
        list.setData(items(),s);
        QCOMPARE(list.size(),72u);

        s.lang(QLocale::Russian);
        s.form(interrogative().first | declarative().first | negative().first);
        s.tense(present().first | past().first | future().first);
        list.setData(items(),s);
        QCOMPARE(list.size(),72u);

        s.lang(QLocale::Russian);
        s.form(interrogative().first);
        s.tense(present().first);
        list.setData(items(),s);
    }
    void testBirds2() {
          Birds2 birds2;
          testInstance<Birds2>();

          sets s;
          birds2.setData(items(),s);
          QCOMPARE(birds2.size(),0u);

          s.form(interrogative().first | declarative().first | negative().first);
          birds2.setData(items(),s);
          QCOMPARE(birds2.size(),0u);

          s.form(0);
          s.tense(present().first | past().first | future().first);
          birds2.setData(items(),s);
          QCOMPARE(birds2.size(),0u);

          s.form(interrogative().first | declarative().first | negative().first);
          s.tense(present().first | past().first | future().first);
          birds2.setData(items(),s);
          QCOMPARE(birds2.size(),144u);

          s.lang(QLocale::English);
          s.form(interrogative().first | declarative().first | negative().first);
          s.tense(present().first | past().first | future().first);
          birds2.setData(items(),s);
          QCOMPARE(birds2.size(),72u);

          s.lang(QLocale::Russian);
          s.form(interrogative().first | declarative().first | negative().first);
          s.tense(present().first | past().first | future().first);
          birds2.setData(items(),s);
          QCOMPARE(birds2.size(),72u);

          s.lang(QLocale::Russian);
          s.form(interrogative().first);
          s.tense(present().first);
          birds2.setData(items(),s);
     }
};

QTEST_MAIN(Test_Data)
#include "main.moc"

