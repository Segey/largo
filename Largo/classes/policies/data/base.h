/**
 * \file       classes/policies/data/base.h
 * \brief      The Astract Base Data policy class provides shared function for all Data policy classes
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S.Panin
 * \version    v.1.1
 * \date       Created on 30 August 2013 y., 18:54
 * \TODO
**/
#pragma once
#include <QStringList>
#include "sets.h"

/** \namespace lg::policy::data */
namespace lg {
namespace policy {
namespace data {

class Base {
public:
    typedef Base                                 class_name;
    typedef lg::LibraryNone::LangItem::Item      Item;
    typedef lg::LibraryNone::LangItem::Items     Items;
    typedef lg::LibraryNone::LangItem::ListItems ListItems;
    typedef QStringList                          words_t;

    Sets    sets_;
    words_t words_;

protected:
    unsigned tense (Item const& item) const {
        using namespace module::library::tense;
        if(sets_.tense() & past().first    && item.tense_ == past().first)    return item.tense_;
        if(sets_.tense() & present().first && item.tense_ == present().first) return item.tense_;
        if(sets_.tense() & future().first  && item.tense_ == future().first)  return item.tense_;
        return 0;
    }
    unsigned form (Item const& item) const {
        using namespace module::library::form;
        if(sets_.form() & interrogative().first && item.form_ == interrogative().first) return 8u;
        if(sets_.form() & declarative().first   && item.form_ == declarative().first)   return 16u;
        if(sets_.form() & negative().first      && item.form_ == negative().first)      return 32u;
        return 0;
    }
    void setSets(Sets const& s) {
        sets_ = s;
    }
    void setWords(words_t const& w) {
        words_ = w;
    }

public:
    explicit Base() {
    }
    virtual ~Base() {
    }
    Sets const& getSets() const {
        return sets_;
    }
    words_t const& getWords() const {
        return words_;
    }
};

}}} // end namespace lg::policy::data
