/**
 * \file       classes/policies/data/simply.h
 * \brief      The Simple Data policy class provides work with the Simple dialog
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.2
 * \date       Created on 01 August 2013 y., 01:40
 * \TODO
**/
#pragma once
#include <QTime>
#include "ps/c++/qt/rand.h"
#include "base.h"

/** \namespace lg::policy::data */
namespace lg {
namespace policy {
namespace data {

/** \namespace birds2 */
namespace birds2 {
struct Item {
    typedef Item         class_name;

    unsigned    id_;
    unsigned    lang_;
    QString     question_;
    QString     answer_;
    QString     placeholder_;

    Item() {}
    Item(unsigned id, unsigned lang, QString const& question)
        : id_(id), lang_(lang), question_(question){
    }
};

struct Lang {
    typedef Lang class_name;

    unsigned  id_;
    unsigned  lang_;
    QString   text_;

    Lang(unsigned id, unsigned lang, QString const& text)
        : id_(id), lang_(lang), text_(text) {
    }
    Lang(){}
    friend bool operator ==(Lang const& lhs, Lang const& rhs) {
        return lhs.id_ == rhs.id_ && lhs.lang_ == rhs.lang_ && lhs.text_ == lhs.text_;
    }
    friend bool operator !=(Lang const& lhs, Lang const& rhs) {
        return !(lhs == rhs);
    }
};
} // end namespace birds2

/**
 *  \brief The Birds2 class provides easy work with the Birds2 dialog
 *   \code
 *        using namespace lg::policy::data;
 *        Birds2 words;
 *        words.start();
 *        words.stop();
 *        auto data = words.getData();
 *   \endcode
**/
class Birds2: public Base {
public:
    typedef Birds2                               class_name;
    typedef Base                                 inherited;
    typedef birds2::Lang                         lang_t;
    typedef QVector<lang_t>                      langs_t;
    typedef unsigned                             id_t;
    typedef birds2::Item                         item_t;
    typedef QMultiMap<id_t,item_t>               imaps_t;
    typedef lg::LibraryNone::LangItem::Item      Item;
    typedef lg::LibraryNone::LangItem::Items     Items;
    typedef lg::LibraryNone::LangItem::ListItems ListItems;
    typedef QPair<QString, QString>              question_t;
    typedef QVector<item_t>                      items_t;

private:
    langs_t ru_;
    langs_t en_;
    item_t  current_;
    items_t items_;

    void createMap(imaps_t& imap, Item const& item)  {
        auto t  = inherited::tense(item);
        auto f  = inherited::form(item);
        if(!t || !f) return;
        unsigned result = t | f;

        langs_t langs;
        langs.push_back(lang_t((result | 64),  item.lang_, item.I_));
        langs.push_back(lang_t((result | 128), item.lang_, item.ye_));
        langs.push_back(lang_t((result | 256), item.lang_, item.you_));
        langs.push_back(lang_t((result | 512), item.lang_, item.he_));
        langs.push_back(lang_t((result | 1024),item.lang_, item.she_));
        langs.push_back(lang_t((result | 2048),item.lang_, item.it_));
        langs.push_back(lang_t((result | 4096),item.lang_, item.we_));
        langs.push_back(lang_t((result | 8192),item.lang_, item.they_));

        auto& la = item.lang_ == QLocale::English ? en_ : ru_;

        foreach (auto lang, langs) {
            imap.insert(lang.id_, item_t(lang.id_, lang.lang_, lang.text_));
            la.push_back(lang);
        }
    }
    void setPlaceHolder(item_t& item, unsigned lang, QString const& answer) {
        item.answer_ = answer;

        if(ps::randBool()) {
            item.placeholder_ = answer;
            return;
        }

        auto const& array = item.lang_ == QLocale::English ? ru_ : en_;

        auto it = qFind(array.begin(), array.end(), lang_t(item.id_, lang, item.answer_));
        if(it == array.end()) return;

        const auto hole = std::distance(array.begin(), it);
        auto index = ps::randIntWithHole(0, array.size() -1, hole);
        item.placeholder_ = array[index].text_;

     //   qDebug() << item.question_ << item.answer_ << item.placeholder_;
    }

public:
    explicit Birds2() {
    }
    ~Birds2() {
    }
    void setData(ListItems const& list, words_t const& w, Sets const& s) {
        inherited::setSets(s);
        inherited::setWords(w);

        items_.clear();
        items_.reserve(144);
        std::for_each(list.begin(), list.end(), [&] (Items const& items) {
            imaps_t imap;
            std::for_each(items.begin(), items.end(), [&] (Item const& item) {
                createMap(imap, item);
            });
            foreach (auto index, imap.uniqueKeys()) {
                auto const& vals = imap.values(index);
                auto first  = vals.front();
                auto second = vals.back();
                setPlaceHolder(first,  second.lang_, second.question_);
                setPlaceHolder(second, first.lang_,  first.question_);

                if(s.lang() == QLocale::AnyLanguage || s.lang() == first.lang_)  items_.push_back(first);
                if(s.lang() == QLocale::AnyLanguage || s.lang() == second.lang_) items_.push_back(second);
            }
        });
    }
    question_t showQuestion(unsigned next) {
        if(static_cast<int>(next) >= items_.size()) return question_t();
        auto it   = items_.begin() + next;
        current_  = (*it);
        items_.erase(it);
        return question_t(qMakePair(current_.question_, current_.placeholder_));
    }
    QString showAnswer() const {
        return current_.answer_;
    }
    bool hasQuestion() const {
        return false == items_.empty();
    }
    unsigned size() const {
        return items_.size();
    }
};

}}} // end namespace lg::policy::data
