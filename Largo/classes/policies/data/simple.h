/**
 * \file       classes/policies/data/simply.h
 * \brief      The Simple Data policy class provides work with the Simple dialog
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.1
 * \date       Created on 01 August 2013 y., 01:40
 * \TODO
**/
#pragma once
#include <QTime>
#include "base.h"

/** \namespace lg::policy::data */
namespace lg {
namespace policy {
namespace data {

/** \namespace simple */
namespace simple {
struct Item {
    typedef Item   class_name;

    unsigned    lang_;
    QString     question_;
    QString     answer_;

    Item() {}
    Item(unsigned lang, QString const& question)
        : lang_(lang), question_(question){
    }
};
} // end namespace simple

/**
 *  \brief The Simple class provides easy work with the Simple dialog 
 *   \code
 *        using namespace lg::policy::data;
 *        Simple simple;
 *        simple.start();
 *        simple.stop();
 *        auto data = simple.getData();
 *   \endcode
**/
class Simple : public Base {
public:
    typedef Simple                               class_name;
    typedef Base                                 inherited;
    typedef simple::Item                         item_t;
    typedef QVector<item_t>                      data_t;
    typedef QMultiMap<unsigned, item_t>          imap_t;
    typedef lg::LibraryNone::LangItem::Item      Item;
    typedef lg::LibraryNone::LangItem::Items     Items;
    typedef lg::LibraryNone::LangItem::ListItems ListItems;

private:
    data_t   items_;
    item_t   current_;

    void createMap(imap_t& imap, Item const& item)  {
        auto t  = inherited::tense(item);
        auto f  = inherited::form(item);
        if(!t || !f) return;
        unsigned result = t | f;

        imap.insert((result | 64),  item_t(item.lang_, item.I_));
        imap.insert((result | 128), item_t(item.lang_, item.ye_));
        imap.insert((result | 256), item_t(item.lang_, item.you_));
        imap.insert((result | 512), item_t(item.lang_, item.he_));
        imap.insert((result | 1024),item_t(item.lang_, item.she_));
        imap.insert((result | 2048),item_t(item.lang_, item.it_));
        imap.insert((result | 4096),item_t(item.lang_, item.we_));
        imap.insert((result | 8192),item_t(item.lang_, item.they_));
    }

public:
    explicit Simple() {
    }
    ~Simple() {
    }
    void setData(ListItems const& list, words_t const& w, Sets const& s) {
        inherited::setSets(s);
        inherited::setWords(w);


        items_.clear();
        items_.reserve(144 * list.size());
        std::for_each(list.begin(), list.end(), [&] (Items const& items) {
            imap_t imap;
            std::for_each(items.begin(), items.end(), [&] (Item const& item) {
                createMap(imap, item);
            });

            foreach (unsigned index, imap.uniqueKeys()) {
                auto const& vals = imap.values(index);
                auto first  = vals.front();
                auto second = vals.back();

                first.answer_  = second.question_;
                second.answer_ = first.question_;

                 if(s.lang() == QLocale::AnyLanguage || s.lang() == first.lang_)  items_.push_back(first);
                 if(s.lang() == QLocale::AnyLanguage || s.lang() == second.lang_) items_.push_back(second);
            }
        });
    }
    QString showQuestion(unsigned next) {
        if(static_cast<int>(next) >= items_.size()) return QString();
        auto it   = items_.begin() + next;
        current_  = (*it);
        items_.erase(it);
        return current_.question_;
    }
    QString showAnswer() {
        return current_.answer_;
    }
    bool hasQuestion() const {
       return false == items_.empty();
    }
    unsigned size() const {
        return items_.size();
    }
};

}}} // end namespace lg::policy::data
