/**
 * \file       classes/policies/data/none.h
 * \brief      The Abstract Data policy class provides virtual functions
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.2
 * \date       Created on 01 August 2013 y., 01:31
 * \TODO
**/
#pragma once
#include "base.h"

/** \namespace lg::policy::data */
namespace lg {
namespace policy {
namespace data {

/**
 *  \brief The Empty class is nothing doing 
 *   \code
 *        using namespace lg::policy::data;
 *        None none;
 *        auto data = none.showAnswer();
 *        Q_ASSERT(data == QString());
 *   \endcode
**/
class None : public Base {
public:
    typedef None  class_name;
    typedef Base  inherited;

public:
    explicit None() {}
    ~None() {}

    void setData(ListItems const& /* list */, words_t const& w, Sets const& s) {
        inherited::setSets(s);
        inherited::setWords(w);
    }
    QString showQuestion(unsigned /* next */) {
        return QString();
    }
    QString showAnswer() {
        return QString();
    }
    bool hasQuestion() const {
        return false;
    }
    unsigned size() const {
        return 0u;
    }
};

}}} // end namespace lg::policy::data
