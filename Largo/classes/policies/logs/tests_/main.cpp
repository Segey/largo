#include <QtTest/QtTest>
#include <QTGui>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <iostream>
#include <vector>
#include <algorithm>
#include "tr.h"
#include "const.h"
#include "ps/c++/qt/macros.h"
#include "ps/c++/qt/strategy/strategy_message.h"
#include "ps/c++/qt/db/sqlite/serialization.h"
#include "ps/c++/qt/db/sqlite/base_db.h"
#include "classes/db/source_settings.h"
#include "classes/db/settings.h"
#include "classes/policies/logs/none.h"
#include "classes/policies/logs/v1.h"

using namespace lg::policy::logs;

class Test_Logs : public QObject {
Q_OBJECT
    typedef Test_Logs class_name;

	QSqlDatabase db_;

    QString dbName() const {
        return "settings.db";
    }
    void removeDB() {
        if(true == QFile::exists(dbName())) {
            db_.close(); 
            QFile::remove(dbName());
        }
    }
	void createAndOpenDB() {
        removeDB();

        db_ = QSqlDatabase::addDatabase("QSQLITE","settings");
        db_.setDatabaseName(dbName());
        if(false == db_.open()) return;

        size_t done = 0;
        QSqlQuery query(db_);

        QStringList list = lg::db::source::Settings::source();
        foreach (QString const& line, list) {
            done += !query.exec(line);
        } 
    }

    template <typename T>
    void testInstance() {
        T t;
        t.save(QTime(), QStringList(), sets());
        t.get();
    }

private slots:
    void initTestCase()    { //!< Called before everything else
        createAndOpenDB();
   }
   void cleanupTestCase() { //!< After testing
       removeDB();
   }
	void testNone() {
        None none;
        auto done = none.save(QTime(), QStringList(), sets());
        QVERIFY(done == true);
        auto str = none.get();
        QVERIFY(str == QString());
        testInstance<None>();
    } 
    void testLogs() {
        using namespace lg::settings;
        using namespace module::library::form;
        using namespace module::library::tense;

        sets s;
        s.form(interrogative().first | declarative().first | negative().first);

        LogV1 log2(QTime(3,25,43), QStringList() << "look", s);
        QVERIFY(0 != LogV1::toDB(log2));
        auto logs2 = LogV1::allLogsFromDB();
        QVERIFY(logs2.size() == 1);
        auto log3 = logs2.back();
        QVERIFY(log3.duration() == QTime(3,25,43));
        QVERIFY(log3.settings() == s);
        QVERIFY(log3.words() ==  QStringList() << "look");

        s.lang(QLocale::English);
        s.form(interrogative().first | declarative().first | negative().first);
        s.tense(present().first | past().first | future().first);
        LogV1 log4(QTime(1,5,3), QStringList() << "look" << "see", s);
        QVERIFY(0 != LogV1::toDB(log4));
        auto logs4_1 = LogV1::allLogsFromDB();
        QVERIFY(logs4_1.size() == 2);
        auto log4_2 = logs4_1.back();
        QVERIFY(log4_2.duration() == QTime(1,5,3));
        QVERIFY(log4_2.settings() == s);
        QVERIFY(log4_2.words()    == QStringList() << "look" << "see");

	}
    void testV1() {
        using namespace lg::settings;
        using namespace module::library::form;
        using namespace module::library::tense;

        sets s;
        s.form(interrogative().first | declarative().first);

        V1 v1;
        QVERIFY(v1.save(QTime(1,8,33), QStringList() << "look",  s) == true);
        auto s1 = v1.get();
        QVERIFY(s1.size() > 30);
        QVERIFY(s1.indexOf(" interrogative+declarative") !=-1);
        QVERIFY(s1.indexOf(" all ") !=-1);
        QVERIFY(s1.indexOf(" 01:08:33 ") !=-1);
        QVERIFY(s1.indexOf(" {look} ") !=-1);

        s.form(declarative().first);
        s.tense(future().first);
        s.lang(QLocale::English);
        s.dialog(QLocale::English);
        QVERIFY(v1.save(QTime(5,3,14), QStringList() << "look" << "see", s) == true);
        s1 = v1.get();
        QVERIFY(s1.size() > 30);
        QVERIFY(s1.indexOf(" english ") !=-1);
        QVERIFY(s1.indexOf(" simple ") !=-1);
        QVERIFY(s1.indexOf(" 05:03:14 ") !=-1);
        QVERIFY(s1.indexOf(" future ") !=-1);
        QVERIFY(s1.indexOf(" declarative") !=-1);
        QVERIFY(s1.indexOf(" {look,see} ") !=-1);
        //qDebug() << s1;
        testInstance<V1>();
    }
};

QTEST_MAIN(Test_Logs)
#include "main.moc"

