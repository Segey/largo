/**
 * \file       classes/policies/logs/none.h
 * \brief      The Abstract Logs policy class provides virtual functions
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.1
 * \date       Created on 13 September 2013 y., 13:41
 * \TODO
**/
#pragma once
#include <QTime>
#include <QString>
#include "const.h"
#include "sets.h"

/** \namespace lg::policy::logs */
namespace lg {
namespace policy {
namespace logs {

/**
 *  \brief The Empty class nothing doing 
 *  \code
*         using namespace lg::policy::logs;
 *        None none;
 *        none.save(s,duration);
 *        auto str = none.get();
 *        qDebug() << str;
 *  \endcode
**/
class None {
public:
    typedef None  class_name;

public:
    explicit None() {}
    ~None() {}
    QString get() const {
        return QString();
    }
    bool save( QTime const& /* duration */, QStringList const& /* words */, Sets const& /* s */) {
        return true;
    }
};

}}} // end namespace lg::policy::logs
