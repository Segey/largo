/**
 * \file       classes/policies/logs/v1.h
 * \brief      The Logs policy class provides the simplest work with logs (Version 1)
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.3
 * \date       Created on 13 September 2013 y., 14:25
 * \TODO
**/
#pragma once
#include <QTime>
#include <QString>
#include <QStringList>
#include "const.h"
#include "sets.h"
#include "tr.h"
#include "classes/db/settings/logsv1.h"

/** \namespace lg::policy::logs */
namespace lg {
namespace policy {
namespace logs {

/**
 *  \brief The V1 class provides easy work with QTime 
 *   \code
 *        using namespace lg::policy::logs;
 *        V1 logs;
 *        logs.save(s,duration);
 *        auto str = logs.get();
 *        qDebug() << str;
 *   \endcode
**/
class V1 {
public:
    typedef V1                  class_name;
    typedef lg::settings::LogV1 logV1_t;

    unsigned  log_id_;

private:
    /**
     * \brief The Words to String
    **/
    static QString getWords (QStringList const& w) {
        if(w.size() == 0) return QString();

        QString result = w[0];
        for(int i = 1; i != w.size(); ++i) {
            result += "," + w[i];
        }
        return result;
    }

    static QString getLog(logV1_t const& log)  {
        QString str = log.count() == 0 ? QString() : QString("%1/%2").arg(log.result()).arg(log.count());
        return  QString(iTemplate::tr("[%1] %2 %3 {%4} %5 %6 %7 %8"))
                              .arg(log.time().toString(Qt::SystemLocaleShortDate))
                              .arg(log.duration().toString())
                              .arg(sets::dialogToString(log.settings()))
                              .arg(getWords(log.words()))
                              .arg(sets::langToString(log.settings()))
                              .arg(sets::tenseToString(log.settings()))
                              .arg(sets::formToString(log.settings()))
                              .arg(str)
                               ;
    }

public:
    explicit V1()
        : log_id_(0) {
    }
    ~V1() {
    }
    void setLogId(unsigned log_id) {
        log_id_ = log_id;
    }
    unsigned logId() const {
        return log_id_;
    }
    QString get() const {
        if(log_id_ == 0 ) return QString();

        auto log =  logV1_t::fromDB(log_id_);
        return getLog(log);
    }
    bool save(QTime const& duration, QStringList const& words, Sets const& s) {
        log_id_ =  logV1_t::toDB(logV1_t(duration, words, s));
        return log_id_ != 0;
    }
    bool save(QTime const& duration, QStringList const& words, Sets const& s, unsigned result, unsigned count) {
        log_id_ =  logV1_t::toDB(logV1_t(duration, words, s, result, count));
        return log_id_ != 0;
    }
    static QStringList getAllLogs() {
        QStringList result;
        auto logs = logV1_t::allLogsFromDB();
        foreach (const auto& log, logs) {
            result << getLog(log);
        }
        return result;
    }
    static void clearAllLogs() {
        logV1_t::clearAllLogs();
    }
};

}}} // end namespace lg::policy::logs
