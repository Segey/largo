/**
 * \file       classes/policies/times/none.h
 * \brief      The Abstract Time policy class provides virtual functions
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.2
 * \date       Created on 31 July 2013 y., 23:51
 * \TODO
**/
#pragma once

/** namespace lg::policy::time */
namespace lg {
namespace policy {
namespace time {

/**
 *  \brief The Empty class nothing doing 
 *   \code
*         using namespace lg::policy::time;
 *        None none;
 *        none.start();
 *        none.stop();
 *        auto time = none.time();
 *        QCOMPARE(time, QTime(0,0,0));
 *   \endcode
**/
class None {
public:
    typedef None  class_name;

public:
    explicit None() {}
    ~None() {}
    void start() {
    }
    void stop() {
    }
    void restart() {
    }
    QTime time() const {
        return QTime(0,0,0);
    }
};

}}} // end namespace lg::policy::time
