#include <QtTest/QtTest>
#include <QTGui>
#include "classes/policies/times/none.h"
#include "classes/policies/times/simply.h"

using namespace lg::policy::time;

class Test_Time : public QObject {
Q_OBJECT

    template <typename T>
    void testInstance() {
        T t;
        t.start();
        t.stop();
        t.restart();
        t.time();
    }

private slots:
    void initTestCase()    { //!< Called before everything else
    }      
    void cleanupTestCase() { //!< After testing
    }
	void testNone() {
        None none;
        none.start();
        none.stop();
        none.restart();
        QCOMPARE(none.time(), QTime(0,0,0));
        testInstance<None>(); 
    } 
	void testSimple() {
        Simple simple;
        QCOMPARE(simple.time(), QTime(0,0,0));
        simple.start();
        QTest::qSleep(10);
        simple.stop();
        QVERIFY(simple.time() != QTime(0,0,0));
        simple.restart(); 
        QVERIFY(simple.time() != QTime(0,0,0));
        testInstance<Simple>(); 
	}
};

QTEST_MAIN(Test_Time)
#include "main.moc"

