/**
 * \file       classes/policies/times/simply.h
 * \brief      The Simple Time policy class provides the simplest work with time
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.1
 * \date       Created on 01 August 2013 y., 00:12
 * \TODO
**/
#pragma once
#include <QTime>

/** namespace lg::policy::time */
namespace lg {
namespace policy {
namespace time {

/**
 *  \brief The Simple class provides easy work with QTime 
 *   \code
 *        using namespace lg::policy::time;
 *        Simple simple;
 *        simple.start();
 *        simple.stop();
 *        const auto t = simple.time();
 *   \endcode
**/
class Simple {
public:
    typedef Simple class_name;

    QTime  time_;

private:
    void instanceTime() {
        time_ = QTime(0,0,0);
    }

public:
    explicit Simple()
        : time_(0,0,0) {
    }
    ~Simple() {
    }
    void start() {
        instanceTime();
        time_.start();
    }
    void stop() {
        auto result = time_.elapsed();
        instanceTime();
        time_ = time_.addMSecs(result);
    }
    void restart() {
        time_.restart();
    }
    QTime time() const {
        return time_;
    }
};

}}} // end namespace lg::policy::time
