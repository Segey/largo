/**
 * \file      save_restore_window.h
 * \brief     The File is used to describe the parameters necessary for creating window.
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.1
 * \date      Created on 17 July 2013 y., 23:53
 * \TODO
**/
#pragma once
#include <QMainWindow>

/** namespace lg */
namespace lg {

/**
 * \code 
 *    class DisplaySimpleWordseMainWindow : public lg::SaveRestoreMainWindow;
 * \endcode
**/
class SaveRestoreMainWindow : public QMainWindow {
    typedef SaveRestoreMainWindow class_name;
    typedef QMainWindow           inherited;

private:
    QLatin1String settins_location_name_;

protected:
    virtual void settingsLocationRead() {
       lg::SettingsNone::restoreWindow(settins_location_name_, this);
    }
    virtual void settingsLocationWrite() {
       lg::SettingsNone::saveWindow(settins_location_name_, this);
    }
    virtual void closeEvent(QCloseEvent* /* event */ ) {
        settingsLocationWrite();
    }
public:
    explicit SaveRestoreMainWindow (QLatin1String const& name, QWidget* parent = nullptr)
        : inherited(parent), settins_location_name_(name) {
    }
};

} // end namespace lg
