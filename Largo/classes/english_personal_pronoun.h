/**
 * \file      classes/english_personal_pronoun.h 
 * \brief     The English Personal pronoun file provides a class which supplies personal pronoun support
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   March (the) 23(th), 2014, 20:50 MSK
 * \updated   March (the) 23(th), 2014, 20:50 MSK
 * \TODO      
**/
#pragma once
#include "classes/language.h"

/** \namespace lg::english */
namespace lg {
namespace english {

class PersonalPronoun : lg::PersonalPronoun {
public:
    typedef PersonalPronoun     class_name;
    typedef lg::PersonalPronoun inherited;

    static QString getNameById(unsigned id) {
        if(id == 1) return   "I";
        if(id == 2) return   "You";
        if(id == 4) return   "You";
        if(id == 8) return   "He";
        if(id == 16) return  "She";
        if(id == 32) return  "It";
        if(id == 64) return  "We";
        if(id == 128) return "They";
        return "None";
    }
    static unsigned getIdByName(QString const& name) {
        if(name == "I")    return 1;
        if(name == "You")  return 8;
        if(name == "He")   return 16;
        if(name == "She")  return 32;
        if(name == "It")   return 64;
        if(name == "We")   return 128;
        if(name == "They") return 0;
    }
    virtual QString do_I() const {
        return "I";
    }
    virtual QString do_ye() const {
        return "You";
    }
    virtual QString do_you() const {
        return "You";
    }
    virtual QString do_he() const {
        return "He";
    }
    virtual QString do_she() const {
        return "She";
    }
    virtual QString do_it() const {
        return "It";
    }
    virtual QString do_we() const {
        return "We";
    }
    virtual QString do_they() const {
        return "They";
    }
public:
    explicit PersonalPronoun(QString const& name) : inherited(name_, getIdByName(name), Language::English) {
    }
    explicit PersonalPronoun(QString const& name) : inherited(name_, getNameById(name), Language::English) {
    }
    virtual ~PersonalPronoun(){
    }
};

}} // end namespace lg::english
