/** 
 * \file       classes/db/settings/logsv1.h
 * \brief      File for work with DB and Logs version 1
 * \author     S.Panin <dix75@mail.ru>
 * \copyright  S.Panin
 * \version    v.2.2
 * \date       Created on 03 September 2013 y., 23:34
 * \TODO
**/
#pragma once
#include <QStringList>
#include <QDateTime>
#include "sets.h"
#include "classes/db/settings.h"

/** \namespace lg::settings  */
namespace lg {
namespace settings {

/**
 * \brief The Logs version 1
 * \code
 *     using namespace lg::settings;
 *     LogV1 log();
 *  \endcode
**/
class LogV1 {
public:
    typedef LogV1                  class_name;
    typedef QVector<LogV1>         LogsV1;
    typedef QStringList           words_t;

 private:
    unsigned  log_id_;
    QDateTime time_;
    QTime     duration_;
    words_t   words_;
    Sets      sets_;
    unsigned  result_;
    unsigned  count_;

public:
    LogV1(unsigned log_id, QDateTime const& time, QTime const& duration, words_t const& words, Sets const& s, unsigned result, unsigned count)
        : log_id_(log_id), time_(time), duration_(duration), words_(words), sets_(s), result_(result), count_(count) {
    }
    LogV1(QTime const& duration, words_t const& words, Sets const& s, unsigned result, unsigned count)
        : log_id_(0u), time_(QDateTime::currentDateTime()), duration_(duration), words_(words), sets_(s), result_(result), count_(count) {
    }
    LogV1(QTime const& duration, words_t const& words, Sets const& s)
        : log_id_(0u), time_(QDateTime::currentDateTime()), duration_(duration), words_(words), sets_(s), result_(0), count_(0) {
    }
    LogV1()
        : log_id_(0u), time_(QDateTime::currentDateTime()), duration_(QTime(0,0,0)),  words_(words_t()), sets_(Sets()), result_(0), count_(0) {
    }
    QDateTime const& time() const {
        return time_;
    }
    void setTime(QDateTime const& time) {
        time_ = time;
    }
    QTime const& duration() const {
        return duration_;
    }
    void setDuration(QTime const& d) {
        duration_ = d;
    }
    unsigned logId() const {
        return log_id_;
    }
    void setLogId(unsigned d) {
        log_id_ = d;
    }
    const Sets& settings() const {
        return sets_;
    }
    void setSettings(Sets const& s) {
        sets_ = s;
    }
    const words_t& words() const {
        return words_;
    }
    void setWords(words_t const& w) {
        words_ = w;
    }
    unsigned result() const {
        return result_;
    }
    void setResult(unsigned c)  {
        result_ = c;
    }
    unsigned count() const {
        return count_;
    }
    void setCount(unsigned c)  {
        count_ = c;
    }
     /**
      * \brief Inserts a Log to DB
      * \result {out: bool} The success of this operation
      * \code
      *     using namespace lg::settings;
      *     LogV1 log(0, QDateTime(QDate(2012,7,6), QTime(8,30,0)), QStringList() << "see", s, 1, 5);
      *     QVERIFY(0 != LogV1::toDB(log));
      * \endcode
     **/
     static unsigned toDB(LogV1 const& log) {
        QSqlQuery query(Settings::database());
        query.prepare("INSERT INTO Logs1(time, duration, word, settings, result, count) VALUES(?,?,?,?,?,?);");
        query.addBindValue(log.time_);
        query.addBindValue(log.duration_);
        query.addBindValue(ps::Serialization48::to(log.words_));
        query.addBindValue(Sets::toString(log.sets_));
        query.addBindValue(log.result_);
        query.addBindValue(log.count_);
        query.exec();
        return Settings::getLastRowId(query);
     }
     /**
      * \brief Getting All logs from the db
      * \code
      *      using namespace lg::settings;
      *      auto logs = LogV1::allLogsFromDB();
      *      QVERIFY(logs.size() !=0);
      * \endcode
     **/
     static LogsV1 allLogsFromDB() {
          LogsV1 result;
          QSqlQuery query(Settings::database());
          query.exec("SELECT log1_id, time, duration, word, settings, result, count FROM Logs1 ORDER BY time DESC LIMIT 50;");
          while(query.next()) {
              const words_t& w = ps::Serialization48::from<words_t>(query.value(3).toString());
              const Sets& s    = Sets::fromString(query.value(4).toString());
              LogV1 log(query.value(0).toUInt(), query.value(1).toDateTime(), query.value(2).toTime(), w, s, query.value(5).toInt(), query.value(6).toInt());
              result.push_back(log);
          }
          return result;
      }
     /**
      * \brief Getting a log from the db
      * \code
      *      using namespace lg::settings;
      *      auto log = LogV1::fromDB(1);
      *      QVERIFY(log !=0);
      * \endcode
     **/
     static LogV1 fromDB(unsigned id) {
          QSqlQuery query(Settings::database());
          query.exec(QString("SELECT log1_id, time, duration, word, settings, result, count FROM Logs1 WHERE log1_id = %1 LIMIT 1;").arg(id));
          if(query.next()) {
              const words_t& w = ps::Serialization48::from<words_t>(query.value(3).toString());
              const Sets& s = Sets::fromString(query.value(4).toString());
              LogV1 log(query.value(0).toUInt(), query.value(1).toDateTime(), query.value(2).toTime(), w, s, query.value(5).toInt(), query.value(6).toInt());
              return log;
          }
          return LogV1();
      }
     /**
      * \brief Clearing all Logs from db
      * \code
      *      using namespace lg::settings;
      *      auto done = LogV1::clearAllLogs();
      *      QVERIFY(done);
      * \endcode
     **/
     static bool clearAllLogs() {
          QSqlQuery query(Settings::database());
          query.exec("DELETE FROM Logs1;");
          return query.next();
      }
};

}} // end namespace lg::settings

