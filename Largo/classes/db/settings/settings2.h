/** 
 * \file       classes/db/settings/settings2.h
 * \brief      File for work with DB and Settings with 2 fields
 * \author     S.Panin <dix75@mail.ru>
 * \copyright  S.Panin
 * \version    v.1.1
 * \date       Created on 01 November 2013 y., 19:37
 * \TODO
**/
#pragma once
#include "classes/db/settings.h"

/** \namespace lg::settings  */
namespace lg {
namespace settings {

/**
 * \brief The Settings2 with 2 fields
 * \code
 *     using namespace lg::settings;
 *     Settings2 set2();
 *  \endcode
**/
class Settings2 {
public:
    typedef Settings2                  class_name;
    typedef QVector<Settings2>         LogsV1;

 private:
    QString  name_;
    QString  value_;

public:
    Settings2(QString const& name, QString const& value)
        : name_(name), value_(value) {
    }
    Settings2()
        : name_(QString()), value_(QString()){
    }
    QString const& name() const {
        return name_;
    }
    QString const& value() const {
        return value_;
    }
    void setName(QString const& name) {
        name_ = name;
    }
    void setValue(QString const& value) {
        value_ = value;
    }
     /**
      * \brief Inserts a Setting value to DB
      * \result {out: bool} The success of this operation
      * \code
      *     using namespace lg::settings;
      *     Settings2 s2("Dialog", "2");
      *     QVERIFY(true != Settings2::toDB(s2));
      * \endcode
     **/
    static bool toDB(Settings2 const& s2) {
       QSqlQuery query(Settings::database());
       query.exec(QString("SELECT name FROM settings2 WHERE name = '%1' LIMIT 1;").arg(s2.name()));
       if(query.next()) return query.exec(QString("UPDATE Settings2 SET value ='%2' WHERE name = '%1';").arg(s2.name()).arg(s2.value()));
       else return query.exec(QString("INSERT INTO Settings2(name, value) VALUES ('%1', '%2');").arg(s2.name()).arg(s2.value()));
    }
     /**
      * \brief Getting The Settings2 from the db
      * \code
      *      using namespace lg::settings;
      *      auto logs = Settings2::fromDB("Name", "3);
      *      QVERIFY(logs.size() !=0);
      * \endcode
     **/
     static Settings2 fromDB(QString const& name, QString const& default_value) {
          QSqlQuery query(Settings::database());
          query.exec(QString("SELECT name, value FROM settings2 WHERE name='%1' LIMIT 1;").arg(name));
          if(query.next()) return Settings2(name, query.value(1).toString());
          return Settings2(name, default_value);
      }
};

}} // end namespace lg::settings

