/**
 * \file      classes/db/source_library.h
 * \brief     Sources for Library
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S. Panin
 * \version   v.1.6
 * \date      Created on 19 July 2013 y., 23:45
 * \TODO
**/
#pragma once

/** \namespace lg::db::source  */
namespace lg {
namespace db {
namespace source {

/** \brief The Script to create the sql library */
class Library {
public:
    typedef Library	class_name;

private:
	class Tenses {
		typedef Tenses class_name;

		static QString table() {
            return "CREATE TABLE Tenses ("
                "tense_id    INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                "name        TEXT NOT NULL,"
                "description TEXT"
                ")";
		}
		static QString index() {
			return "CREATE UNIQUE INDEX Tenses_tense_id ON Tenses(tense_id);";
		}
	public:
		static QStringList get() {
			QStringList result;
			result.push_back(table());
			result.push_back(index());
			return result;
		}
	};
	class Items {
		typedef Items class_name;

		static QString table() {
            return "CREATE TABLE Items ("
                "item_id  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                "tense_id INTEGER NOT NULL,"
                "tense    INTEGER NOT NULL," // The Time   line
                "type     INTEGER NOT NULL," // The Time's Type
                "form     INTEGER NOT NULL," // The Form   of verbs
                "lang     INTEGER NOT NULL,"
                "I        TEXT,"
                "ye       TEXT,"
                "you      TEXT,"
                "he       TEXT,"
                "she      TEXT,"
                "it       TEXT,"
                "we       TEXT,"
                "they     TEXT"
                ");";
		}
		static QString index() {
			return "CREATE INDEX Items_item_id ON Items(tense_id);";
		}
	public:
		static QStringList get() {
			QStringList result;
			result.push_back(table());
			result.push_back(index());
			return result;
		}
	};
    static QString info() {
        return "CREATE TABLE program_info ( "
			"program TEXT NOT NULL UNIQUE,"
			"database TEXT NOT NULL UNIQUE,"
			"version INTEGER NOT NULL UNIQUE"
			")";
    }
    static QString insertInfo() {
        return QString("INSERT INTO program_info(program, database, version) VALUES('%1', '%2', '%3');")
			.arg(program::name()).arg(module::library::name()).arg(module::library::version());
    }
public:
    /**
     * \brief Getting text line.
     * \result QStringList - The Text list.
    **/
    static QStringList source() {
        QStringList list;
		list << Tenses::get();
		list << Items::get();
        list.push_back(class_name::info());
        list.push_back(class_name::insertInfo());
        return list;
    }
};
}}} // end namespace lg::db::source

