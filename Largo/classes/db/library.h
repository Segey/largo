/**
 * \file      classes/db/sqlite/library.h
 * \brief     The file for work with Library
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin
 * \version   v.1.6
 * \date      Created on 23 July 2012 y., 12:00
 * \URL       http://www.sqlite.org/docs.html 
 * \TODO		
**/
#pragma once
/** \namespace lg  */
namespace lg {

/**
 * \brief The base Library
 * \param S - Error strategy
 * \see ps::base_bd;
 * 
 *  Example code:
 *  \code
 * 	   lg::LibraryNone library;
 * 	   library.saveForm(name, this);
 *  \endcode
**/
template<typename S>
class LibraryT : private ps::base_db<S> {
public:
    typedef S                       strategy_type;
    typedef LibraryT<strategy_type> class_name;
    typedef ps::base_db<S>          inherited;

public:
	/** \brief Ctor */
    LibraryT() { 
		inherited::connect(module::library::file(), module::library::name());	
	}
	bool connect() {
		return true;
	}
	/** \brief Getting link to the dababase **/
    static QSqlDatabase database() { 
        return inherited::database(module::library::name()); 
    }
	/**
	 * \brief Creating the database
	 * \result bool - Success of this operation
	 * \code
     * 	lg::LibraryT<lg::strategy::message::window>::createDatabase();
	 * or 
     * 	lg::Library::createDatabase();
	 * \endcode
	**/
    static bool createDatabase() {
        return inherited::createDatabase(lg::db::source::Library::source(), module::library::file(), module::library::name());
	}	
    static bool tryToUpdate() {
        unsigned version = module::library::version().toUInt();
        ::lg::db::update::library::Worker worker(database(),version);
        return worker();
    }
    struct Tense {
        struct Item;
        typedef std::vector<Item> Items;

        /**
         * \brief  This class provides all work with tense
         * \code
         *    lg::LibraryNone::Tense::Item item0(1,"Look","Look description");
         * \endcode
         **/
        struct Item {
            typedef Item       class_name;

            unsigned tense_id_;         //!< The Tense ID
            QString  name_;             //!< The Name
            QString  description_;      //!< The Description
            Item() : tense_id_(0u), name_(QString()), description_(QString()) {}
            Item(unsigned tense_id, QString const& name, QString const& description)
                : tense_id_(tense_id), name_(name), description_(description) {}
            friend bool operator == (Item const& lhs, Item const& rhs) {
                return lhs.tense_id_ == rhs.tense_id_ && lhs.name_ == rhs.name_ && lhs.description_ == rhs.description_;
            }
            friend bool operator != (Item const& lhs, Item const& rhs) {
                return !(lhs == rhs);
            }
        };
        /**
         * \brief This function adds new Tense into the db
         * \result {out: unsigned} The last insert row id
         * \code
         *      lg::LibraryNone::Tense::Item item(0,"Look","Look description");
         *      auto id = lg::LibraryNone::Tense::add(item);
         *      QVERIFY(id != 0u);
         * \endcodel
         */
        static unsigned add(Item const& item) {
            QSqlQuery query(database());	
            auto result = query.exec(QString("INSERT INTO Tenses(name, description) VALUES('%1','%2');").arg(item.name_).arg(item.description_));
            if(false == result) return 0u;
            return inherited::getLastRowId(query);
        }
        /**
         * \brief Updating tense
         * \result {out: bool} The success of this operation 
         * \code
         *      lg::LibraryNone::Tense::Item item(1,"Look","Look description");
         *      auto done = lg::LibraryNone::Language::update(item);
         *      QCOMPARE(done, true);
         * \endcode
         */
        static bool update(Item const& newItem) {
            QSqlQuery query(database());	
            return query.exec(QString("UPDATE Tenses SET  name='%2', description = '%3' WHERE tense_id = '%1';").arg(newItem.tense_id_).arg(newItem.name_).arg(newItem.description_));
        }
        /**
         * \brief This function removes a Tense id
         * \result {out: bool} The success of this operation 
         * \code
         *      auto item = lg::LibraryNone::Tense::item(1u);
         *      auto done = lg::LibraryNone::Tense::remove(item.tense_id_);
         *      QCOMPARE(done, true);
         * \endcode
         */
        static bool remove(unsigned tense_id) {
            auto db = database(); 
            QSqlQuery query(db);	
            db.transaction();
            query.exec(QString("DELETE FROM Items  WHERE tense_id = %1").arg(tense_id));
            query.exec(QString("DELETE FROM Tenses WHERE tense_id = %1").arg(tense_id));
            return db.commit();
        };
        /**
         * \brief Getting a TenseItem by ID
         * \param {in: unsigned}  tense_id - The Tense id
         * \result {out: TenseItem} The TenseItem object
         * \code
         *      auto item = lg::LibraryNone::Tense::item(1u);
         *      QCOMPARE(item.tense_id_, 1u);
         * \endcode
         */
        static Item item(unsigned tense_id) {
            QSqlQuery query(database());	
            query.exec(QString("SELECT tense_id, name, description FROM Tenses WHERE tense_id = '%1' LIMIT 1;").arg(tense_id));
            if(false == query.next()) return Item();
            return  Item(query.value(0).toUInt(),query.value(1).toString(), query.value(2).toString());
        } 
        /**
         * \brief This function gets all tenseitems 
         * \result {out: TenseItems} The container of tenseitems
         * \code
         *      auto vec = lg::LibraryNone::Tense::items();
         *      QCOMPARE(vec.size(), 2);
         * \endcode
         */
        static Items items() {
            Items result;
            QSqlQuery query(database());	
            query.exec(QString("SELECT tense_id, name, description FROM Tenses;"));
            while(query.next()) {
                result.push_back(Item(query.value(0).toUInt(),query.value(1).toString(), query.value(2).toString()));
            }
            return result;
        } 
    };
    struct LangItem {
        struct  Item;
        typedef std::vector<Item>  Items;
        typedef std::vector<Items> ListItems;

        /**
         * \brief  This class provides all work with Item
         **/
        struct Item {
            typedef Item  class_name;

            unsigned item_id_;         //!< The Item ID
            unsigned tense_id_;        //!< The Tense ID
            unsigned tense_;           //!< The Time line       // "past" -1  "present" -2  "future" -4
            unsigned type_;            //!< The Type's type     // "simple" - 1
            unsigned form_;            //!< The Form of verbs   // "affirmative" -1, "question" -2,  "negative" -4
            unsigned lang_;            //!< The Language ID
            QString  I_;
            QString  ye_;
            QString  you_;
            QString  he_;
            QString  she_;
            QString  it_;
            QString  we_;
            QString  they_;
            Item()
                : item_id_(0u), tense_id_(0u), tense_(0u), type_(0u), form_(0u), lang_(0u),I_(QString()), ye_(QString()), you_(QString()),he_(QString()),she_(QString()),it_(QString()),we_(QString()),they_(QString()){}
            Item(unsigned item_id, unsigned tense_id, unsigned tense, unsigned type, unsigned form, unsigned lang, QString const&  I, QString  const& ye, QString const&  you, QString const&  he, QString const&  she, QString const&  it, QString  const& we, QString const& they)
                : item_id_(item_id), tense_id_(tense_id), tense_(tense), type_(type), form_(form), lang_(lang), I_(I), ye_(ye), you_(you),he_(he),she_(she),it_(it),we_(we),they_(they){}
            friend bool operator == (Item const& lhs, Item const& rhs) {
                return lhs.item_id_ == rhs.item_id_ && lhs.tense_id_ == rhs.tense_id_ &&
                       lhs.lang_ == rhs.lang_ && lhs.tense_ == rhs.tense_ && lhs.type_ == rhs.type_ && lhs.form_ == rhs.form_ &&
                       lhs.I_ == rhs.I_ && lhs.ye_ == rhs.ye_ && lhs.you_ == rhs.you_ && lhs.he_ == rhs.he_ &&
                       lhs.she_ == rhs.she_ && lhs.it_ == rhs.it_ && lhs.we_ == rhs.we_ && lhs.they_ == rhs.they_;
            }
            friend bool operator != (Item const& lhs, Item const& rhs) {
                return !(lhs == rhs);
            }
        };
        /**
         * \brief This function adds new Item into the db
         * \result {out: unsigned} The last insert row id
         * \code
         *      lg::LibraryNone::LangItem::Item item(1u, tense.tense_id_,1,1,1,QLocale::Russian, "I look","ye look", "you look", "he looks", "she looks", "it looks", "we look", "they look");
         *      auto id = lg::LibraryNone::LangItem::add(item);
         *      QVERIFY(id != 0u);
         * \endcodel
         */
        static unsigned add(Item const& item) {
            QSqlQuery query(database());	
            query.prepare(QString("INSERT INTO Items(tense_id, tense, type, form, lang, I, ye, you, he, she, it, we, they) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);"));
            query.addBindValue(item.tense_id_);
            query.addBindValue(item.tense_);
            query.addBindValue(item.type_);
            query.addBindValue(item.form_);
            query.addBindValue(item.lang_);
            query.addBindValue(item.I_);
            query.addBindValue(item.ye_);
            query.addBindValue(item.you_);
            query.addBindValue(item.he_);
            query.addBindValue(item.she_);
            query.addBindValue(item.it_);
            query.addBindValue(item.we_);
            query.addBindValue(item.they_);
            if(false == query.exec()) return 0u;
            return inherited::getLastRowId(query);
        }
        /**
         * \brief This function gets all tenseitems 
         * \result {out: TenseItems} The container of tenseitems
         * \code
         *      auto vec = lg::LibraryNone::LangItem::items(tense_id);
         *      QCOMPARE(vec.size(), 2u);
         * \endcode
         */
        static Items items(unsigned tense_id) {
            Items result;
            QSqlQuery query(database());	
            query.exec(QString("SELECT item_id, tense_id, tense, type, form, lang, I, ye, you, he, she, it, we, they FROM Items WHERE tense_id = %1;").arg(tense_id));
            while(query.next()) {
                result.push_back(Item(query.value(0).toUInt(),   query.value(1).toUInt(), query.value(2).toUInt(),
                                      query.value(3).toUInt(),   query.value(4).toUInt(), query.value(5).toUInt(),
                                      query.value(6).toString(), query.value(7).toString(),
                                      query.value(7).toString(), query.value(9).toString(),
                                      query.value(10).toString(), query.value(11).toString(),
                                      query.value(12).toString(), query.value(13).toString()));
            }
            return result;
        } 
    };
};

typedef LibraryT<ps::strategy::message::window> 	Library;
typedef LibraryT<ps::strategy::message::none> 		LibraryNone;

} // end namespace lg
