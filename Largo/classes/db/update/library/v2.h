/**
 * \file      classes/db/update/library/v2.h
 * \brief     The v2 version for Library
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S. Panin
 * \version   v.1.1
 * \date      Created on 22 October 2013 y., 17:48
 * \TODO
**/
#pragma once
#include "../base.h"

/** \namespace lg::db::update::library  */
namespace lg {
namespace db {
namespace update {
namespace library {

class V2 : public lg::db::update::Base {
public:
    typedef V2                   class_name;
    typedef lg::db::update::Base inherited;

private:
    class Items {
        typedef Items class_name;

        static QString updateTense() {
            return "Update Items SET tense = 4 WHERE tense = 3;";
        }
        static QString updateForm() {
            return "Update Items SET form = 4 WHERE form = 3;";
        }
    public:
        static QStringList get() {
            QStringList result;
            result.push_back(updateTense());
            result.push_back(updateForm());
            return result;
        }
    };
protected:
    QStringList doExecute() {
        return Items::get();
    }

public:
   V2() {}
};
}}}} // end namespace lg::db::update::library

