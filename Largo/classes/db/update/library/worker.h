/**
 * \file      classes/db/update/library/worker.h
 * \brief     The Worker for Library Updates.
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S. Panin
 * \version   v.1.1
 * \date      Created on 22 October 2013 y., 17:16
 * \TODO
**/
#pragma once
#include <map>
#include "../base.h"
#include "v2.h"

/** \namespace lg::db::update::library  */
namespace lg {
namespace db {
namespace update {
namespace library {

/** \brief The Worker updates the structure of library database. */
class Worker {
public:
    typedef Worker	                                 class_name;
    typedef QSharedPointer<lg::db::update::Base>     base_t;
    typedef std::pair<unsigned, base_t>              pair_t;
    typedef std::map<unsigned, base_t>	             map_t;

private:
    QSqlDatabase db_;
    unsigned     last_version_;
    map_t        map_;

    unsigned getVersion() const {
        QSqlQuery query(db_);
        query.exec("SELECT version FROM program_info LIMIT 1;");
        if(!query.next()) return 0;
        return query.value(0).toUInt();
    }
    void initMap() {
        map_.insert(pair_t(2, base_t(new ::lg::db::update::library::V2())));
    }

public:
    Worker(QSqlDatabase const& db, unsigned last_version)
        : db_(db), last_version_(last_version) {
        initMap();
    }
    bool operator() () const {
        auto current_version = getVersion();
        if(current_version == 0 || current_version >= last_version_) return true;

        QSqlQuery query(db_);
        for(unsigned i = current_version; i != last_version_; ++i ) {
            auto it = map_.find(i+1);
            if(it != map_.end()) {
                auto list = (*it->second)();

                size_t done = 0;
                foreach (QString const& line, list)
                    done += query.exec(line);
                if(!done) return false;
           }
        }
        return query.exec(QString("UPDATE program_info SET version = '%1';").arg(last_version_));
    }
};
}}}} // end namespace lg::db::update::library

