/**
 * \file      classes/db/update/library/base.h
 * \brief     The Base Abstract class for Update Library
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S. Panin
 * \version   v.1.2
 * \date      Created on 22 October 2013 y., 17:54
 * \TODO
**/
#pragma once

/** \namespace lg::db::update  */
namespace lg {
namespace db {
namespace update {

class Base {
public:
    typedef Base class_name;

protected:
  virtual QStringList doExecute() = 0;

public:
   Base() {}
   virtual ~Base() {}
   QStringList operator()(){
       return doExecute();
   }
};
}}} // end namespace lg::db::update

