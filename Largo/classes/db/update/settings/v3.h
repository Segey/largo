/**
 * \file      classes/db/update/settings/v3.h
 * \brief     The v3 version for Settings
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin
 * \version   v.1.1
 * \date      Created on 01 November 2013 y., 19:43
 * \TODO
**/
#pragma once
#include "../base.h"

/** \namespace lg::db::update::settings  */
namespace lg {
namespace db {
namespace update {
namespace settings {

class V3 : public lg::db::update::Base {
public:
    typedef V3                   class_name;
    typedef lg::db::update::Base inherited;

private:
    class Settings2 {
        typedef Settings2 class_name;

        static QString drop() {
            return "DROP TABLE Settings2;";
        }
		static QString table() {
            return "CREATE TABLE Settings2 ("
                "settings2_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                "name         TEXT not null,"
                "value        TEXT not null"
                ")";
		}
		static QStringList index() {
            QStringList result;
			result << "CREATE UNIQUE INDEX Settings2_name ON Settings2(name);";
            return result;		
        }
    public:
        static QStringList get() {
            QStringList result;
            result.push_back(drop());
            result.push_back(table());
            result << index();
            return result;
        }
    };

protected:
    QStringList doExecute() {
        QStringList list;
        list << Settings2::get();
        return list;
    }

public:
   V3() {}
};
}}}} // end namespace lg::db::update::settings

