/**
 * \file      classes/db/update/settings/v2.h
 * \brief     The v2 version for Settings
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin
 * \version   v.1.1
 * \date      Created on 23 October 2013 y., 01:01
 * \TODO
**/
#pragma once
#include "../base.h"

/** \namespace lg::db::update::settings  */
namespace lg {
namespace db {
namespace update {
namespace settings {

class V2 : public lg::db::update::Base {
public:
    typedef V2                   class_name;
    typedef lg::db::update::Base inherited;

private:
    class Logs1 {
        typedef Logs1 class_name;

        static QString drop() {
            return "DROP TABLE Logs1;";
        }
        static QString table() {
            return "CREATE TABLE Logs1 ("
                "log1_id          INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                "time             TIMESTAMP NOT NULL,"
                "duration         TIME NOT NULL,"
                "word             TEXT NOT NULL,"
                "settings         TEXT NOT NULL,"
                "result           INTEGER NOT NULL,"
                "count            INTEGER NOT NULL"
                ")";
        }
        static QStringList index() {
            QStringList result;
            result << "CREATE INDEX Logs1_log1_id ON Logs1(log1_id);";
            return result;
        }
    public:
        static QStringList get() {
            QStringList result;
            result.push_back(drop());
            result.push_back(table());
            result << index();
            return result;
        }
    };
    static QString simpleWords() {
        return "DROP TABLE SimpleWords;";
    }

protected:
    QStringList doExecute() {
        QStringList list;
        list << Logs1::get();
        list.push_back(class_name::simpleWords());
        return list;
    }

public:
   V2() {}
};
}}}} // end namespace lg::db::update::settings

