/** 
 * \file       classes/db/settings.h
 * \brief      File for work with DB
 * \author     S.Panin <dix75@mail.ru>
 * \copyright  S.Panin
 * \version    v.1.8
 * \date       Created on 03 July 2013 y., 12:56
 * \TODO
**/
#pragma once

/** \namespace lg  */
namespace lg {
/**
 * \brief The base Settings
 * \param S - It is a strategy of error messages
 * \see go::base_bd;
 * 
 * Example code:
 *  \code
 *     lg::SettingsNone settings;
 *     settings.saveForm(name, this);
 *  \endcode
**/
template<typename S>
class SettingsT : private ps::base_db<S> {
public:
    typedef S                        strategy_type;
    typedef SettingsT<strategy_type> class_name;
    typedef ps::base_db<S>           inherited;

private:
    static bool isConnect;
    
public:
	/** \brief Ctor */
    SettingsT() {
        inherited::connect(module::settings::file(), module::settings::name());
    }
    /**
     * \brief This function returns True if is connected to db, otherwise false
     */
	bool connect() {
		return true;
	}
	/** \brief Getting link to the dababase **/
    static QSqlDatabase database() { 
        return inherited::database(module::settings::name()); 
    }
    /** \brief Getting last Row id **/
    static unsigned getLastRowId(QSqlQuery& query) {
        return inherited::getLastRowId(query);
    }
	/**
	 * \brief Creating the database
	 * \result bool - Success of this operation
	 * \code
     * 	lg::SettingsT<lg::strategy::message::window>::createDatabase();
	 * or 
     * 	lg::Settings::createDatabase();
	 * \endcode
    **/
    static bool createDatabase() {
		return inherited::createDatabase(lg::db::source::Settings::source(), module::settings::file(), module::settings::name());	
	}
    static bool tryToUpdate() {
        const unsigned version = module::settings::version().toUInt();
        ::lg::db::update::settings::Worker worker(database(),version);
        return worker();
    }

    /**
     * \brief Do the window's settings exist?
     * \param {in: QString const&} name     The record name
     * \code
     *    bool is = lg::SettingsNone::isWindowSaved(name());
     * \endcode
    **/
	static bool isWindowSaved(QString const& name) {
            QSqlQuery query(database());
            query.exec(QString("SELECT window_id FROM Windows WHERE name = '%1' LIMIT 1;").arg(name));
            query.next();
            return (query.value(0).toUInt() !=0 );
	}
    /**
     * \brief Saving any Window's settings
     * \param {in: QString const&} name  A record name
     * \param {in: template T*} window   A WIndow pointer
    **/
	template<typename T>
	static bool saveWindow(QString const& name, T* window) {
        QSqlQuery query(database());	
        query.exec(QString("SELECT window_id FROM Windows WHERE name = '%1' LIMIT 1;").arg(name));
        query.next();
		const auto id = query.value(0).toUInt(); 
		if(id > 0) return query.exec(QString("UPDATE Windows SET geometry ='%2', state = '%3' WHERE window_id = '%1';").arg(id).arg(ps::Serialization48::to(window->geometry())).arg(ps::Serialization48::to(window->saveState())));
		else  return query.exec(QString("INSERT INTO Windows(name, geometry, state) VALUES ('%1', '%2', '%3');").arg(name).arg(ps::Serialization48::to(window->geometry())).arg(ps::Serialization48::to(window->saveState())));
	}
    /**
     * \brief Restoring WIndow Data
     * \param {in: QString const&} name             A record name
     * \param {in: template T*} window              A window pointer
     * \param {in: QRect const&} default_geometry 	Default WIndow size
     * \param {in: QByteArray const&} default_state	Default Window State
     * \note  Success of this operation
    **/
	template<typename T>
	static bool restoreWindow(QString const& name, T* window, QRect const& default_geometry = QRect(), QByteArray const& default_state = QByteArray()) {
         QSqlQuery query(database());
	     query.exec(QString("SELECT window_id, geometry, state FROM Windows WHERE name = '%1' LIMIT 1;").arg(name));
             query.next();
		if( 0u ==  query.value(0).toUInt()) return false;
		window->setGeometry(ps::Serialization48::from(query.value(1).toString(), default_geometry));
		window->restoreState(ps::Serialization48::from(query.value(2).toString(), default_state));
		return true;
	}
       /**
        * \brief Saving a FileDialog settings
        * \code
        *     lg::SettingsNone::saveFileDialogPath(fileOpenPathName(), fileName);
        * \endcode
        * \param {in: QString const&} name  A record name
       **/
	static bool saveFileDialogPath(QString const& name, QString const& path) {
           QSqlQuery query(database());
           query.exec(QString("SELECT file_dialog_id FROM FileDialogs WHERE name = '%1' LIMIT 1;").arg(name));
           query.next();
           auto id = query.value(0).toUInt();
           if(id > 0u) return query.exec(QString("UPDATE FileDialogs SET path ='%2' WHERE file_dialog_id = '%1';").arg(id).arg(ps::Serialization48::to(path)));
           else return query.exec(QString("INSERT INTO FileDialogs(name, path) VALUES ('%1', '%2');").arg(name).arg(ps::Serialization48::to(path)));
	}
        /**
         * \brief Restoring a FileDialog path
         * \param {in: QString const&} name          a Record name
         * \param {in: QString const&} dialog        a Default path
         * \code
         *     auto path = lg::SettingsNone::restoreFileDialogPath(fileOpenPathName(), QString());
         * \endcode
        **/
    static QString restoreFileDialogPath(QString const& name, QString const& default_path = QString()) {
            QSqlQuery query(database());
            query.exec(QString("SELECT path FROM FileDialogs WHERE name = '%1' LIMIT 1;").arg(name));
            query.next();
            return ps::Serialization48::from(query.value(0).toString(), default_path);
	}
       /**
        * \brief Saving a Dialog settings
        * \param {in: QString const&} name  A record name
        * \param {in: template T*} dialog   A window pointer
       **/
	template<typename T>
	static bool saveDialog(QString const& name, T* dialog) {
           QSqlQuery query(database());
           query.exec(QString("SELECT dialog_id FROM Dialogs WHERE name = '%1' LIMIT 1;").arg(name));
           query.next();
           auto id = query.value(0).toUInt();
           if(id > 0u) return query.exec(QString("UPDATE Dialogs SET geometry ='%2' WHERE dialog_id = '%1';").arg(id).arg(ps::Serialization48::to(dialog->geometry())));
           else return query.exec(QString("INSERT INTO Dialogs(name, geometry) VALUES ('%1', '%2');").arg(name).arg(ps::Serialization48::to(dialog->geometry())));
	}
        /**
         * \brief Restoring a Dialog settings
         * \param {in: QString const&} name          a Record name
         * \param {in: template T*} dialog           a Window Pointer 
         * \param {in: QRect const&} default_geometry Window Size
         * \note  В случаи отсутствия значения возращается false
        **/
	template<typename T>
	static bool restoreDialog(QString const& name, T* dialog, QRect const& default_geometry = QRect()) {
            QSqlQuery query(database());
            query.exec(QString("SELECT dialog_id, geometry FROM Dialogs WHERE name = '%1' LIMIT 1;").arg(name));
            query.next();
            auto id = query.value(0).toUInt();
            if(0u == id) return false;
            dialog->setGeometry(ps::Serialization48::from(query.value(1).toString(), default_geometry));
            return true;
	}
};

    typedef SettingsT<ps::strategy::message::window>   Settings;
    typedef SettingsT<ps::strategy::message::none>     SettingsNone;

}; // end namespace lg 

