/**
 * \file       classes/db/source_settings.h
 * \brief      Sources for Settings
 * \author     S.Panin <dix75@mail.ru>
 * \copyright  S. Panin
 * \version    v.1.5
 * \date       Created on 02 July 2013 y., 12:57
 * \TODO
**/
#pragma once

/** \namespace lg::db::source  */
namespace lg {
namespace db {
namespace source {

/**
 * \brief The script ctreates the db Settings. 
 */
class Settings {
public:
    typedef Settings class_name;

private:
	class Dialogs {
		typedef Dialogs class_name;

		static QString table() {
            return "CREATE TABLE Dialogs ("
                "dialog_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                "name TEXT NOT NULL UNIQUE,"
                "geometry TEXT NOT NULL"
                ")";
		}
		static QString index() {
			return "CREATE UNIQUE INDEX Dialogs_dialog_id ON Dialogs(name);";
		}
	public:
		static QStringList get() {
			QStringList result;
			result.push_back(table());
			result.push_back(index());
			return result;
		}
	};
	class FileDialogs {
		typedef FileDialogs class_name;

		static QString table() {
            return "CREATE TABLE FileDialogs ("
                "file_dialog_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                "name           TEXT NOT NULL UNIQUE,"
                "path           TEXT NOT NULL"
                ")";
		}
		static QString index() {
			return "CREATE UNIQUE INDEX FIleDialogs_name ON FileDialogs(name);";
		}
	public:
		static QStringList get() {
			QStringList result;
			result.push_back(table());
			result.push_back(index());
			return result;
		}
	};
	class Windows {
		typedef Windows class_name;

		static QString table() {
            return "CREATE TABLE Windows ("
                "window_id INTEGER PRIMARY KEY     AUTOINCREMENT NOT NULL,"
                "name      TEXT    NOT     NULL    UNIQUE,"
                "geometry  TEXT    NOT     NULL,"
                "state     TEXT    NOT     NULL"
                ")";
		}
		static QString index() {
			return "CREATE UNIQUE INDEX Windows_window_id ON Windows(name);";
		}
	public:
		static QStringList get() {
			QStringList result;
			result.push_back(table());
			result.push_back(index());
			return result;
		}
	};

    class Logs1 {
        typedef Logs1 class_name;

        static QString table() {
            return "CREATE TABLE Logs1 ("
                "log1_id          INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                "time             TIMESTAMP NOT NULL,"
                "duration         TIME NOT NULL,"
                "word             TEXT NOT NULL,"
                "settings         TEXT NOT NULL,"
                "result           INTEGER NOT NULL,"
                "count            INTEGER NOT NULL"
                ")";
        }
        static QStringList index() {
            QStringList result;
            result << "CREATE INDEX Logs1_log1_id ON Logs1(log1_id);";
            return result;
        }
    public:
        static QStringList get() {
            QStringList result;
            result.push_back(table());
            result << index();
            return result;
        }
    };

	class Settings2 {
		typedef Settings2 class_name;

		static QString table() {
            return "CREATE TABLE Settings2 ("
                "settings2_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                "name         TEXT not null,"
                "value        TEXT not null"
                ")";
		}
		static QStringList index() {
            QStringList result;
			result << "CREATE UNIQUE INDEX Settings2_name ON Settings2(name);";
            return result;		
        }
	public:
		static QStringList get() {
			QStringList result;
			result.push_back(table());
			result << index();
			return result;
		}
	};
    static QString info() {
        return "CREATE TABLE program_info ( "
			"program TEXT NOT NULL UNIQUE,"
			"database TEXT NOT NULL UNIQUE,"
			"version INTEGER NOT NULL UNIQUE"
            ")";
    }
    static QString insertInfo() {
        return QString("INSERT INTO program_info(program, database, version) VALUES('%1', '%2', '%3');")
			.arg(program::name()).arg(module::settings::name()).arg(module::settings::version());
    }
public:
    /**
     * \brief Getting text line.
     * \result QStringList - The Text list.
    **/
    static QStringList source() {
        QStringList list;
		list << FileDialogs::get();
		list << Dialogs::get();
		list << Windows::get();
        list << Logs1::get();
		list << Settings2::get();
        list.push_back(class_name::info());
        list.push_back(class_name::insertInfo());
        return list;
    }
};
}}} // end namespace lg::db::source

