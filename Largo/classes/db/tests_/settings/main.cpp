#include <QtTest/QtTest>
#include <QTGui>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <iostream>
#include <vector>
#include <algorithm>
#include "tr.h"
#include "const.h"
#include "ps/c++/qt/macros.h"
#include "ps/c++/qt/strategy/strategy_message.h"
#include "ps/c++/qt/db/sqlite/serialization.h"
#include "ps/c++/qt/db/sqlite/base_db.h"
#include "classes/db/source_settings.h"
#include "classes/db/settings.h"
#include "classes/db/settings/logsv1.h"

class Test_Source : public QObject {
Q_OBJECT

	QSqlDatabase db_;

    QString dbName() const {
        return "settings.db";
    }

    void removeDB() {
        if(true == QFile::exists(dbName())) {
            db_.close(); 
            QFile::remove(dbName());
        }
    }

	void createAndOpenDB() {
        removeDB();

        db_ = QSqlDatabase::addDatabase("QSQLITE","settings");
        db_.setDatabaseName(dbName());
        if(false == db_.open()) return;

        size_t done = 0;
        QSqlQuery query(db_);

        QStringList list = lg::db::source::Settings::source();
        foreach (QString const& line, list) {
            done += !query.exec(line);
        } 
    }
	void countPragmaTableColumns(QString const& table, int count) {
		QSqlQuery query(db_);
		query.exec(QString("PRAGMA table_info ('%1');").arg(table));
		query.last();
		QCOMPARE(query.value(0).toInt() + 1, count);
		query.clear();
	}
	void countTableColumns(QString const& table, int count) {
		QSqlQuery query(db_);
		query.exec(QString("SELECT COUNT(*) FROM '%1';").arg(table));
		query.next();
		QCOMPARE(query.value(0).toInt(), count);
		query.clear();
	}
	void checkIndex(QString const& index_name, QString const& field) {
		QSqlQuery query(db_);
		query.exec(QString("PRAGMA index_info('%1');").arg(index_name));
		query.next();
		QCOMPARE(query.value(2).toString(), field);
		query.clear();
	}
private slots:
     void initTestCase()    { //!< Called before everything else
         createAndOpenDB(); 
    }      
    void cleanupTestCase() { //!< After testing
        removeDB();
    }
	void testDBTables() {
		QSqlQuery query(db_);
		query.exec("SELECT COUNT(*) FROM SQLITE_MASTER WHERE type='table' ORDER BY name;");
		query.next();
        QCOMPARE(query.value(0).toInt(), 7);
		query.clear();

		query.exec("SELECT name FROM SQLITE_MASTER WHERE type='table' ORDER BY name;");
		query.next();
        QCOMPARE(query.value(0).toString(), QString("Dialogs"));
		query.next();
        QCOMPARE(query.value(0).toString(), QString("FileDialogs"));
        query.next();
        QCOMPARE(query.value(0).toString(), QString("Logs1"));
		query.next();
        QCOMPARE(query.value(0).toString(), QString("Settings2"));
		query.next();
        QCOMPARE(query.value(0).toString(), QString("Windows"));
		query.next();
        QCOMPARE(query.value(0).toString(), QString("program_info"));
        query.next();
        QCOMPARE(query.value(0).toString(), QString("sqlite_sequence"));
	}
	void testDBProgram_info() {
        QSqlQuery query(db_);
        query.exec("SELECT program, database, version FROM program_info");
        query.next();
        QCOMPARE(query.value(0).toString(), QString("Largo"));
        QCOMPARE(query.value(1).toString(), QString("settings"));
        QCOMPARE(query.value(2).toInt(), 2);
	}
	void testDBTableFileDialogs() {
        countPragmaTableColumns("FileDialogs", 3);
        checkIndex("FileDialogs_name", "name");
	}
	void testDBTableDialogs() {
        countPragmaTableColumns("Dialogs", 3);
        checkIndex("Dialogs_dialog_id", "name");
	}
	void testDBTableWindows() {
        countPragmaTableColumns("Windows", 4);
        checkIndex("Windows_window_id", "name");
	}
    void testDBTableLogs1() {
        countPragmaTableColumns("Logs1",   7);
        checkIndex("Logs1_log1_id", "log1_id");
    }
	void testDBTableSettings2() {
        countPragmaTableColumns("Settings2", 3);
        checkIndex("Settings2_name", "name");
	}
    void testLogs1() {
        using namespace lg::settings;
        using namespace module::library::form;
        using namespace module::library::tense;

        LogV1 log1(0, QDateTime(QDate(2012,7,6), QTime(8,30,0)), QTime(1,23,4), QStringList() << "see", Sets(), 0, 0);
        QVERIFY(0 != LogV1::toDB(log1));

        auto logs1 = LogV1::allLogsFromDB();
        QVERIFY(logs1.size() == 1);

        Sets s1, s2;
        s1.form(interrogative().first | declarative().first | negative().first);
        LogV1 log2(0, QDateTime(QDate(2012,7,6), QTime(8,30,0)), QTime(3,25,43), QStringList() << "look", s1, 1, 5);
        QVERIFY(0 != LogV1::toDB(log2));
        auto logs2 = LogV1::allLogsFromDB();
        QVERIFY(logs2.size() == 2);
        auto log3 = logs2.back();
        QVERIFY(log3.duration()  == QTime(3,25,43));
        QVERIFY(log3.settings()  == s1);
        QVERIFY(log3.settings()  != s2);
        QVERIFY(log3.words()      == QStringList() << "look");
        QVERIFY(log3.result()     == 1);
        QVERIFY(log3.count()      == 5);
    }
};

QTEST_MAIN(Test_Source)
#include "main.moc"

