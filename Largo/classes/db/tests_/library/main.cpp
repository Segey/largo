#include <QtTest/QtTest>
#include <QTGui>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <iostream>
#include <vector>
#include <algorithm>
#include "tr.h"
#include "const.h"
#include "ps/c++/qt/macros.h"
#include "ps/c++/qt/strategy/strategy_message.h"
#include "ps/c++/qt/db/sqlite/serialization.h"
#include "ps/c++/qt/db/sqlite/base_db.h"
#include "classes/db/source_library.h"
#include "classes/db/library.h"

namespace ird {
class Test_Library : public QObject {
Q_OBJECT
    typedef Test_Library class_name;

	QSqlDatabase db_;

    QString dbName() const {
        return "library.db";
    }

    void removeDB() {
        if(true == QFile::exists(dbName())) {
            db_.close(); 
            QFile::remove(dbName());
        }
    }

	void createAndOpenDB() {
        removeDB();

        db_ = QSqlDatabase::addDatabase("QSQLITE","library");
        db_.setDatabaseName(dbName());
        if(false == db_.open()) return;

        size_t done = 0;
        QSqlQuery query(db_);

        QStringList list = lg::db::source::Library::source();
        foreach (QString const& line, list) {
            done += !query.exec(line);
        } 
    }
	void countPragmaTableColumns(QString const& table, int count) {
		QSqlQuery query(db_);
		query.exec(QString("PRAGMA table_info ('%1');").arg(table));
		query.last();
		QCOMPARE(query.value(0).toInt() + 1, count);
		query.clear();
	}
	void countTableColumns(QString const& table, int count) {
		QSqlQuery query(db_);
		query.exec(QString("SELECT COUNT(*) FROM '%1';").arg(table));
		query.next();
		QCOMPARE(query.value(0).toInt(), count);
		query.clear();
	}
	void checkIndex(QString const& index_name, QString const& field) {
		QSqlQuery query(db_);
		query.exec(QString("PRAGMA index_info('%1');").arg(index_name));
		query.next();
		QCOMPARE(query.value(2).toString(), field);
		query.clear();
	}
private slots:
     void initTestCase()    { //!< Called before everything else
         createAndOpenDB(); 
    }      
    void cleanupTestCase() { //!< After testing
        removeDB(); 
    }
	void testDBTables() {
		QSqlQuery query(db_);
		query.exec("SELECT COUNT(*) FROM SQLITE_MASTER WHERE type='table' ORDER BY name;");
		query.next();
		QCOMPARE(query.value(0).toInt(), 4);
		query.clear();

		query.exec("SELECT name FROM SQLITE_MASTER WHERE type='table' ORDER BY name;");
		query.next();
        QCOMPARE(query.value(0).toString(), QString("Items"));
		query.next();
        QCOMPARE(query.value(0).toString(), QString("Tenses"));
		query.next();
        QCOMPARE(query.value(0).toString(), QString("program_info"));
        query.next();
        QCOMPARE(query.value(0).toString(), QString("sqlite_sequence"));
	}
	void testDBProgram_info() {
        QSqlQuery query(db_);
        query.exec("SELECT program, database, version FROM program_info");
        query.next();
        QCOMPARE(query.value(0).toString(), QString("Largo"));
        QCOMPARE(query.value(1).toString(), QString("library"));
        QCOMPARE(query.value(2).toInt(), 1);
	}
	void testDBTableTenses() {
        countPragmaTableColumns("Tenses", 3);
        checkIndex("Tenses_tense_id", "tense_id");
	}
	void testDBTableItems() {
        countPragmaTableColumns("Items", 14);
        checkIndex("Items_item_id", "tense_id");
	}
    void testTenses()    {
         lg::LibraryNone::Tense::Item item0(1,"Look","Look description");
         auto id = lg::LibraryNone::Tense::add(item0);
         QCOMPARE(id, 1u);
         auto item1 = lg::LibraryNone::Tense::item(1u);
         QVERIFY(item0 == item1);
         
         lg::LibraryNone::Tense::Item item2(item1.tense_id_, "see", "Seesn");
         auto done = lg::LibraryNone::Tense::update(item2);
         QCOMPARE(done, true);
         auto item3 = lg::LibraryNone::Tense::item(1u);
         QVERIFY(item2 == item3);
         QVERIFY(item0 != item2);

         const auto empty = lg::LibraryNone::Tense::Item();
         auto item4 = lg::LibraryNone::Tense::item(2u);
         QVERIFY(item4 == empty);
         
         auto done1 = lg::LibraryNone::Tense::remove(1u);
         QCOMPARE(done1, true);
         auto item5 = lg::LibraryNone::Tense::item(1u);
         QVERIFY(item5 == empty);
         
         lg::LibraryNone::Tense::Item item12(2,"Look","Look description");
         lg::LibraryNone::Tense::Item item13(3,"Look","Look description");
         lg::LibraryNone::Tense::Item item14(4,"Look","Look description");
         lg::LibraryNone::Tense::add(item12);
         lg::LibraryNone::Tense::add(item13);
         lg::LibraryNone::Tense::add(item14);

         auto items = lg::LibraryNone::Tense::items();
         QVERIFY(items.size() == 3);
         QVERIFY(item12 == items.at(0));
         QVERIFY(item13 == items.at(1));
         QVERIFY(item14 == items.at(2));
    }      
    void testItems()    {
         lg::LibraryNone::Tense::Item tense(5u,"look", "look tense");
         auto id = lg::LibraryNone::Tense::add(tense);
         QCOMPARE(id, 5u);

         lg::LibraryNone::LangItem::Item item1(1u, id, 1, 1, 1, QLocale::Russian,"I look","ye look", "you look", "he looks", "she looks", "it looks", "we look", "they look");
         auto id1 = lg::LibraryNone::LangItem::add(item1);
         QVERIFY(id1 == 1u);
         
         lg::LibraryNone::LangItem::Item item2(2u, id, 2, 2, 2, QLocale::Russian,"I see","ye see", "you see", "he sees", "she sees", "it sees", "we see", "they see");
         auto id2 = lg::LibraryNone::LangItem::add(item2);
         QVERIFY(id2 == 2u);
         
         auto vec = lg::LibraryNone::LangItem::items(id);
         QCOMPARE(vec.size(), 2u);
         
         auto done1 = lg::LibraryNone::Tense::remove(id);
         QCOMPARE(done1, true);
         
         auto vec2 = lg::LibraryNone::LangItem::items(tense.tense_id_);
         QCOMPARE(vec2.size(), 0u);
    }
};

} // end namespace
QTEST_MAIN(ird::Test_Library)
#include "main.moc"
