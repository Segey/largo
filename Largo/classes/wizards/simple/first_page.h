/**
 * \file       classes/wizards/simple/first_page.h
 * \brief      The Simple first_page 
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S.Panin
 * \version    v.1.1
 * \date       Created on 16 January 2014 y., 01:02
 * \TODO
**/
#pragma once

#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <QWizardPage>

/** \namespace lg::wizards */
namespace lg {
namespace wizards {
namespace simple {

/**
 *  \brief The Simple Input wizard 
 *   \code
 *       using namespace lg::wizards::simple;
 *       FirstPage first;
 *       auto page = first.getPage();
 *         or
 *       auto const& first = lg::wizards::simple::FirstPage().getPage();
 *
 *   \endcode
**/
class FirstPage: public QWizardPage {
Q_OBJECT

public:
    typedef FirstPage   class_name;
    typedef QWizardPage inherited;

private:
    QLabel*      title_label_;
    QLabel*      example_label_;
    QLineEdit*   edit_;

    void instanceWidgets() {
        inherited::setTitle(iDialog::tr("Fill in Your Details"));
        inherited::setSubTitle(iDialog::tr("Please fill the English word field"));

        title_label_   = new QLabel(iDialog::tr("A E&nglish word"));
        edit_          = new QLineEdit;
        title_label_->setBuddy(edit_);

        example_label_ = new QLabel();
        QFont font = example_label_->font();
        font.setItalic(true);
        example_label_->setFont(font);

        connect(edit_, SIGNAL(textChanged(QString const&)), SLOT(textChanged(QString const&)));
    }
    QGridLayout* intanceLayout() {
        auto layout = new QGridLayout;
        layout->setRowStretch(0,1);
        layout->addWidget(title_label_, 1, 0);
        layout->addWidget(edit_, 1, 1);
        layout->addWidget(example_label_, 2, 1);
        layout->setRowStretch(3,3);
        return layout;
    }
private slots:
    void textChanged(QString const& text) {
        example_label_->setText(text.size() < 2 ? "" : QString("Example:   I will %1.").arg(text));
    }

public:
    explicit FirstPage()
        : title_label_(nullptr), example_label_(nullptr), edit_(nullptr){
        instanceWidgets();
        setLayout(intanceLayout());
    }
    virtual ~FirstPage() {
    }
};

}}} // end namespace lg::wizards::simple

