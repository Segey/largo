/**
 * \file      classes/functions/language.h 
 * \brief     The Main Language file
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2013 - 2014 
 * \version   v.1.0
 * \created   March (the) 16(th), 2014, 17:53 MSK
 * \updated   March (the) 16(th), 2014, 17:53 MSK
 * \TODO      
**/
#pragma once

/** \namespace lg */
namespace lg { 

class Language {
public:
    typedef Language class_name;

private:
    unsigned lang_;

public:
    explicit Language(unsigned lang = QLocale::AnyLanguage)
        : lang_(lang){
    }
    Language(Language const& rhs)
        : lang_(rhs.lang_) {
    }
    static Language Russian() {
        return Language(QLocale::Russian);
    }
    static Language English() {
        return Language(QLocale::English);
    }
    static Language None() {
        return Language(QLocale::AnyLanguage);
    }
    Language& operator=(Language const& rhs){
        lang_ = rhs.lang_;
        return *this;
    }
    Language& operator=(unsigned lang){
        lang_ = lang;
        return *this;
    }
    operator unsigned() const {
        return lang_;
    }
    QString short_lang() const {
        if(lang_ == QLocale::Russian) return "ru";
        if(lang_ == QLocale::English) return "en";
        return "none";
    }
    ~Language(){
    }
};
} // end namespace lg
