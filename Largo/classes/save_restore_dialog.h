/**
 * \file      save_restore_dialog.h
 * \brief     The File is used to describe the parameters necessary for creating dialog.
 * \author    S.Panin <dix75@mail.ru>
 * \Copyright S. Panin
 * \version   v.1.1
 * \date      Created on 17 July 2013 y., 23:43
 * \TODO
**/
#pragma once
/** namespace lg */
namespace lg {

/**
 * \code 
 *    class DisplaySimpleWordsDialog : public lg::SaveRestoreDialog;
 * \endcode
**/
class SaveRestoreDialog : public QDialog {
    typedef SaveRestoreDialog class_name;
    typedef QDialog inherited;

private:
    QLatin1String settins_location_name_;

protected:
    virtual void settingsLocationRead() {
       lg::SettingsNone::restoreDialog(settins_location_name_, this);
    }
    virtual void settingsLocationWrite() {
       lg::SettingsNone::saveDialog(settins_location_name_, this);
    }
    virtual void closeEvent(QCloseEvent* /* event */ ) {
        settingsLocationWrite();
    }
public:
    explicit SaveRestoreDialog (QLatin1String const& name, QWidget* parent = nullptr)
        : inherited(parent), settins_location_name_(name) {
    }
};

} // end namespace lg
