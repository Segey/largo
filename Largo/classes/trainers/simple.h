/**
 * \file       classes/simple/dialogs/trainer/simple.h
 * \brief      The Simple Trainer class provides to display all items
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.2
 * \date       Created on 16 July 2013 y., 15:41
 * \TODO
**/
#pragma once
#include <QLabel>
#include <QVector>
#include <QMap>
#include "base.h"


/** namespace lg::simple */
namespace lg {
namespace simple {

class SimpleTrainer : public BaseTrainer {
    typedef SimpleTrainer                class_name;
    typedef BaseTrainer                  inherited;
    typedef QMultiMap<unsigned, QString> imap_t;
    typedef QPair<QString,      QString> item_t;
    typedef QVector<item_t>              items_t;

private:
    item_t  current_;
    items_t items_;

    void createMap(imap_t& imap, inherited::Item const& item) const {
        const auto tense = item.tense_;
        const auto form  = item.form_;
        unsigned result = (3u == tense ? 4u : tense);
        result |= (1u == form ? 8u : (2u == form ? 16u: 32u));
        imap.insert((result | 64),   item.I_);
        imap.insert((result | 128),  item.ye_);
        imap.insert((result | 256),  item.you_);
        imap.insert((result | 512),  item.he_);
        imap.insert((result | 1024), item.she_);
        imap.insert((result | 2048), item.it_);
        imap.insert((result | 4096), item.we_);
        imap.insert((result | 8192), item.they_);
    }

protected:
    virtual bool doHasNext() const {
        return false == items_.empty();
    }
    virtual void doNext(QLabel* question, QLabel* rest) {
        auto num = ps::randInt(items_.size() - 1);
        auto it = items_.begin() + num;
        current_ = (*it);
        question->setText(it->first);
        items_.erase(it);
        rest->setText(QString("Rest %1 word(s)").arg(items_.size()));
     //   qDebug() << num  << items_.size() << current_.first << current_.second;
    }
    virtual void doShowAnswer(QLabel* answer) const {
        answer->setText(current_.second);
    }
    virtual void doSetData(inherited::ListItems const& list) {
        imap_t imap;
        items_.clear();
        items_.reserve(144);
        std::for_each(list.begin(), list.end(), [&] (inherited::Items const& items) {
            std::for_each(items.begin(), items.end(), [&] (inherited::Item const& item) {
                createMap(imap, item);
            });
        });

        foreach (unsigned index, imap.uniqueKeys()) {
            auto const& vals = imap.values(index);
            items_.push_back(qMakePair(vals.front(),vals.back()));
            items_.push_back(qMakePair(vals.back(),vals.front()));
        }
    } 
public:
    /**
     * \code
     *     lg::simple::SimpleTrainer trainer;
     * \endcode
    **/
    explicit SimpleTrainer() : BaseTrainer() {}
    virtual ~SimpleTrainer() {}
};

}} // end namespace lg::simple
