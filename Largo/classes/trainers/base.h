/**
 * \file       classes/simple/dialogs/trainer/base.h
 * \brief      The Abstract Trainer class provides virtual functions
 * \author     S.Panin <dix75@mail.ru>
 * \Copyright  S. Panin
 * \version    v.1.2
 * \date       Created on 16 July 2013 y., 15:41
 * \TODO
**/
#pragma once

/** namespace lg::simple */
namespace lg {
namespace simple {

class BaseTrainer {
public:
    typedef BaseTrainer                          class_name;
    typedef lg::LibraryNone::LangItem::Item      Item;
    typedef lg::LibraryNone::LangItem::Items     Items;
    typedef lg::LibraryNone::LangItem::ListItems ListItems;
private:

protected:
    virtual void doSetData(ListItems const&) = 0; 
    virtual bool doHasNext() const = 0;
    virtual void doNext(QLabel*, QLabel*) = 0;
    virtual void doShowAnswer(QLabel*) const = 0;

public:
    explicit BaseTrainer() {}
    virtual ~BaseTrainer() {}
    void setData(ListItems const& list) {
       doSetData(list); 
    }
    bool hasNext() const {
        return doHasNext();
    }
    void next(QLabel* question, QLabel* rest) {
        doNext(question, rest);
    }
    void showAnswer(QLabel* answer) {
        doShowAnswer(answer);
    }
};

}} // end namespace lg::simple
