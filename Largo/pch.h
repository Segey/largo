/**
* \file      pch.h
* \brief     The PCH file
* \author    S.Panin <dix75@mail.ru>
* \Copyright S.Panin
* \version   v.1.1
* \date      Created on 29 July 2013 y., 00:05
* \TODO
**/

#include <set>
#include <iostream>
#include <algorithm>
#include <iterator>

#include "tr.h"
#include "const.h"
#include "sets.h"

#include "ps/c++/qt/macros.h"
#include "ps/c++/qt/rand.h"
#include "ps/c++/qt/strategy/strategy_message.h"
#include "ps/c++/qt/instance/dialog_instance.h"
#include "ps/c++/qt/instance/main_window_instance.h"
#include "ps/c++/qt/instance/plugin_main_window_instance.h"
#include "ps/c++/qt/db/sqlite/base_db.h"
#include "ps/c++/qt/db/sqlite/serialization.h"
#include "ps/c++/qt/widgets/checkbox_group.h"
#include "ps/c++/qt/widgets/radio_button_group.h"
#include "ps/c++/qt/widgets/extend_radio_button_group.h"
#include "ps/c++/qt/widgets/check_header_view.h"
#include "ps/c++/qt/delegates/bool_delegate.h"

#include "classes/functions/dialog.h"
//#include "classes/functions/verb.h"
//#include "classes/functions/irregular_verbs.h"

#include "classes/db/update/base.h"
#include "classes/db/update/settings/worker.h"
#include "classes/db/update/settings/v2.h"
#include "classes/db/update/library/worker.h"
#include "classes/db/update/library/v2.h"

#include "classes/db/source_library.h"
#include "classes/db/library.h"
#include "classes/db/update/library/worker.h"
#include "classes/db/update/library/v2.h"

#include "classes/db/source_settings.h"
#include "classes/db/settings.h"
#include "classes/db/settings/logsv1.h"
#include "classes/db/settings/settings2.h"

#include "classes/save_restore_dialog.h"
#include "classes/save_restore_window.h"

#include "classes/policies/times/none.h"
#include "classes/policies/times/simply.h"

#include "classes/policies/data/none.h"
#include "classes/policies/data/simple.h"
#include "classes/policies/data/words.h"
#include "classes/policies/data/birds2.h"

#include "classes/policies/sequences/none.h"
#include "classes/policies/sequences/random.h"
#include "classes/policies/sequences/lineal.h"

#include "classes/policies/logs/none.h"
#include "classes/policies/logs/v1.h"

#include "classes/trainers/base.h"
#include "classes/trainers/simple.h"

#include "classes/dialogs/done/instance.h"
#include "classes/dialogs/done/done_dialog.h"

#include "classes/dialogs/simple/instance.h"
#include "classes/dialogs/simple/simple_dialog_base.h"
#include "classes/dialogs/simple/simple_dialog.h"

#include "classes/dialogs/simple2/instance.h"
#include "classes/dialogs/simple2/simple2_dialog_base.h"
#include "classes/dialogs/simple2/simple2_dialog.h"

#include "classes/dialogs/birds2/instance.h"
#include "classes/dialogs/birds2/birds2_dialog_base.h"
#include "classes/dialogs/birds2/birds2_dialog.h"

#include "classes/dialogs/words/instance.h"
#include "classes/dialogs/words/words_dialog_base.h"
#include "classes/dialogs/words/words_dialog.h"

#include "classes/simple/menu/menu_help.h"

#include "classes/simple/central_widget_instance.h"
#include "classes/simple/central_widget.h"

#include "classes/simple/docks/logs/logs.h"
#include "classes/simple/docks/options/options.h"

#include "classes/wizards/simple/first_page.h"
#include "classes/wizards/simple/second_page.h"

#include "classes/simple/docks/simple_words/from_xml.h"
#include "classes/simple/docks/simple_words/simple_words_view.h"
#include "classes/simple/docks/simple_words/simple_words_instance.h"
#include "classes/simple/docks/simple_words/simple_words.h"
