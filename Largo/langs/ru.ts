<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU" sourcelanguage="en_US">
<context>
    <name>iAction</name>
    <message>
        <location filename="../classes/simple/menu/menu_help.h" line="28"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/logs/logs_instance.h" line="36"/>
        <source>Clear Logs</source>
        <translation>Удалить логи</translation>
    </message>
</context>
<context>
    <name>iButton</name>
    <message>
        <location filename="../classes/simple/central_widget_instance.h" line="69"/>
        <location filename="../classes/simple/central_widget_instance.h" line="71"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../classes/simple/central_widget_instance.h" line="70"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/simple/instance.h" line="55"/>
        <location filename="../classes/dialogs/words/instance.h" line="48"/>
        <source>Answer</source>
        <translation>Ответ</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/simple/instance.h" line="56"/>
        <location filename="../classes/dialogs/simple2/instance.h" line="67"/>
        <location filename="../classes/dialogs/words/instance.h" line="49"/>
        <source>Next &gt;&gt;</source>
        <translation>Следующее &gt;&gt;</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/simple_words_instance.h" line="61"/>
        <source>The list of words</source>
        <translation>Список слов</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/simple_words_instance.h" line="63"/>
        <source>The list of actions</source>
        <translation>Список действий</translation>
    </message>
    <message>
        <source>Omit/Don&apos;t omit word(s)</source>
        <translation type="obsolete">Включить/Исключить слово(а)</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/simple_words_instance.h" line="64"/>
        <source>Remove word(s)</source>
        <translation>Удалить слово(а)</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/simple_words_instance.h" line="65"/>
        <source>Add word</source>
        <translation>Добавить слово из файла</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/simple_words_instance.h" line="68"/>
        <source>Execute</source>
        <translation>Выполнить</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/birds2/instance.h" line="45"/>
        <source>True</source>
        <translation>Правда</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/birds2/instance.h" line="46"/>
        <source>False</source>
        <translation>Ложь</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/logs/logs_instance.h" line="52"/>
        <source>Logs</source>
        <translation>Логи</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="64"/>
        <source>Options</source>
        <translation>Опции</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="67"/>
        <source>The Dialogs</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="68"/>
        <source>The simple</source>
        <translation>Простое</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="69"/>
        <source>The simple2</source>
        <translation>Простое2</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="70"/>
        <source>The words</source>
        <translation>Слова</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="71"/>
        <source>The true-false</source>
        <translation>Сплит</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="75"/>
        <source>The forms</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="76"/>
        <location filename="../classes/simple/docks/options/options_instance.h" line="88"/>
        <source>more...</source>
        <translation>еще...</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="77"/>
        <location filename="../classes/simple/docks/options/options_instance.h" line="89"/>
        <location filename="../classes/simple/docks/options/options_instance.h" line="99"/>
        <source>All</source>
        <translation>Все</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="78"/>
        <source>The interrogative</source>
        <translation>Вопросительное</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="79"/>
        <source>The declarative</source>
        <translation>Декларативное</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="80"/>
        <source>The negative</source>
        <translation>Отрицание</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="81"/>
        <source>The interrogative + the declarative</source>
        <translation>Вопросительное + Декларативное</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="82"/>
        <source>The interrogative + the negative</source>
        <translation>Вопросительное + Отрицание</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="83"/>
        <source>The declarative + the negative</source>
        <translation>Декларативное + Отрицание</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="87"/>
        <source>The tenses</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="90"/>
        <source>The past</source>
        <translation>Прошедшее</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="91"/>
        <source>The present</source>
        <translation>Настоящее</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="92"/>
        <source>The future</source>
        <translation>Будущее</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="93"/>
        <source>The past + the present</source>
        <translation>Прошлое + Настоящее</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="94"/>
        <source>The past + the future</source>
        <translation>Прошлое + Будущее</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="95"/>
        <source>The present + the future</source>
        <translation>Настоящее + Будущее</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="98"/>
        <source>The languages</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="100"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/options/options_instance.h" line="101"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/simple2/instance.h" line="101"/>
        <source>Delay time(sec):</source>
        <translation>Задержка(сек):</translation>
    </message>
</context>
<context>
    <name>iCalc</name>
    <message>
        <location filename="../classes/simple/menu/menu_help.h" line="44"/>
        <source>author:</source>
        <translation>автор:</translation>
    </message>
    <message>
        <location filename="../classes/simple/menu/menu_help.h" line="45"/>
        <source>version:</source>
        <translation>версия:</translation>
    </message>
    <message>
        <location filename="../classes/simple/menu/menu_help.h" line="46"/>
        <source>build:</source>
        <translation>build:</translation>
    </message>
</context>
<context>
    <name>iDialog</name>
    <message>
        <location filename="../classes/dialogs/done/instance.h" line="54"/>
        <source>Statistics</source>
        <translation>Статистика</translation>
    </message>
</context>
<context>
    <name>iError</name>
    <message>
        <location filename="../classes/simple/docks/simple_words/from_xml/v1.h" line="32"/>
        <location filename="../classes/simple/docks/simple_words/from_xml/v2.h" line="114"/>
        <source>The file doesn&apos;t contain the Item tags!</source>
        <translation>В файле не найдено ни одного тега item!</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/from_xml/v1.h" line="44"/>
        <location filename="../classes/simple/docks/simple_words/from_xml/v2.h" line="130"/>
        <source>The file doesn&apos;t contain the correct Item tags!</source>
        <translation>В файле найдены один или более некорректных тега item!</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/from_xml.h" line="104"/>
        <location filename="../classes/simple/docks/simple_words/from_xml.h" line="134"/>
        <source>File %1 doesn&apos;t exist or busy !</source>
        <translation>Файл %1 не существует или занят другим процессом!</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/from_xml.h" line="107"/>
        <location filename="../classes/simple/docks/simple_words/from_xml.h" line="135"/>
        <source>The file doesn&apos;t contain the Simple Word data!</source>
        <translation>Файл не содержит корректных данных!</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/from_xml.h" line="112"/>
        <source>The file contains the incorrect version of data type! This version hasn&apos;t supported yet!</source>
        <translation>Файл содержит не корректные данные. Данная версия еще не поддерживается!</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/from_xml.h" line="143"/>
        <source>The Largo Tag doesn&apos;t contain the Name attribute!</source>
        <translation>Тег Largo не содержит атрибута Name!</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/from_xml.h" line="144"/>
        <source>The Largo Tag doesn&apos;t contain the Description attribute!</source>
        <translation>Тег Largo не содержит атрибута Description!</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/from_xml.h" line="153"/>
        <source>Cannot add the library tense item to the Database!</source>
        <translation>Невозможно добавить данные Library item в базу данных!</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/from_xml.h" line="157"/>
        <source>Cannot add the library tense items to the Database!</source>
        <translation>Невозможно добавить данные Library items в базу данных!</translation>
    </message>
    <message>
        <source>Cannot add the settings simple word item to the Database!</source>
        <translation type="obsolete">Невозможно добавить данные Settings items в базу данных!</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/simple_words_view.h" line="158"/>
        <source>Adding operation is failed with error!</source>
        <translation>Операция завершилась с ошибкой!</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/simple_words_view.h" line="159"/>
        <source>You have to check your data in file.</source>
        <translation>Проверьте пожалуйста данные в файле.</translation>
    </message>
</context>
<context>
    <name>iMenu</name>
    <message>
        <location filename="../classes/simple/docks/simple_words/simple_words_view.h" line="268"/>
        <source>Select one Simple word file to open</source>
        <translation>Выберете файл с данными для открытия</translation>
    </message>
    <message>
        <location filename="../classes/simple/menu/menu_help.h" line="64"/>
        <source>&amp;Help</source>
        <translation>&amp;Помощь</translation>
    </message>
    <message>
        <location filename="../classes/simple/simple_instance.h" line="63"/>
        <source>&amp;View</source>
        <translation>&amp;Отображение</translation>
    </message>
</context>
<context>
    <name>iMessage</name>
    <message>
        <location filename="../classes/simple/menu/menu_help.h" line="41"/>
        <source>The program trainee for studying Languages.</source>
        <translation>Программа для изучения иностранных языков.</translation>
    </message>
    <message>
        <location filename="../ps/c++/qt/db/sqlite/base_db.h" line="36"/>
        <source>Error while trying to create database file &apos;%1&apos; !</source>
        <translation>Возникла ошибка при создании базы данных &apos;%1&apos; !</translation>
    </message>
    <message>
        <location filename="../ps/c++/qt/db/sqlite/base_db.h" line="73"/>
        <source>%1 could not open &apos;%2&apos; because it is either not a supported file type or because the file has been damaged (for example, it was sent as an email attachment and wasn&apos;t correctly decoded).</source>
        <translation>%1 не может открыть &apos;%2&apos;, возможно файл отсутствует или был поврежден (например, файл открыт в другой программе).</translation>
    </message>
</context>
<context>
    <name>iTemplate</name>
    <message>
        <location filename="../classes/policies/logs/v1.h" line="57"/>
        <source>[%1] %2 %3 {%4} %5 %6 %7 %8</source>
        <translation>[%1] %2 %3 {%4} %5 %6 %7 %8</translation>
    </message>
</context>
<context>
    <name>iText</name>
    <message>
        <location filename="../classes/dialogs/done/done_dialog.h" line="55"/>
        <source>The type of dialog</source>
        <translation>Тип диалога</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/done/done_dialog.h" line="56"/>
        <source>The word(s)</source>
        <translation>Слово(а)</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/done/done_dialog.h" line="57"/>
        <source>The language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/done/done_dialog.h" line="58"/>
        <source>The forms</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/done/done_dialog.h" line="59"/>
        <source>The tenses</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/done/done_dialog.h" line="60"/>
        <source>The number of correct answers</source>
        <translation>Кол-во верных ответов</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/done/done_dialog.h" line="61"/>
        <source>The number of questions</source>
        <translation>Кол-во вопросов</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/done/done_dialog.h" line="62"/>
        <source>The percentage</source>
        <translation>Процентное отношение</translation>
    </message>
</context>
<context>
    <name>iType</name>
    <message>
        <source>true</source>
        <translation type="obsolete">да</translation>
    </message>
    <message>
        <source>false</source>
        <translation type="obsolete">нет</translation>
    </message>
</context>
<context>
    <name>iWord</name>
    <message>
        <location filename="../classes/dialogs/birds2/instance.h" line="81"/>
        <location filename="../classes/dialogs/simple/instance.h" line="77"/>
        <location filename="../classes/dialogs/simple2/instance.h" line="100"/>
        <location filename="../classes/dialogs/words/instance.h" line="72"/>
        <source>The current word:</source>
        <translation>Текущее слово:</translation>
    </message>
    <message>
        <location filename="../classes/simple/docks/simple_words/simple_words_view.h" line="148"/>
        <source>Word</source>
        <translation>Слово</translation>
    </message>
    <message>
        <source>Min. time</source>
        <translation type="obsolete">Мин. время</translation>
    </message>
    <message>
        <source>Attempts</source>
        <translation type="obsolete">Попытки</translation>
    </message>
    <message>
        <source>Omit</source>
        <translation type="obsolete">Исключить</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/birds2/birds2_dialog_base.h" line="61"/>
        <location filename="../classes/dialogs/simple/simple_dialog_base.h" line="51"/>
        <location filename="../classes/dialogs/simple2/simple2_dialog_base.h" line="67"/>
        <location filename="../classes/dialogs/words/words_dialog_base.h" line="105"/>
        <source>Rest %1 word(s)</source>
        <translation>Осталось %1 предложение(й)</translation>
    </message>
    <message>
        <location filename="../classes/dialogs/words/instance.h" line="44"/>
        <source>The variations of words:</source>
        <translation>Предложения:</translation>
    </message>
</context>
</TS>
