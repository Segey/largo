[Setup]
AppName=Largo
AppVersion=1.3
DefaultDirName={pf}\Largo
DefaultGroupName=Largo
UninstallDisplayIcon={app}\xLargo.ico
Compression=lzma2
SolidCompression=yes
AppMutex=LargoMutex
CloseApplications=true
SetupIconFile=Release/Largo.ico
WizardSmallImageFile=Release/Largo.bmp

[Languages]
Name: en; MessagesFile: "compiler:Default.isl"; LicenseFile: "Release/license.rtf";
Name: ru; MessagesFile: "compiler:Languages\Russian.isl"; LicenseFile: "Release/license.rtf";

[Messages]
en.BeveledLabel=English
ru.BeveledLabel=Russian

[Files]
Source: "Release/*"; DestDir: "{app}"; Flags: recursesubdirs ignoreversion; 
Source: "Release/Largo.exe"; DestDir: "{app}"; Flags: isreadme restartreplace;

[Icons]
Name: "{group}\Largo"; Filename: "{app}\Largo.exe"; WorkingDir: "{app}";  IconFilename: "{app}\Largo.ico"
Name: "{group}\������� Largo"; Filename: "{uninstallexe}"; Languages: ru;  IconFilename: "{app}\xLargo.ico"
Name: "{group}\�������� Largo"; Filename: "{app}\license.rtf"; Languages: ru;
Name: "{group}\Uninstall Largo"; Filename: "{uninstallexe}"; Languages: en;    IconFilename: "{app}\xLargo.ico"
Name: "{group}\The Largo license"; Filename: "{app}\license.rtf"; Languages: en;
